﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ChemTubeUI : MonoBehaviour 
{
	public GameObject tubePrefab;
	public WonderwoodColor wwColor;
	public Image uiWater;

	Vector3 firstScale;
	// Use this for initialization
	void Start () 
	{
		firstScale = transform.localScale;
		UpdateWaterColor();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void UpdateWaterColor()
	{
		uiWater.color = ColorManager.Instance.WonderwoodToColor(wwColor);
	}

	public ChemTube InstantiateChemTube()
	{
		ChemTube chemTube = Instantiate(tubePrefab).GetComponent<ChemTube>();
		chemTube.SetWaterColor(ColorManager.Instance.WonderwoodToColor(wwColor));
		transform.DOKill();
		transform.localScale = firstScale;
		transform.DOPunchScale(Vector3.one * 0.3f, 1f, 6);
		return chemTube;
	}

	
}
