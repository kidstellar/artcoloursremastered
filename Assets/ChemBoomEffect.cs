﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChemBoomEffect : MonoBehaviour 
{
	public ParticleSystem particle;

	public static ChemBoomEffect Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void PlayEffect()
	{
		particle.Play();
	}

	public void SetStartEndColors(Color startColor, Color endColor)
	{
		var main = particle.main;
		main.startColor = new ParticleSystem.MinMaxGradient(startColor, endColor);

	}



}
