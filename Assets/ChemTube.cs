﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemTube : MonoBehaviour 
{
	public SpriteRenderer waterSprite;
	public ParticleSystem waterParticle;
	public float hideY;
	public float showY;
	public Color waterColor;



	// Use this for initialization
	void Start () 
	{
		SetParticleColor(waterParticle, waterColor);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void SetParticleEmission(ParticleSystem particle, float value)
	{
		var emission = particle.emission;
		emission.rateOverTime = value;
	}

	void SetParticleColor(ParticleSystem particle, Color color)
	{
		var main = particle.main;
		main.startColor = color;
	}

	public void SetWaterColor(Color color)
	{
		waterColor = color;
		waterSprite.color = color;
	}

	public void Unfill(ChemBeaker beaker)
	{
		StartCoroutine(UnfillTubeTask(beaker));
	}

	IEnumerator UnfillTubeTask(ChemBeaker beaker)
	{
		transform.DOMove(beaker.transform.position + new Vector3(0.9f, 1.5f), 0.5f);
		yield return transform.DORotate(Vector3.forward * 94f, 0.5f).WaitForCompletion();
		SetParticleEmission(waterParticle, 30);
		beaker.color = waterColor;
		beaker.FillLiquid();
		yield return waterSprite.transform.DOLocalMoveY(hideY, 2f).WaitForCompletion();
		SetParticleEmission(waterParticle, 0);
		SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
		for (int i = 0; i < srs.Length; i++)
		{
			srs[i].DOFade(0f, 0.3f);
		}
		yield return new WaitForSeconds(0.3f);
		Destroy(gameObject);
	}

	
}
