﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemSmallTube : MonoBehaviour 
{
	public SpriteRenderer waterSprite;
	public float hideY;
	public float showY;
	public Color waterColor;

	Vector3 firstPos;

	public static ChemSmallTube Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		firstPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SetWaterColor(Color color)
	{
		waterColor = color;
		waterSprite.color = color;
	}

	public IEnumerator FillTubeTask()
	{
		yield return waterSprite.transform.DOLocalMoveY(showY, 3f).WaitForCompletion();
	}

	public void PutBackOnHolder()
	{
		SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
		waterSprite.transform.localPosition = new Vector3(waterSprite.transform.localPosition.x, hideY, waterSprite.transform.localPosition.z);
		for (int i = 0; i < sprites.Length; i++)
		{
			Utility.changeAlphaSprite(sprites[i], 0);
		}
		transform.position = firstPos;
		for (int i = 0; i < sprites.Length; i++)
		{
			sprites[i].DOFade(1f, 0.3f);
		}
	}
}
