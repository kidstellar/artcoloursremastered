﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemPump : MonoBehaviour 
{
	bool playing = false;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}


	void OnMouseUpAsButton()
	{
		if(playing)
		{
			return;
		}
		playing = true;
		StartCoroutine(PlayTask());
		ChemBigTube.Instance.StartToFill();
	}

	IEnumerator PlayTask()
	{
		Vector3 firstScale = transform.localScale;
		for (int i = 0; i < 3; i++)
		{
			yield return transform.DOScale(new Vector3(firstScale.x, firstScale.y * 0.6f, firstScale.z), 0.4f).SetEase(Ease.InSine).WaitForCompletion();
			yield return transform.DOScale(firstScale, 0.4f).SetEase(Ease.OutSine).WaitForCompletion();
		}
		playing = false;
	}
}
