﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalScrollDragDrop : MonoBehaviour 
{
	public ScrollRect scrollRect;
	public GraphicRaycaster gr;

	bool holding = false;
	bool holdingElement = false;
	bool carrying = false;
	Vector3 holdPos;

	ChemTubeUI currentChemTubeUI = null;
	ChemTube currentChemTube;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(HasHoldOntoScroll() && !holding && !carrying)
		{
			holding = true;
			currentChemTubeUI = GetChemTubeOnMouse();
			if(currentChemTubeUI)
			{
				holdingElement = true;
				holdPos = Utility.mouseToWorldPosition();
			}
		}
		else if(holdingElement && !carrying)
		{
			Vector3 deltaPos = Utility.mouseToWorldPosition() - holdPos;
			if(deltaPos.y > 0.1f)
			{
				carrying = true;
				scrollRect.enabled = false;
				currentChemTube = currentChemTubeUI.InstantiateChemTube();
			}
			else if(Mathf.Abs(deltaPos.x) > 0.2f)
			{
				ResetHoldScroll();
			}
		}
		else if(holding && !carrying && Input.GetMouseButtonUp(0))
		{
			ResetHoldScroll();
		}
		else if(holding && carrying && Input.GetMouseButtonUp(0))
		{
			ChemBeaker chemBeaker = Utility.getColliderAt<ChemBeaker>(Utility.mouseToWorldPosition());
			if(chemBeaker && !chemBeaker.IsFilling() )
			{
				currentChemTube.Unfill(chemBeaker);
			}
			else
			{
				Destroy(currentChemTube.gameObject);
			}
			currentChemTube = null;
			ResetHoldScroll();
		}
		else if(holding && carrying)
		{
			currentChemTube.transform.position = Utility.mouseToWorldPosition();
		}
	}

	void ResetHoldScroll()
	{
		scrollRect.enabled = true;
		holding = false;
		holdingElement = false;
		carrying = false;
	}

	bool HasHoldOntoScroll()
	{
		return Input.GetMouseButtonDown(0) && Utility.getUIElementAt<ScrollRect>(gr, Input.mousePosition) == scrollRect;
	}

	ChemTubeUI GetChemTubeOnMouse()
	{
		return Utility.getUIElementAt<ChemTubeUI>(gr, Input.mousePosition);
	}
}
