﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SmallTube : MonoBehaviour 
{
	public SpriteRenderer liquidSprite;
	public ParticleSystem waterParticle;
	public float fillSpeed;
	bool filling = false;

	Vector3 firstPos;
	// Use this for initialization
	void Start () 
	{
		firstPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void FillLiquid(Color color)
	{
		StartCoroutine(FillLiquidTask(color));
	}

	public bool IsFilling()
	{
		return filling;
	}

	IEnumerator FillLiquidTask(Color color)
	{
		filling = true;
		ChangeParticleColor(color);
		SetParticleEmission(100);
		liquidSprite.color = color;
		liquidSprite.transform.DOKill();
		yield return liquidSprite.transform.DOLocalMoveY(-0.33f, fillSpeed).SetEase(Ease.OutSine).SetSpeedBased().WaitForCompletion();
		SetParticleEmission(0);
		
		yield return transform.DOMove(new Vector3(7.33f, 5.23f), 1f).WaitForCompletion();
		FadeAllSprites(0f, 0.1f);
		yield return new WaitForSeconds(0.1f);
		transform.position = firstPos;
		liquidSprite.transform.localPosition = new Vector3(0f, -1.23f);
		FadeAllSprites(1f, 0.5f);
		filling = false;
		ExtractionMachine.Instance.CheckForAllSmallTubesArrived();
	}

	void FadeAllSprites(float fadeAmount, float duration)
	{
		SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
		for (int i = 0; i < srs.Length; i++)
		{
			srs[i].DOKill();
			srs[i].DOFade(fadeAmount, duration);
		}
	}

	void ChangeParticleColor(Color color)
	{
		var main = waterParticle.main;
		main.startColor = color;
	}

	void SetParticleEmission(float value)
	{
		var emission = waterParticle.emission;
		emission.rateOverTime = value;
	}

}
