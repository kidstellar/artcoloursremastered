﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FlowerPot : MonoBehaviour 
{
	public Transform placesParent;
	
	List<FlowerSeed> collectedFlowers;
	int placeIndex = 0;

	public static FlowerPot Instance;
	void Awake()
	{
		Instance = this;
		collectedFlowers = new List<FlowerSeed>();
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public float AbsorbAllFlowersToDrone()
	{
		StartCoroutine(AbsorbAllFlowersToDroneTask());
		placeIndex = 0;
		return 1.2f + 0.3f * collectedFlowers.Count;
	}

	IEnumerator AbsorbAllFlowersToDroneTask()
	{
		for (int i = 0; i < collectedFlowers.Count; i++)
		{
			StartCoroutine(collectedFlowers[i].AbsorbByDroneTask());
			yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
		}
		collectedFlowers.Clear();
	}

	public bool IsCapacityFull()
	{
		return collectedFlowers.Count >= 8;
	}

	public void PutFlower(FlowerSeed flowerSeed)
	{
		flowerSeed.transform.DOKill();
		flowerSeed.transform.position = placesParent.GetChild(placeIndex).position;
		flowerSeed.Bounce(Ease.OutBack, 0.3f);
		placeIndex++;
		collectedFlowers.Add(flowerSeed);
	}

	public void OnButtonClick()
	{
		if(collectedFlowers.Count > 0 && !Drone.Instance.IsTravelling())
		{
			Drone.Instance.GoToCollect();
		}
	}

}
