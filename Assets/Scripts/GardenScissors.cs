﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GardenScissors : MonoBehaviour 
{
	public SpriteRenderer scissorsSprite;

	bool holding = false;
	int phase = 0;
	
	//0 for idle, 1 for preparing for cut

	float phaseTimer = 0;
	DyeField currentDyeField;
	Vector3 deltaPos;
	Vector3 firstPos;
	Vector3 firstScale;
	int firstSortingOrder;
	// Use this for initialization
	void Start () 
	{
		firstPos = transform.position;
		firstScale = scissorsSprite.transform.localScale;
		firstSortingOrder = scissorsSprite.sortingOrder;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(holding)
		{
			transform.position = Utility.mouseToWorldPosition() - deltaPos;
			CheckForDyeField();
		}
	}

	void CheckForDyeField()
	{
		DyeField dyeField = Utility.getColliderAt<DyeField>(Utility.mouseToWorldPosition());
		if(phase == 0 && dyeField && dyeField.IsAvailableForCutting())
		{
			phase = 1;
			phaseTimer = 0f;
			currentDyeField = dyeField;
			scissorsSprite.transform.DOScale(firstScale * 1.5f, 0.5f);
			scissorsSprite.transform.DOShakePosition(0.5f, Vector2.one * 0.0f, 20, 5, false, false);
		}
		else if(phase == 1 && dyeField)
		{
			phaseTimer += Time.deltaTime;
			if(phaseTimer >= 0.5f)
			{
				phase = 0;
				phaseTimer = 0f;
				scissorsSprite.transform.DOKill();
				scissorsSprite.transform.localPosition = Vector3.zero;
				scissorsSprite.transform.localScale = firstScale;
				scissorsSprite.transform.DOLocalRotate(Vector3.forward * 720f, 0.4f, RotateMode.FastBeyond360).SetEase(Ease.OutBack);
				currentDyeField.CutOffFlower();
				currentDyeField = null;
			}
		}
		else if(phase == 1 && !dyeField)
		{
			phase = 0;
			phaseTimer = 0f;
			currentDyeField = null;
			scissorsSprite.transform.DOKill();
			scissorsSprite.transform.localPosition = Vector3.zero;
			scissorsSprite.transform.localScale = firstScale;
			scissorsSprite.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
		}
	}



	private void OnMouseDown()
	{
		if(holding)
		{
			return;
		}
		deltaPos = Utility.mouseToWorldPosition() - transform.position;
		holding = true;
		scissorsSprite.sortingOrder = 600;
		scissorsSprite.transform.DOScale(firstScale * 1.2f, 0.2f);
		scissorsSprite.transform.DOLocalRotate(Vector3.forward * 720f, 0.75f, RotateMode.FastBeyond360).SetEase(Ease.OutBack);
	}

	private void OnMouseUp()
	{
		if(!holding)
		{
			return;
		}
		
		holding = false;
		phase = 0;
		phaseTimer = 0f;
		currentDyeField = null;
		transform.position = firstPos;
		transform.DOKill(true);
		transform.DOPunchScale(Vector3.one * 0.2f, 1f, 6).SetEase(Ease.OutElastic);
		scissorsSprite.sortingOrder = firstSortingOrder;
	}
}
