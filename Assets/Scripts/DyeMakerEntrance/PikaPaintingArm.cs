﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PikaPaintingArm : MonoBehaviour 
{
	public float angle = 15f;
	public float duration = 1f;
	// Use this for initialization
	void Start () 
	{
		transform.DORotate(Vector3.forward * angle, duration).SetRelative().SetLoops(-1, LoopType.Yoyo);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
