﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BigTube : MonoBehaviour 
{
	public SmallTube smallTube;
	public Transform pathParent;
	public SpriteRenderer liquidSprite;
	public GameObject liquidPrefab;
	public Color currentColor;
	public int liquidOrder;
	public float fillSpeed = 2.5f;
	public float unfillSpeed = 2.5f;
	public float[] gradualHeights;

	int phase = 0;
	bool colorSetted = false;
	bool filled = false;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.K)) 
		{

		}
	   	else if (Input.GetKeyDown(KeyCode.U)) 
	   	{
			UnfillLiquid();
		}
	}

	public bool IsFilled()
	{
		return filled;
	}

	public void SetColor(Color color)
	{
		colorSetted = true;
		currentColor = color;
	}

	public bool FillByValve()
	{
		if(phase >= gradualHeights.Length || !colorSetted) 
		{
			return false;
		}
		StartCoroutine(FillTubeTask());
		return true;
	}

	public void FillLiquid(Color color)
	{
		liquidSprite.color = color;
		liquidSprite.transform.DOKill();
		liquidSprite.GetComponent<_2dxFX_Wave>().enabled = true;
		liquidSprite.transform.DOLocalMoveY(gradualHeights[phase], fillSpeed).SetEase(Ease.OutSine).SetSpeedBased();
	}

	public void UnfillLiquid()
	{
		liquidSprite.transform.DOKill();
		liquidSprite.transform.DOLocalMoveY(-3.13f, unfillSpeed).SetEase(Ease.OutSine).SetSpeedBased().OnComplete(() => 
		{
			liquidSprite.GetComponent<_2dxFX_Wave>().enabled = false;
			filled = false;
			phase = 0;
			colorSetted = false;
		});
	}

	IEnumerator FillTubeTask()
	{
		TubeLiquid liquid = Instantiate(liquidPrefab).GetComponent<TubeLiquid>();
		liquid.SetColor(currentColor);
		liquid.pathParent = pathParent;
		liquid.SetSortingOrder(liquidOrder);
		yield return liquid.SendLiquidTask();
		//yield return new WaitForSeconds(0.25f);
	   	if(phase < gradualHeights.Length) 
	   	{
			FillLiquid(currentColor);
			phase++;
			if(phase == gradualHeights.Length)
			{
				ExtractionMachine.Instance.bigValve.DeactivateAnim();
				filled = true;
			}
		}
		
	}

	public void OnMouseUpAsButton()
	{
		if(!filled)
		{
			return;
		}
		UnfillLiquid();
		smallTube.FillLiquid(currentColor);
	}


}
