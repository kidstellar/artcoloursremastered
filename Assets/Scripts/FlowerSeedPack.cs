﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FlowerSeedPack : MonoBehaviour 
{
	public GameObject flowerSeedPrefab;
	public Transform flowerSeedParent;

	FlowerSeed currentFlowerSeed;
	bool holding = false;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(holding)
		{
			currentFlowerSeed.transform.position = Utility.mouseToWorldPosition() + new Vector3(0f, 0.1f);
		}
	}

	private void OnMouseDown()
	{
		if(holding)
		{
			return;
		}
		transform.DOKill(true);
		transform.DOPunchScale(Vector3.one * 0.2f, 1f, 6).SetEase(Ease.OutElastic);
		currentFlowerSeed = Instantiate(flowerSeedPrefab, flowerSeedParent).GetComponent<FlowerSeed>();
		holding = true;
	}

	private void OnMouseUp()
	{
		if(!holding)
		{
			return;
		}
		DyeField dyeField = Utility.getColliderAt<DyeField>(Utility.mouseToWorldPosition());
		if(dyeField && !dyeField.HasAlreadyPlanted() && !dyeField.IsDried())
		{
			dyeField.PlantFlower(currentFlowerSeed);
		}
		else
		{
			Destroy(currentFlowerSeed.gameObject);
		}
		
		holding = false;
	}
}
