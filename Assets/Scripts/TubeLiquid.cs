﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TubeLiquid : MonoBehaviour 
{
	public Transform pathParent;
	public TrailRenderer trailRenderer;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void SetSortingOrder(int order)
	{
		trailRenderer.sortingOrder = order;
	}

	public void SetColor(Color color)
	{
		trailRenderer.startColor = color;
		trailRenderer.endColor = color;
		trailRenderer.Clear();
	}

	public IEnumerator SendLiquidTask()
	{
		for (int i = 0; i < pathParent.childCount; i++) 
		{
			Vector3[] pathArray = new Vector3[pathParent.GetChild(i).childCount];
			for (int j = 0; j < pathParent.GetChild(i).childCount; j++) 
			{
				pathArray[j] = pathParent.GetChild(i).GetChild(j).position;

			}
			trailRenderer.enabled = false;
			transform.position = pathArray[0];
			trailRenderer.enabled = true;
			trailRenderer.Clear();
			yield return transform.DOPath(pathArray, 5f, PathType.Linear, PathMode.Sidescroller2D).SetSpeedBased().WaitForCompletion();
			if(i < pathParent.childCount - 1) 
			{
				yield return new WaitForSeconds(trailRenderer.time);
			}
		}

		
	}



}
