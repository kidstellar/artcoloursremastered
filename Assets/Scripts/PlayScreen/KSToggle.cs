﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class KSToggle : MonoBehaviour 
{
    public Image cursorRect;
    public float minWidth;
    public float maxWidth;
    public Color enabledColor;
    public Color disabledColor;
    public KSToggleEvent onToggled;

    public bool isToggled = true;
    // Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void toggleOff()
    {
        cursorRect.DOKill();
        cursorRect.DOColor(disabledColor, 0.2f);
        cursorRect.rectTransform.DOAnchorPosX(minWidth, 0.2f);
        isToggled = false;
    }

    public void toggleOn()
    {
        cursorRect.DOKill();
        cursorRect.DOColor(enabledColor, 0.2f);
        cursorRect.rectTransform.DOAnchorPosX(maxWidth, 0.2f);
        isToggled = true;
    }

    public void onCursorClick()
    {
        if(isToggled)
        {
            toggleOff();
        }
        else
        {
            toggleOn();
        }

        onToggled.Invoke(isToggled);
    }
}

[System.Serializable]
public class KSToggleEvent : UnityEvent<bool> { }
