﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayScreen : MonoBehaviour {

    public Image fluBGImage;
    public RectTransform settingsPanel;
    public RectTransform playButtonRect;

    public KSToggle musicToggle;
    public KSToggle soundToggle;
    // Use this for initialization
    void Start()
    {
        //SlimeBookManager.Instance.checkForActivate();
        Application.targetFrameRate = 60;
        Input.multiTouchEnabled = false;
        checkForMusicAndSound();
        if (MuteManager.Instance)
        {
            MuteManager.Instance.checkForSoundMute();
        }
        //if(A2SPadaManager.Instance)
        //{
        //    A2SPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //A1FHalloweenPlayScreen.Instance.checkForHalloween();
        playButtonRect.DOScale(playButtonRect.transform.localScale * 1.2f, 0.7f).SetLoops(-1, LoopType.Yoyo);
        StartCoroutine(playScreenStartTask());
    }

    // Update is called once per frame
    void Update()
    {

    }

    void checkForMusicAndSound()
    {
        if (!PlayerPrefs.HasKey("artcolors1_music_on"))
        {
            PlayerPrefs.SetInt("artcolors1_music_on", 1);
        }

        if (!PlayerPrefs.HasKey("artcolors1_sound_on"))
        {
            PlayerPrefs.SetInt("artcolors1_sound_on", 1);
        }
        int music = PlayerPrefs.GetInt("artcolors1_music_on");
        int sound = PlayerPrefs.GetInt("artcolors1_sound_on");
        if (music == 1)
        {
            musicToggle.toggleOn();
        }
        else
        {
            musicToggle.toggleOff();
        }

        if (sound == 1)
        {
            soundToggle.toggleOn();
        }
        else
        {
            soundToggle.toggleOff();
        }
    }

    public void updateMusicPref()
    {
        if (musicToggle.isToggled)
        {
            PlayerPrefs.SetInt("artcolors1_music_on", 1);
        }
        else
        {
            PlayerPrefs.SetInt("artcolors1_music_on", 0);
        }
        MuteManager.Instance.checkForSoundMute();
    }

    public void updateSoundPref()
    {
        if (soundToggle.isToggled)
        {
            PlayerPrefs.SetInt("artcolors1_sound_on", 1);
        }
        else
        {
            PlayerPrefs.SetInt("artcolors1_sound_on", 0);
        }
        MuteManager.Instance.checkForSoundMute();
    }

    IEnumerator playScreenStartTask()
    {
        //yield return new WaitForSeconds(0.5f);
        if (FirebaseManager.Instance)
        {
           FirebaseManager.Instance.changeScreen("ART1C_PlayScreen", "ART1C_PlayScreen");
        }
        //if (A1FPadaManager.Instance)
        //{
        //    A1FPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //yield return FirebaseManager.Instance.fetchRemoteConfigTask();
        yield return FaderController.Instance.unfadeScreen(0.3f);
    }

    public void goTomap()
    {
        playButtonRect.transform.DOKill(true);
        playButtonRect.transform.DOPunchScale(new Vector3(0.07f, 0.07f, 0.07f), 1f, 6, 2f);
        StartCoroutine(goToScene("ColorsPreLoading"));
    }

    IEnumerator goToScene(string sceneName)
    {
        yield return FaderController.Instance.fadeScreen(0.3f);
        SceneManager.LoadScene(sceneName);
    }

    public void showSettings()
    {
        settingsPanel.DOKill();
        settingsPanel.DOAnchorPosY(0f, 1f).SetEase(Ease.OutQuart);
        fluBGImage.DOKill();
        fluBGImage.raycastTarget = true;
        fluBGImage.DOFade(0.5f, 0.5f);

    }

    public void hideSettings()
    {
        settingsPanel.DOKill();
        settingsPanel.DOAnchorPosY(600f, 1f).SetEase(Ease.InQuad);
        fluBGImage.DOKill();
        fluBGImage.raycastTarget = false;
        fluBGImage.DOFade(0f, 0.5f);
    }

    public void onBookButtonClick()
    {
        //SlimeBookController.Instance.onBookClick();
    }
}
