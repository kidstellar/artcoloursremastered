﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoreGameManager : MonoBehaviour {

    public Button moreGamesButton;
    public Button settingsButton;
    public Button bookButton;

    public RectTransform moreGamesPanel;
    public Image fluBGImage;
    public ScrollRect iconScroll;
    // Use this for initialization
    void Start()
    {
        animateMGButton();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void animateMGButton()
    {
        moreGamesButton.transform.DOKill();
        moreGamesButton.transform.localScale = Vector3.one * 0.38f;
        moreGamesButton.transform.DOScale(moreGamesButton.transform.localScale * 1.1f, 1f).SetLoops(-1, LoopType.Yoyo);
    }

    public void onClickGameIcon(string url)
    {
        Application.OpenURL(url);
    }

    public void showMoreGames()
    {
        fluBGImage.DOKill();
        fluBGImage.DOFade(1f, 0.5f);
        fluBGImage.raycastTarget = true;
        moreGamesPanel.gameObject.SetActive(true);

        moreGamesButton.gameObject.SetActive(false);
        settingsButton.gameObject.SetActive(false);
        bookButton.gameObject.SetActive(false);
    }

    public void hideMoreGames()
    {
        moreGamesButton.gameObject.SetActive(true);
        settingsButton.gameObject.SetActive(true);
        bookButton.gameObject.SetActive(true);

        fluBGImage.DOKill();
        fluBGImage.DOFade(0f, 0.5f);
        fluBGImage.raycastTarget = false;

        moreGamesPanel.gameObject.SetActive(false);
        animateMGButton();

    }
}
