﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ParentalGateManager : MonoBehaviour
{
    public bool parentEnabled = true;

    public Button parentButton;
    public Button feedbackButton;
    public Button closeButton;
    public Canvas parentalCanvas;
    public RectTransform passwordPanel;
    public RectTransform parentalMenuPanel;
    public RectTransform moreGamesPanel;
    public Text[] passwordInputDigits;

    public AudioSource okClickAudio;
    public AudioSource cancelClickAudio;

    private string password = "";
    private int passwordIndex = 0;
    bool directMoreGames = false;
    // Use this for initialization
    void Start()
    {
        if (!parentEnabled)
        {
            parentButton.gameObject.SetActive(false);
        }
        else
        {
            parentButton.transform.DOScale(parentButton.transform.localScale * 1.2f, 1f).SetLoops(-1, LoopType.Yoyo);
        }


    }

    // Update is called once per frame
    void Update()
    {

    }

    bool isPasswordValid()
    {
        if (password.Length < 4)
        {
            return false;
        }
        int currentYear = DateTime.Now.Year;
        int result = currentYear - int.Parse(password);
        return result >= 18 && result < 150;
    }

    void openParentGate()
    {
        //if (FirebaseManager.Instance.isParentalFeedbackEnabled())
        //{
        //    feedbackButton.interactable = true;
        //}
        passwordPanel.gameObject.SetActive(true);
        moreGamesPanel.gameObject.SetActive(false);
        parentalMenuPanel.gameObject.SetActive(false);
        clearDigits();
        updateDigits();
        parentalCanvas.gameObject.SetActive(true);
    }

    void closeParentGate()
    {
        parentalCanvas.gameObject.SetActive(false);
    }

    void addNumber(int number)
    {
        if (password.Length < 4)
        {
            password += number;
            passwordIndex++;
        }
    }

    void clearDigits()
    {
        password = "";
    }

    void deleteNumber()
    {
        if (password.Length > 0)
        {
            password = password.Remove(password.Length - 1);
            passwordIndex--;
        }
    }

    void updateDigits()
    {
        for (int i = 0; i < passwordInputDigits.Length; i++)
        {
            passwordInputDigits[i].text = "";
        }
        for (int i = 0; i < password.Length; i++)
        {
            passwordInputDigits[i].text = password[i].ToString();
        }
    }

    void decideAfterPassword()
    {
        if (directMoreGames)
        {
            passwordPanel.gameObject.SetActive(false);
            onMoreGamesClick();
        }
        else
        {
            passwordPanel.gameObject.SetActive(false);
            parentalMenuPanel.gameObject.SetActive(true);
        }
    }

    public void onContactUsClick()
    {
        Application.OpenURL("mailto:wonderwood@ageofkids.com");
       okClickAudio.Play();
    }

    public void onFeedbackClick()
    {
        Application.OpenURL("https://ageofkids.com/feedback_form");
       okClickAudio.Play();
    }

    public void onMoreGamesClick()
    {
        parentalMenuPanel.gameObject.SetActive(false);
        closeButton.gameObject.SetActive(false);
        moreGamesPanel.gameObject.SetActive(true);
        okClickAudio.Play();

    }

    public void onMoreGamesClick2()
    {
        directMoreGames = true;
        openParentGate();
        okClickAudio.Play();
    }

    public void onCloseMoreGamesClick()
    {
        if (!directMoreGames)
        {
            parentalMenuPanel.gameObject.SetActive(true);
            closeButton.gameObject.SetActive(true);
            moreGamesPanel.gameObject.SetActive(false);
            cancelClickAudio.Play();
        }
        else
        {
            onCloseButtonClick();
            closeButton.gameObject.SetActive(true);
        }

    }

    public void onParentButtonClick()
    {
        directMoreGames = false;
        openParentGate();
        okClickAudio.Play();
    }

    public void onCloseButtonClick()
    {
        closeParentGate();
        cancelClickAudio.Play();
    }

    public void onNumberClick(int number)
    {
        addNumber(number);
        updateDigits();
        if (isPasswordValid())
        {
            decideAfterPassword();
        }
        okClickAudio.Play();
    }

    public void onDeleteNumberClick()
    {
        deleteNumber();
        updateDigits();
        cancelClickAudio.Play();
    }

    public void onClearNumbersClick()
    {
        clearDigits();
        updateDigits();
    }
}
