﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChemistrySetManager : MonoBehaviour 
{
	public RectTransform tubeListRect;
	public GameObject tubeUIPrefab;
	public RectTransform tubeUIContent;

	bool alreadyLoading;

	public static ChemistrySetManager Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		InitTubeUIList();
		if (MuteManager.Instance)
        {
            MuteManager.Instance.checkForSoundMute();
        }
        //if(A2SPadaManager.Instance)
        //{
        //    A2SPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        StartCoroutine(mapStartTask());
	}

	
	// Update is called once per frame
	void Update () 
	{

	}

	public void ShowTubeList()
	{
		tubeListRect.DOKill();
		tubeListRect.DOAnchorPosY(56f, 0.5f);
	}

	public void HideTubeList()
	{
		tubeListRect.DOKill();
		tubeListRect.DOAnchorPosY(-76f, 0.5f);
	}

	void UpdateContentWidth()
	{
		tubeUIContent.sizeDelta = new Vector2(tubeUIContent.childCount * 194f + (tubeUIContent.childCount - 1) * 30f, tubeUIContent.sizeDelta.y);
	}

	void InitTubeUIList()
	{
		for (int i = 0; i < ColorManager.Instance.colors.Length; i++)
		{
			AddTubeUI(ColorManager.Instance.colors[i].wonderwoodColor);
		}
		UpdateContentWidth();
	}

	void AddTubeUI(WonderwoodColor wwColor)
	{
		ChemTubeUI chemTubeUI = Instantiate(tubeUIPrefab, tubeUIContent).GetComponent<ChemTubeUI>();
		chemTubeUI.wwColor = wwColor;
	}

	public void OnBackButtonClick()
	{
		LoadScene("DyeLab");
	}

	void LoadScene(string sceneName)
	{
		if(alreadyLoading)
		{
			return;
		}
		alreadyLoading = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen(0.3f);
		SceneManager.LoadScene(sceneName);
	}

	IEnumerator mapStartTask()
    {
        //yield return new WaitForSeconds(0.5f);
        //if (FirebaseManager.Instance)
        //{
        //    FirebaseManager.Instance.changeScreen("A2S_PlayScreen", "A2S_PlayScreen");
        //}
		if (FirebaseManager.Instance)
        {
           FirebaseManager.Instance.changeScreen("ART1C_ChemistrySet", "ART1C_ChemistrySet");
        }
        //if (A1FPadaManager.Instance)
        //{
        //    A1FPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //yield return FirebaseManager.Instance.fetchRemoteConfigTask();
        yield return FaderController.Instance.unfadeScreen(0.3f);
    }
}
