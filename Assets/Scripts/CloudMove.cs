﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CloudMove : MonoBehaviour 
{
	public float startX;
	public float endX;
	public float speed;
	// Use this for initialization
	void Start () 
	{
		StartCoroutine(MoveTask());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	IEnumerator MoveTask()
	{
		while(true)
		{
			yield return transform.DOLocalMoveX(endX, speed).SetSpeedBased().WaitForCompletion();
			transform.localPosition = new Vector3(startX, transform.localPosition.y, transform.localPosition.z);
		}
	}
}
