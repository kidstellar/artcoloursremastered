﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DyeLabInterior : MonoBehaviour 
{
	public Transform extractionTransform;
	public Transform chemistryTransform;

	bool alreadyLoading = false;
	// Use this for initialization
	void Start () 
	{
		if (MuteManager.Instance)
        {
            MuteManager.Instance.checkForSoundMute();
        }
        //if(A2SPadaManager.Instance)
        //{
        //    A2SPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        StartCoroutine(mapStartTask());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	IEnumerator mapStartTask()
    {
        //yield return new WaitForSeconds(0.5f);
        //if (FirebaseManager.Instance)
        //{
        //    FirebaseManager.Instance.changeScreen("A2S_PlayScreen", "A2S_PlayScreen");
        //}
		if (FirebaseManager.Instance)
        {
           FirebaseManager.Instance.changeScreen("ART1C_DyeLabInterior", "ART1C_DyeLabInterior");
        }
        //if (A1FPadaManager.Instance)
        //{
        //    A1FPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //yield return FirebaseManager.Instance.fetchRemoteConfigTask();
        yield return FaderController.Instance.unfadeScreen(0.3f);
    }

	public void OnBackButtonClick()
	{
		LoadScene("DyeMakerEntrance");
	}

	public void GoToExtractionMachine()
	{
		extractionTransform.DOPunchScale(Vector2.one * 0.1f, 0.6f, 6);
		LoadScene("ExtractionMachine");
	}
	
	public void GoToChemistry()
	{
		chemistryTransform.DOPunchScale(Vector2.one * 0.1f, 0.6f, 6);
		LoadScene("Chemistry");
	}

	public void LoadScene(string sceneName)
	{
		if(alreadyLoading)
		{
			return;
		}
		alreadyLoading = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen(0.3f);
		SceneManager.LoadScene(sceneName);
	}
}
