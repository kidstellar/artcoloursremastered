﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Spine;
using Spine.Unity;
using UnityEngine.UI;
using DG.Tweening;

public class ColorsPreLoading : MonoBehaviour
{

    public string nextSceneName = "ColorsMap";
    public SkeletonAnimation normalSkeleton;

    public AudioSource voiceSource;
    public Button skipButton;

    private SkeletonAnimation characterSkeleton;
    private AsyncOperation asyncLoad;
    private bool readyForGo = false;
    private bool alreadySkipped = false;
    private Coroutine preLoadingCo;
    // Use this for initialization
    void Start()
    {


            characterSkeleton = normalSkeleton;

        // if(SlimeBookManager.Instance)
        // {
        //    SlimeBookManager.Instance.checkForActivate();
        // }

        if (MuteManager.Instance)
        {
            MuteManager.Instance.checkForSoundMute();
        }
        preLoadingCo = StartCoroutine(preLoadingTask());
    }

    // Update is called once per frame
    void Update()
    {

    }

    void activeteSkipButton()
    {
        if(PlayerPrefs.GetInt("a1c_preloading_skip") == 1)
        {
            skipButton.GetComponent<Image>().DOFade(1f, 0.5f);
            skipButton.GetComponent<Image>().raycastTarget = true;
        }

    }



    IEnumerator preLoadingTask()
    {
        yield return FaderController.Instance.unfadeScreen(0.5f);
        yield return new WaitForSeconds(0.25f);
        playTalk();
        voiceSource.Play();
        asyncLoad = SceneManager.LoadSceneAsync(nextSceneName);
        asyncLoad.allowSceneActivation = false;
        float totalTimer = 0f;
        while(asyncLoad.progress < 0.9f)
        {
            totalTimer += Time.deltaTime;
            yield return null;
        }
        activeteSkipButton();
        readyForGo = true;
        float waitDifferenceTime = Mathf.Max(0.1f, voiceSource.clip.length - totalTimer);
        yield return new WaitForSeconds(waitDifferenceTime);
        playIdle();
        if(!alreadySkipped)
        {
            readyForGo = false;
            yield return new WaitForSeconds(0.25f);
            PlayerPrefs.SetInt("a1c_preloading_skip", 1);
            yield return fadeAndGO();

        }

    }

    IEnumerator fadeAndGO()
    {
        yield return FaderController.Instance.fadeScreen(0.5f);
        asyncLoad.allowSceneActivation = true;
    }

    public float playIdle()
    {
        characterSkeleton.timeScale = 1f;
        TrackEntry animTrack = characterSkeleton.state.SetAnimation(0, "idle", true);
        return animTrack.Animation.Duration;
    }

    public float playTalk()
    {
        characterSkeleton.timeScale = 1;
        TrackEntry animTrack = characterSkeleton.state.SetAnimation(0, "talk", true);
        return animTrack.Animation.Duration;
    }

    public void onSkipClick()
    {
        if(readyForGo)
        {
            alreadySkipped = true;
            StartCoroutine(fadeAndGO());
        }
    }
}
