﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemLiquid : MonoBehaviour 
{
	public TrailRenderer trailRenderer;
	public Transform pathParent;
	bool travelling = false;

	Vector3 firstPos;
	// Use this for initialization
	void Start () 
	{
		firstPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public bool IsTravelling()
	{
		return travelling;
	}

	public void SetColor(Color color)
	{
		trailRenderer.startColor = color;
		trailRenderer.endColor = color;
	}

	public IEnumerator TravelTask()
	{
		travelling = true;
		Vector3[] pathArray = new Vector3[pathParent.childCount];
		for (int j = 0; j < pathParent.childCount; j++) 
		{
			pathArray[j] = pathParent.GetChild(j).position;
		}
		yield return transform.DOPath(pathArray, 5f, PathType.Linear, PathMode.Sidescroller2D).SetSpeedBased().WaitForCompletion();
		travelling = false;
		
	}

	public void Reset()
	{
		trailRenderer.enabled = false;
		transform.position = firstPos;
		trailRenderer.enabled = true;
		trailRenderer.Clear();
	}
}
