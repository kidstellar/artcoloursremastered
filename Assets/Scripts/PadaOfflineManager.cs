﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PADA;
using System.IO;

public class PadaOfflineManager : MonoBehaviour
{
    public string padaOfflineDataFolder = "padaOfflineDatas";
    public string sessionFileName = "session.txt";
    public string favFileName = "fav.txt";
    public string knowledgeFileName = "knowledges.txt";
    public string abilityFileName = "abilities.txt";


    public static PadaOfflineManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Debug.Log(Application.persistentDataPath);
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void appendSessions(List<RequestPack> sessions)
    {
        string result = "";
        for (int i = 0; i < sessions.Count; i++)
        {
            result += sessions[i].jsonString;
            result += ",";

        }
        if (!Directory.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + padaOfflineDataFolder);
            File.Create(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + sessionFileName).Close();
        }
        File.AppendAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + sessionFileName, result);
    }

    public void appendFavs(List<RequestPack> favs)
    {
        string result = "";
        for (int i = 0; i < favs.Count; i++)
        {
            result += favs[i].jsonString;
            result += ",";

        }
        if (!Directory.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + padaOfflineDataFolder);
            File.Create(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + favFileName).Close();
        }
        File.AppendAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + favFileName, result);
    }

    public void appendKnowledges(List<RequestPack> knows)
    {
        string result = "";
        for (int i = 0; i < knows.Count; i++)
        {
            result += knows[i].jsonString;
            result += ",";

        }
        if (!Directory.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + padaOfflineDataFolder);
            File.Create(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + knowledgeFileName).Close();
        }
        File.AppendAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + knowledgeFileName, result);
    }

    public void appendAbilities(List<RequestPack> knows)
    {
        string result = "";
        for (int i = 0; i < knows.Count; i++)
        {
            result += knows[i].jsonString;
            result += ",";

        }
        if (!Directory.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + padaOfflineDataFolder);
            File.Create(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + abilityFileName).Close();
        }
        File.AppendAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + abilityFileName, result);
    }

    public IEnumerator sendOfflineDatasToServerTask()
    {
        if (File.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + sessionFileName))
        {
            string sessionData = File.ReadAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + sessionFileName);
            if (sessionData.Length >= 3)
            {
                sessionData = "[" + sessionData.Remove(sessionData.Length - 1) + "]";
                Debug.Log("Session Data :::::::::: " + sessionData);
                yield return A1CWebRequester.Instance.PostRequestForOfflineData(A1CPadaManager.Instance.hostName + A1CPadaManager.Instance.hostLocation + A1CPadaManager.Instance.sessionPrefix,
                                                                               sessionData, 0, clearSessionTextFile);
            }
        }

        if (File.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + favFileName))
        {
            string favData = File.ReadAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + favFileName);
            if (favData.Length >= 3)
            {
                favData = "[" + favData.Remove(favData.Length - 1) + "]";
                Debug.Log("Fav Data :::::::::: " + favData);
                yield return A1CWebRequester.Instance.PostRequestForOfflineData(A1CPadaManager.Instance.hostName + A1CPadaManager.Instance.hostLocation + A1CPadaManager.Instance.favPrefix,
                                                                               favData, 1, clearFavsTextFile);
            }
        }


        if (File.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + knowledgeFileName))
        {
            string knowData = File.ReadAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + knowledgeFileName);
            if (knowData.Length >= 3)
            {
                knowData = "[" + knowData.Remove(knowData.Length - 1) + "]";
                Debug.Log("Know Data :::::::::: " + knowData);
                yield return A1CWebRequester.Instance.PostRequestForOfflineData(A1CPadaManager.Instance.hostName + A1CPadaManager.Instance.hostLocation + A1CPadaManager.Instance.knowledgePrefix,
                                                                                  knowData, 2, clearKnowsTextFile);
            }
        }

        if (File.Exists(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + abilityFileName))
        {
            string abilityData = File.ReadAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + abilityFileName);
            if (abilityData.Length > 3)
            {
                abilityData = "[" + abilityData.Remove(abilityData.Length - 1) + "]";
                Debug.Log("Ability Data :::::::::: " + abilityData);
                yield return A1CWebRequester.Instance.PostRequestForOfflineData(A1CPadaManager.Instance.hostName + A1CPadaManager.Instance.hostLocation + A1CPadaManager.Instance.abilityPrefix,
                                                                               abilityData, 3, clearAbilitiesTextFile);
            }
        }

    }

    void clearSessionTextFile(bool completed)
    {
        if (completed)
        {
            File.WriteAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + sessionFileName, "");
        }
    }

    void clearFavsTextFile(bool completed)
    {
        if (completed)
        {
            File.WriteAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + favFileName, "");
        }
    }

    void clearKnowsTextFile(bool completed)
    {
        if (completed)
        {
            Debug.Log("GIRDIIIIIII ::::::: ");
            File.WriteAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + knowledgeFileName, "");
        }
    }

    void clearAbilitiesTextFile(bool completed)
    {
        if (completed)
        {
            File.WriteAllText(Application.persistentDataPath + "/" + padaOfflineDataFolder + "/" + abilityFileName, "");
        }
    }
}
