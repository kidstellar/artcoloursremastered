﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DyeMakerGardenManager : MonoBehaviour 
{
	public GameObject plantParticlePrefab;

	bool alreadyLoading = false;

	public static DyeMakerGardenManager Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		StartCoroutine(mapStartTask());
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			GardenTopCover.Instance.OpenCover();
		}
		else if(Input.GetKeyDown(KeyCode.K))
		{
			GardenTopCover.Instance.CloseCover();
		}
	}

	public void GeneratePlantParticle(Vector3 pos, float duration)
	{
		Transform particleTransform = Instantiate(plantParticlePrefab).transform;
		particleTransform.position = pos;
		Destroy(particleTransform.gameObject, duration);
	}

	public void OnBackButtonClick()
	{
		LoadScene("DyeMakerEntrance");
	}

	public void LoadScene(string sceneName)
	{
		if(alreadyLoading)
		{
			return;
		}
		alreadyLoading = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen(0.3f);
		SceneManager.LoadScene(sceneName);
	}

	IEnumerator mapStartTask()
    {
        //yield return new WaitForSeconds(0.5f);
        //if (FirebaseManager.Instance)
        //{
        //    FirebaseManager.Instance.changeScreen("A2S_PlayScreen", "A2S_PlayScreen");
        //}
		if (FirebaseManager.Instance)
        {
           FirebaseManager.Instance.changeScreen("ART1C_FlowerGarden", "ART1C_FlowerGarden");
        }
        //if (A1FPadaManager.Instance)
        //{
        //    A1FPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //yield return FirebaseManager.Instance.fetchRemoteConfigTask();
        yield return FaderController.Instance.unfadeScreen(0.3f);
    }
}
