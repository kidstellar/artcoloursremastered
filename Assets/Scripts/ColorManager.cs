﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour 
{
	public ColorUnit[] colors;
	public MixColor[] mixColors;

	public static ColorManager Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }

    }
    // Use this for initialization
    void Start()
    {

    }
	
	public Color WonderwoodToColor(WonderwoodColor wwColor)
	{
		for (int i = 0; i < colors.Length; i++)
		{
			if(colors[i].wonderwoodColor == wwColor)
			{
				return colors[i].color;
			}
		}
		return Color.white;
	}

	public WonderwoodColor ColorToWonderwood(Color color)
	{
		for (int i = 0; i < colors.Length; i++)
		{
			if(colors[i].color.Equals(color))
			{
				return colors[i].wonderwoodColor;
			}
		}
		return WonderwoodColor.None;
	}

	public WonderwoodColor MixColorsForWonderwood(WonderwoodColor color1, WonderwoodColor color2)
	{
		for (int i = 0; i < mixColors.Length; i++)
		{
			if((mixColors[i].color1.Equals(color1) && mixColors[i].color2.Equals(color2)) || (mixColors[i].color1.Equals(color2) && mixColors[i].color2.Equals(color1)))
			{
				return mixColors[i].resultColor;
			}
		}
		return WonderwoodColor.None;
	}

	public WonderwoodColor MixRealColorsForWonderwood(Color color1, Color color2)
	{
		return MixColorsForWonderwood(ColorToWonderwood(color1), ColorToWonderwood(color2));
	}

	public Color MixColors(WonderwoodColor color1, WonderwoodColor color2)
	{
		for (int i = 0; i < mixColors.Length; i++)
		{
			if((mixColors[i].color1.Equals(color1) && mixColors[i].color2.Equals(color2)) || (mixColors[i].color1.Equals(color2) && mixColors[i].color2.Equals(color1)))
			{
				return WonderwoodToColor(mixColors[i].resultColor);
			}
		}
		return Color.white;
	}

	public Color MixColorsReal(Color color1, Color color2)
	{
		return MixColors(ColorToWonderwood(color1), ColorToWonderwood(color2));
	}

}

[Serializable]
public class MixColor
{
	public string name;
	public WonderwoodColor color1;
	public WonderwoodColor color2;
	public WonderwoodColor resultColor;
}

[Serializable]
public class ColorUnit
{
	public string name;
	public WonderwoodColor wonderwoodColor;
	public Color color;
}

public enum WonderwoodColor
{
	None, 
	Red, 
	Yellow, 
	Blue, 
	White, 
	Black,
	Orange,
	Purple,
	Pink,
	DarkRed,
	LightYellow,
	Green,
	OliveGreen,
	LightBlue,
	NavyBlue,
	Grey,
	Brown,
	Silver,
	Turquoise,
	Beige,
	DarkYellow,
	LightGreen
}
