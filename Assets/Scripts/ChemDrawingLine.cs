﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemDrawingLine : MonoBehaviour 
{
	public LineRenderer lineRenderer;
	public Transform pathParent;
	public Color startColor;
	public Color endColor;
	public Color mixColor;
	public Gradient resultGradient;

	[Space]
	public SpriteRenderer faderSprite;
	public SpriteRenderer spiralBackSprite;
	public SpriteRenderer spiralBorderSprite;
	public PolygonCollider2D spiralCollider;

	int pathIndex = 0;
	bool started = false;
	bool holding = false;
	bool finished = false;
	bool active = false;
	Color currentEndColor;
	Tweener endColorTween;

	public static ChemDrawingLine Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		// Vector3[] pathArray = new Vector3[pathParent.childCount];
		// for (int j = 0; j < pathParent.childCount; j++) 
		// {
		// 	pathArray[j] = pathParent.GetChild(j).position;
		// }
		// lineRenderer.positionCount = pathArray.Length;
		// lineRenderer.SetPositions(pathArray);

		for (int i = 0; i < pathParent.childCount; i++)
		{
			// Vector3 vectorToTarget = pathParent.GetChild(i).position - pathParent.GetChild(i + 1).position;
 			// float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
 			// Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
			if(i < pathParent.childCount - 1)
			{
				pathParent.GetChild(i).rotation = Quaternion.Euler(0f, 0f, Utility.angleBetween(pathParent.GetChild(i + 1).position, pathParent.GetChild(i).position) - 90);
			}
			else
			{
				pathParent.GetChild(i).rotation = Quaternion.Euler(0f, 0f, Utility.angleBetween(pathParent.GetChild(i - 1).position, pathParent.GetChild(i).position) - 90);
			}
 			
			GameObject rightPart = new GameObject("rightPart");
			rightPart.transform.SetParent(pathParent.GetChild(i));
			rightPart.transform.localEulerAngles = Vector3.zero;
			rightPart.transform.localPosition = new Vector3(0.2f, 0f, 0f);

			GameObject leftPart = new GameObject("leftPart");
			leftPart.transform.SetParent(pathParent.GetChild(i));
			leftPart.transform.localEulerAngles = Vector3.zero;
			leftPart.transform.localPosition = new Vector3(-0.2f, 0f, 0f);
		}

		UpdateLineRendererColors();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(holding && !finished)
		{
			// float fullDist = Vector3.Distance(pathParent.GetChild(pathIndex).position, pathParent.GetChild(pathIndex + 1).position);
			// float mouseDist = Vector3.Distance(Utility.mouseToWorldPosition(), pathParent.GetChild(pathIndex + 1).position);
			// float ratio = 1f - mouseDist / fullDist;
			// ratio = Mathf.Clamp(ratio, 0f, 1f);
			float ratio = GetRatioOfMouse(pathParent.GetChild(pathIndex + 1).position);
			float ratioRight = GetRatioOfMouse(pathParent.GetChild(pathIndex + 1).GetChild(0).position);
			float ratioLeft = GetRatioOfMouse(pathParent.GetChild(pathIndex + 1).GetChild(1).position);
			if(ratio > 0.5f || ratioLeft > 0.5f || ratioRight > 0.5f)
			{
				lineRenderer.positionCount = pathIndex + 2;
				lineRenderer.SetPosition(pathIndex, pathParent.GetChild(pathIndex).position);
				UpdateLineRendererEndColor();
				pathIndex++;
				ratio = 0f;
				if(pathIndex + 1 == pathParent.childCount)
				{
					lineRenderer.SetPosition(pathIndex, pathParent.GetChild(pathIndex).position);
					finished = true;
					OnPointerUp();
					StartCoroutine(FinishAnimTask());
					return;
				}
			}
			float deltaX = pathParent.GetChild(pathIndex + 1).position.x - pathParent.GetChild(pathIndex).position.x;
			float deltaY = pathParent.GetChild(pathIndex + 1).position.y - pathParent.GetChild(pathIndex).position.y;
			lineRenderer.SetPosition(pathIndex, pathParent.GetChild(pathIndex).position + new Vector3(ratio * deltaX, ratio * deltaY));
			if(!Utility.getColliderAt(Utility.mouseToWorldPosition(), "spiralCollider"))
			{
				OnPointerUp();
			}
		}
	}

	public void OnPointerDown()
	{
		if(holding || finished || !active)
		{
			return;
		}
		holding = true;
		if(lineRenderer.positionCount == 0)
		{
			lineRenderer.positionCount = 2;
			lineRenderer.SetPosition(0, pathParent.GetChild(0).position);
			pathIndex = 1;
		}
	}

	public void OnPointerUp()
	{
		if(!holding)
		{
			return;
		}
		holding = false;
	}

	public void ChangeStartEndColors(Color start, Color end)
	{
		startColor = start;
		endColor = end;
		mixColor = ColorManager.Instance.MixColorsReal(start, end);
		UpdateLineRendererColors();
	}

	float GetRatioOfMouse(Vector3 nextPoint)
	{
		Vector3 prevPoint = pathParent.GetChild(pathIndex).position;
		float fullDist = Vector3.Distance(pathParent.GetChild(pathIndex).position, nextPoint);
		float mouseDist = Vector3.Distance(Utility.mouseToWorldPosition(), nextPoint);
		float ratio = 1f - mouseDist / fullDist;
		ratio = Mathf.Clamp(ratio, 0f, 1f);
		return ratio;
	}

	void UpdateLineRendererColors()
	{
		lineRenderer.startColor = startColor;
		lineRenderer.endColor = startColor;
		resultGradient = new Gradient();

        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        GradientColorKey[] colorKey = new GradientColorKey[2];
        colorKey[0].color = startColor;
        colorKey[0].time = 0.0f;
        colorKey[1].color = endColor;
        colorKey[1].time = 1.0f;

        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        resultGradient.SetKeys(colorKey, alphaKey);

		currentEndColor = startColor;
	}

	void UpdateLineRendererEndColor()
	{
		float ratio = (float)(pathIndex + 2) / pathParent.childCount;
		ratio = Mathf.Clamp(ratio, 0f, 1f);
		if(endColorTween != null)
		{
			endColorTween.Kill();
		}
		endColorTween = DOTween.To(()=> currentEndColor, x=> currentEndColor = x, resultGradient.Evaluate(ratio), 0.2f).OnUpdate(() => 
		{
			lineRenderer.endColor = currentEndColor;
		});
	}

	public void StartToDraw()
	{
		if(started)
		{
			return;
		}
		started = true;
		StartCoroutine(StartAnimTask());
	}

	IEnumerator StartAnimTask()
	{
		spiralCollider.enabled = true;
		UpdateLineRendererColors();
		faderSprite.DOFade(0.38f, 1f);
		spiralBorderSprite.DOFade(1f, 1f);
		spiralBackSprite.DOFade(1f, 1f);
		yield return new WaitForSeconds(1f);
		active = true;
	}

	IEnumerator FinishAnimTask()
	{
		Color startColorTemp = lineRenderer.startColor;
		Color endColorTemp = lineRenderer.endColor;
		if(ColorManager.Instance.MixRealColorsForWonderwood(startColor, endColor) != WonderwoodColor.None)
		{

            DOTween.To(() => startColorTemp, x => startColorTemp = x, mixColor, 1).OnUpdate(() =>
              {
                  lineRenderer.startColor = startColorTemp;
              });

            yield return DOTween.To(() => endColorTemp, x => endColorTemp = x, mixColor, 1).OnUpdate(() =>
              {
                  lineRenderer.endColor = endColorTemp;
              }).WaitForCompletion();
		}
		
		yield return new WaitForSeconds(0.25f);
		faderSprite.transform.parent.DOScale(1.2f, 1f);
		faderSprite.DOFade(0f, 1f);
		Utility.changeAlphaSprite(spiralBackSprite, 0f);
		spiralBackSprite.enabled = false;
		spiralBorderSprite.DOFade(0f, 1f);

		startColorTemp = lineRenderer.startColor;
		DOTween.To(()=> startColorTemp, x=> startColorTemp = x, new Color(startColorTemp.r,startColorTemp.g,startColorTemp.r,0f), 1).OnUpdate(() => 
		{
			lineRenderer.startColor = startColorTemp;
		});

		endColorTemp = lineRenderer.endColor;
		DOTween.To(()=> endColorTemp, x=> endColorTemp = x, new Color(endColorTemp.r,endColorTemp.g,endColorTemp.r,0f), 1).OnUpdate(() => 
		{
			lineRenderer.endColor = endColorTemp;
		});
		yield return new WaitForSeconds(1f);
		faderSprite.transform.parent.localScale = Vector3.one;
		spiralBackSprite.enabled = true;
		active = false;
		started = false;
		finished = false;
		holding = false;
		pathIndex = 0;
		lineRenderer.positionCount = 0;
		ChemBigTube.Instance.StartToMix();
		spiralCollider.enabled = false;
	}
}
