﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FiddlestickAnim : MonoBehaviour 
{
	public Vector3 distance;
	public float duration;

	Vector3 firstPos;
	// Use this for initialization
	void Start () 
	{
		firstPos = transform.localPosition;
		StartCoroutine(MoveTask());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	IEnumerator MoveTask()
	{
		while(true)
		{
			yield return transform.DOLocalMove(firstPos + distance, duration).WaitForCompletion();
			yield return transform.DOLocalMove(firstPos, duration).WaitForCompletion();
		}
	}
}
