﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FaderController : MonoBehaviour 
{
    public GameObject faderCanvasGO;
    public Image faderImage;
    public static FaderController Instance;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(faderCanvasGO);
        }
        else
        {
            Destroy(faderCanvasGO);
            Destroy(gameObject);
        }

    }
    // Use this for initialization
    void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public IEnumerator fadeScreen(float time)
    {
        faderImage.DOKill();
        faderImage.raycastTarget = true;
        yield return faderImage.DOFade(1f, time).WaitForCompletion();
    }

    public IEnumerator unfadeScreen(float time)
    {
        faderImage.DOKill();
        yield return faderImage.DOFade(0f, time).WaitForCompletion();
        faderImage.raycastTarget = false;
    }
}
