﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExtractionMachine : MonoBehaviour 
{
	public BigTube[] bigTubes;
	public SmallTube[] smallTubes;
	public RectTransform flowersListRect;
	public SpinObject bigValve;

	bool listHidden = false;
	bool alreadyLoading;

	public static ExtractionMachine Instance;
	private void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		if (MuteManager.Instance)
        {
            MuteManager.Instance.checkForSoundMute();
        }
        //if(A2SPadaManager.Instance)
        //{
        //    A2SPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        StartCoroutine(extractionMachineStartTask());
	}

	IEnumerator extractionMachineStartTask()
    {
        //yield return new WaitForSeconds(0.5f);
        //if (FirebaseManager.Instance)
        //{
        //    FirebaseManager.Instance.changeScreen("A2S_PlayScreen", "A2S_PlayScreen");
        //}
		if (FirebaseManager.Instance)
        {
           FirebaseManager.Instance.changeScreen("ART1C_ExtractionMachine", "ART1C_ExtractionMachine");
        }
        //if (A1FPadaManager.Instance)
        //{
        //    A1FPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //yield return FirebaseManager.Instance.fetchRemoteConfigTask();
        yield return FaderController.Instance.unfadeScreen(0.3f);
    }
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void CheckForAllSmallTubesArrived()
	{
		for (int i = 0; i < smallTubes.Length; i++)
		{
			if(smallTubes[i].IsFilling() || bigTubes[i].IsFilled())
			{
				return;
			}
		}
		bigValve.ActivateAnim();
		ShowFlowerList();
	}

	public void FillAllTubes()
	{
		
		for (int i = 0; i < bigTubes.Length; i++) 
		{
			if(bigTubes[i].FillByValve())
			{
				if(!IsListHidden())
				{
					HideFlowerList();
					ExecuteMotorAnimation();
				}
			}
		}
	}

	public bool IsListHidden()
	{
		return listHidden;
	}

	public void HideFlowerList()
	{
		if(listHidden)
		{
			return;
		}
		listHidden = true;
		flowersListRect.DOKill();
		flowersListRect.DOAnchorPosY(-56f, 0.5f);
	}

	public void ShowFlowerList()
	{
		if(!listHidden)
		{
			return;
		}
		listHidden = false;
		flowersListRect.DOKill();
		flowersListRect.DOAnchorPosY(56f, 0.5f);
	}

	public void ExecuteMotorAnimation()
	{
		MotorSwitch.Instance.SwitchAnimation();
		ExtractionPipe.Instance.Animation();
		StartCoroutine(ExecuteMotorAnimationTask());
	}

	IEnumerator ExecuteMotorAnimationTask()
	{
		BigSpeedPointer.Instance.IncreaseEffect();
		SpeedOPointer.Instance.IncreaseEffect();
		yield return new WaitForSeconds(2f);
		SpeedOPointer.Instance.DecreaseEffect();
		BigSpeedPointer.Instance.DecreaseEffect();
	}

	public void LoadScene(string sceneName)
	{
		if(alreadyLoading)
		{
			return;
		}
		alreadyLoading = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen(0.3f);
		SceneManager.LoadScene(sceneName);
	}
}
