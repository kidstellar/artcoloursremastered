﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColorsMapManager : MonoBehaviour 
{
	bool alreadyLoading = false;

	public static ColorsMapManager Instance;
	void Awake()
	{
		Instance = this;
		if(PlayerPrefs.GetInt("artcolors_level_dyemaker_unlocked") == 0)
		{
			PlayerPrefs.SetInt("artcolors_level_dyemaker_unlocked", 1);
		}
	}
	// Use this for initialization
	void Start () 
	{
		if (MuteManager.Instance)
        {
            MuteManager.Instance.checkForSoundMute();
        }
        //if(A2SPadaManager.Instance)
        //{
        //    A2SPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        StartCoroutine(mapStartTask());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void OnBackButtonClick()
	{
		LoadScene("PlayScreen");
	}

	public void LoadScene(string sceneName)
	{
		if(alreadyLoading)
		{
			return;
		}
		alreadyLoading = true;
		StartCoroutine(LoadSceneTask(sceneName));
	}

	IEnumerator LoadSceneTask(string sceneName)
	{
		yield return FaderController.Instance.fadeScreen(0.3f);
		SceneManager.LoadScene(sceneName);
	}

	IEnumerator mapStartTask()
    {
        //yield return new WaitForSeconds(0.5f);
        //if (FirebaseManager.Instance)
        //{
        //    FirebaseManager.Instance.changeScreen("A2S_PlayScreen", "A2S_PlayScreen");
        //}
		if (FirebaseManager.Instance)
        {
           FirebaseManager.Instance.changeScreen("ART1C_ColorsMap", "ART1C_ColorsMap");
        }
        //if (A1FPadaManager.Instance)
        //{
        //    A1FPadaManager.Instance.setCurrentScene("PlayScreen");
        //}
        //yield return FirebaseManager.Instance.fetchRemoteConfigTask();
        yield return FaderController.Instance.unfadeScreen(0.3f);
    }
}
