﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ColorsLevel : MonoBehaviour 
{
	public string lockPostFix;
	public string levelName;
	bool unlocked = false;
	// Use this for initialization
	void Start () 
	{
		unlocked = PlayerPrefs.GetInt("artcolors_level_" + lockPostFix + "_unlocked") == 1;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void OnMouseUpAsButton()
	{
		if(!unlocked)
		{
			return;
		}
		transform.DOPunchScale(Vector3.one * 0.1f, 0.2f, 6);
		ColorsMapManager.Instance.LoadScene(levelName);
	}
}
