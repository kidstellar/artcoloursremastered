﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MotorSwitch : MonoBehaviour 
{
	bool switching = false;

	public static MotorSwitch Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void OnMouseUpAsButton()
	{
		SwitchAnimation();
	}

	public void SwitchAnimation()
	{
		if(switching)
		{
			return;
		}
		StartCoroutine(SwitchAnimTask());
	}

	IEnumerator SwitchAnimTask()
	{
		switching = true;
		yield return transform.DORotate(Vector3.forward * 48f, 0.4f).WaitForCompletion();
		yield return transform.DORotate(Vector3.forward * 0f, 0.4f).WaitForCompletion();
		switching = false;
	}
}
