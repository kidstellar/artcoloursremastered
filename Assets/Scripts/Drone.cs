﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Drone : MonoBehaviour 
{
	public SpriteRenderer droneSprite;

	bool travelling = false;

	public static Drone Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public bool IsTravelling()
	{
		return travelling;
	}

	public void GoToCollect()
	{
		StartCoroutine(GoToCollectTask());
	}

	IEnumerator GoToCollectTask()
	{
		travelling = true;
		transform.DOMoveX(-0.15f, 5f).SetEase(Ease.OutElastic, 0.01f);
		droneSprite.transform.localRotation = Quaternion.Euler(0f, 0f, 30f);
		droneSprite.transform.DOLocalRotate(Vector3.forward * 0f, 5f).SetEase(Ease.OutElastic, 2.5f);
		yield return new WaitForSeconds(2f);
		GardenTopCover.Instance.OpenCover();
		yield return new WaitForSeconds(0.3f);
		float duration = FlowerPot.Instance.AbsorbAllFlowersToDrone();
		yield return new WaitForSeconds(duration + 0.3f);
		GardenTopCover.Instance.CloseCover();
		droneSprite.transform.DOLocalRotate(Vector3.forward * -30f, 1f).SetEase(Ease.InSine, 0.1f);
		transform.DOMoveX(12.32f, 2f).SetEase(Ease.InSine);
		yield return new WaitForSeconds(2f);
		travelling = false;
	}
}
