﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DyeField : MonoBehaviour 
{
	public SpriteRenderer normalSprite;
	public SpriteRenderer deadSprite;
	public Transform plantPlace;
	public int startSorting;

	FlowerSeed currentFlowerSeed;
	BoxCollider2D boxCollider2d;
	
	bool dried = false;
	float saturation = 1f;
	// Use this for initialization
	void Start () 
	{
		boxCollider2d = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public bool IsDried()
	{
		return dried;
	}

	public bool HasAlreadyPlanted()
	{
		return currentFlowerSeed != null;
	}

	public bool IsAvailableForWatering()
	{
		return HasAlreadyPlanted() && !currentFlowerSeed.HasGrown() && !currentFlowerSeed.HasCuttedOff() && !currentFlowerSeed.HasPlaced();
	}

	public bool IsAvailableForCutting()
	{
		return HasAlreadyPlanted() && currentFlowerSeed.HasGrown() && !currentFlowerSeed.HasCuttedOff() && !currentFlowerSeed.HasPlaced();
	}

	public void IncreaseSaturationOfFlower()
	{
		if(currentFlowerSeed)
		{
			currentFlowerSeed.IncreaseSaturation();
		}
	}

	public void PlantFlower(FlowerSeed flowerSeed)
	{
		currentFlowerSeed = flowerSeed;
		flowerSeed.UpdateDyeField(this);
		currentFlowerSeed.transform.position = plantPlace.position;
		currentFlowerSeed.transform.DOPunchScale(Vector3.one * 0.1f, 0.8f, 6).SetEase(Ease.OutElastic);
		currentFlowerSeed.UpdateOrder(startSorting);
		DyeMakerGardenManager.Instance.GeneratePlantParticle(transform.position, 1f);
	}

	public void CutOffFlower()
	{
		if(currentFlowerSeed)
		{
			currentFlowerSeed.CutOff();
			boxCollider2d.enabled = false;
		}
	}

	public void IncreaseSaturationOfSoil()
	{
		if(!dried)
		{
			return;
		}
		saturation += Time.deltaTime;
		saturation = Mathf.Clamp(saturation, 0f, 1f);
		if(Mathf.Approximately(saturation, 1f))
		{
			Fertilize();
		}
	}

	public void Dry()
	{
		normalSprite.DOFade(0f, 0.3f);
		deadSprite.DOFade(1f, 0.3f);
		boxCollider2d.enabled = true;
		saturation = 0f;
		dried = true;
	}

	public void Fertilize()
	{
		normalSprite.DOFade(1f, 0.3f);
		deadSprite.DOFade(0f, 0.3f);
		saturation = 1f;
		dried = false;
		currentFlowerSeed = null;
	}
}
