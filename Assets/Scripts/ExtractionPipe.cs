﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ExtractionPipe : MonoBehaviour 
{
	public ParticleSystem smokeParticle;
	public Transform pipeTransform;
	bool playing = false;

	public static ExtractionPipe Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void Animation()
	{
		if(playing)
		{
			return;
		}
		StartCoroutine(PipeAnimTask());
	}

	void SetParticleEmission(float value)
	{
		var emission = smokeParticle.emission;
		emission.rateOverTime = value;
	}

	IEnumerator PipeAnimTask()
	{
		playing = true;
		pipeTransform.DOPunchPosition(Vector2.one * 0.1f, 2f, 11);
		SetParticleEmission(40);
		yield return new WaitForSeconds(2f);
		SetParticleEmission(0);
		playing = false;
	}
}
