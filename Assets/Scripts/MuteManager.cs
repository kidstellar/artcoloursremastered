﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteManager : MonoBehaviour {

    public bool isSFXMuted = false;
    public bool isMusicMuted = false;

    public static MuteManager Instance;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void checkForSoundMute()
    {
        if (PlayerPrefs.GetInt("artcolors1_sound_on") == 0)
        {
            isSFXMuted = true;
            AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
            for (int index = 0; index < sources.Length; ++index)
            {
                if (sources[index].gameObject.tag != "music")
                {
                    sources[index].mute = true;
                }
            }
        }
        else
        {
            isSFXMuted = false;
            AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
            for (int index = 0; index < sources.Length; ++index)
            {
                if (sources[index].gameObject.tag != "music")
                {
                    sources[index].mute = false;
                }
            }
        }
        checkForMusicMute();
    }

    public void checkForMusicMute()
    {
        if (PlayerPrefs.GetInt("artcolors1_music_on") == 0)
        {
            isMusicMuted = true;
            AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
            for (int index = 0; index < sources.Length; ++index)
            {
                if (sources[index].gameObject.tag == "music")
                {
                    sources[index].mute = true;
                }
            }
        }
        else
        {
            isMusicMuted = false;
            AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
            for (int index = 0; index < sources.Length; ++index)
            {
                if (sources[index].gameObject.tag == "music")
                {
                    sources[index].mute = false;
                }
            }
        }
    }
}
