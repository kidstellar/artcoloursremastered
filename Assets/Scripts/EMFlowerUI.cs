﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class EMFlowerUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

	public GameObject flowerSeedPrefab;
	public Color color;

	Transform currentFlowerSeed;
	bool holding = false;
	// Use this for initialization
	void Start()
	{
			
	}

	// Update is called once per frame
	void Update()
	{
		if (holding)
		{
			currentFlowerSeed.position = Utility.mouseToWorldPosition();
		}
	}


	public void OnPointerDown(PointerEventData eventData)
	{
		if (holding)
		{
			return;
		}
		transform.DOKill(true);
		transform.DOPunchScale(Vector3.one * 0.2f, 1f, 6).SetEase(Ease.OutElastic);
		currentFlowerSeed = Instantiate(flowerSeedPrefab).transform;
		holding = true;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (!holding)
		{
			return;
		}
		TubeHead tubeHead = Utility.getColliderAt<TubeHead>(Utility.mouseToWorldPosition());
		if (tubeHead)
		{
			tubeHead.SetBigTubeColor(color);
			StartCoroutine(PutFlowerIntoTubeHead(tubeHead));
		}
		else
	   	{
			Destroy(currentFlowerSeed.gameObject);
		}

		holding = false;
	}

	IEnumerator PutFlowerIntoTubeHead(TubeHead tubeHead)
	{
		Transform flower = currentFlowerSeed;
		yield return flower.DOMove(tubeHead.transform.position + new Vector3(0f, 0.5f), 3f).SetSpeedBased().SetEase(Ease.OutSine).WaitForCompletion();
		flower.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 153;
		flower.DOScale(flower.localScale / 2f, 3f).SetSpeedBased().SetEase(Ease.InSine);
		yield return flower.DOMove(tubeHead.transform.position - new Vector3(0f, 0.5f), 3f).SetSpeedBased().SetEase(Ease.InSine).WaitForCompletion();
		Destroy(flower.gameObject);
	}
}
