﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenTopCover : MonoBehaviour 
{
	public Sprite[] coverSprites;
	public SpriteRenderer coverSpriteRenderer;

	int framdeIndex = 0;

	Coroutine coverCo;

	public static GardenTopCover Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void OpenCover()
	{
		if(coverCo != null)
		{
			StopCoroutine(coverCo);
		}
		coverCo = StartCoroutine(OpenCoverTask());
	}

	public void CloseCover()
	{
		if(coverCo != null)
		{
			StopCoroutine(coverCo);
		}
		coverCo = StartCoroutine(CloseCoverTask());
	}

	IEnumerator OpenCoverTask()
	{
		coverSpriteRenderer.sprite = coverSprites[0];
		for (int i = 1; i < coverSprites.Length; i++)
		{
			yield return new WaitForSeconds(0.1f);
			coverSpriteRenderer.sprite = coverSprites[i];
		}
	}

	IEnumerator CloseCoverTask()
	{
		coverSpriteRenderer.sprite = coverSprites[coverSprites.Length - 1];
		for (int i = coverSprites.Length - 2; i >= 0; i--)
		{
			yield return new WaitForSeconds(0.1f);
			coverSpriteRenderer.sprite = coverSprites[i];
		}
	}

	
}
