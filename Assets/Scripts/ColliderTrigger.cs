﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderTrigger : MonoBehaviour 
{
	public UnityEvent onPointerDown;
	public UnityEvent onPointerUp;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void OnMouseDown()
	{
		onPointerDown.Invoke();
	}

	private void OnMouseUp()
	{
		onPointerUp.Invoke();
	}

}