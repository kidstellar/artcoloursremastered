﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FertilizerBag : MonoBehaviour 
{
	public SpriteRenderer potSprite;
	public ParticleSystem waterParticle;

	bool holding = false;
	bool pouring = false;
	float followSpeed = 10f;
	DyeField currentDyeField;
	Vector3 deltaPos;
	Vector3 firstPos;
	int firstSortingOrder;
	// Use this for initialization
	void Start () 
	{
		firstPos = transform.position;
		firstSortingOrder = potSprite.sortingOrder;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(holding)
		{
			if(!pouring)
			{
				transform.position = Vector3.MoveTowards(transform.position, Utility.mouseToWorldPosition() - deltaPos, followSpeed * Time.deltaTime); 
			}
			else
			{
				transform.position = Vector3.MoveTowards(transform.position, currentDyeField.transform.position + new Vector3(0.8f, 1.1f), followSpeed * Time.deltaTime);
				currentDyeField.IncreaseSaturationOfSoil();
			}
			CheckForDyeField();
		}
	}

	void CheckForDyeField()
	{
		DyeField dyeField = Utility.getColliderAt<DyeField>(Utility.mouseToWorldPosition());
		if(!pouring && dyeField && dyeField.IsDried() || (pouring && dyeField && dyeField.IsDried()))
		{
			pouring = true;
			potSprite.transform.DOKill();
			potSprite.transform.DOLocalRotate(Vector3.forward * 90f, 300f).SetSpeedBased();
			currentDyeField = dyeField;
			SetWaterEmission(100);
		}
		else if(pouring && (!dyeField || !dyeField.IsDried()))
		{
			potSprite.transform.DOKill();
			potSprite.transform.DOLocalRotate(Vector3.forward * 0f, 300f).SetSpeedBased().OnComplete(() => 
			{
				pouring = false;
			});
			
			SetWaterEmission(0);
		}
	}

	void SetWaterEmission(float emissionRate)
	{
		var emission = waterParticle.emission;
		emission.rateOverTime = emissionRate;
	}

	private void OnMouseDown()
	{
		if(holding)
		{
			return;
		}
		deltaPos = Utility.mouseToWorldPosition() - transform.position;
		holding = true;
		potSprite.sortingOrder = 600;
	}

	private void OnMouseUp()
	{
		if(!holding)
		{
			return;
		}
		
		holding = false;
		SetWaterEmission(0);
		transform.position = firstPos;
		transform.DOKill(true);
		transform.DOPunchScale(Vector3.one * 0.2f, 1f, 6).SetEase(Ease.OutElastic);
		pouring = false;
		potSprite.transform.DOKill();
		potSprite.transform.DOLocalRotate(Vector3.forward * 0f, 300f).SetSpeedBased();
		potSprite.sortingOrder = firstSortingOrder;
	}
}
