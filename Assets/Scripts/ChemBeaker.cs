﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemBeaker : MonoBehaviour 
{
	public SpriteRenderer waterSprite;
	public float hideY;
	public float showY;
	public ChemLiquid liquid;
	public Color color;

	bool filled = false;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public IEnumerator TravelLiquidTask()
	{
		liquid.SetColor(color);
		yield return liquid.TravelTask();
	}

	public void FillLiquid()
	{
		if(filled)
		{
			return;
		}
		filled = true;
		waterSprite.color = color;
		waterSprite.transform.DOKill();
		waterSprite.transform.DOLocalMoveY(showY, 2f);
	}

	public void Unfill()
	{
		if(!filled)
		{
			return;
		}
		filled = false;
		waterSprite.transform.DOKill();
		waterSprite.transform.DOLocalMoveY(hideY, 5f);
	}

	public bool IsFilling()
	{
		return filled;
	}

	public bool HasFilled()
	{
		// return filled && Mathf.Approximately(waterSprite.transform.localPosition.y, showY);
		return filled && Mathf.Approximately(waterSprite.transform.localPosition.y, showY);
	}
}
