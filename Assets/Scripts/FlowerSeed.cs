﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FlowerSeed : MonoBehaviour 
{
	public SpriteRenderer seedSprite;
	public SpriteRenderer stemSprite;
	public SpriteRenderer headSprite;
	public Transform mainFlowerTransform;
	public float finalScale = 0.55f;

	public BoxCollider2D oldCollider;
	public BoxCollider2D newCollider;

	bool grown = false;
	bool cutted = false;
	bool holding = false;
	bool placed = false;
	float saturation = 0f;
	DyeField currentDyeField;

	Vector3 firstPos;
	Vector3 firstScale;
	int firstSortingOrder;
	// Use this for initialization
	void Start () 
	{
		seedSprite.sortingOrder = 600;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(holding)
		{
			transform.position = Utility.mouseToWorldPosition();
		}
	}

	public void UpdateDyeField(DyeField dyeField)
	{
		currentDyeField = dyeField;
	}

	public void UpdateOrder(int startOrder)
	{
		seedSprite.sortingOrder = startOrder;
		stemSprite.sortingOrder = startOrder;
		headSprite.sortingOrder = startOrder + 1;
		firstSortingOrder = headSprite.sortingOrder;
	}

	public void IncreaseSaturation()
	{
		if(grown || placed)
		{
			return;
		}
		saturation += Time.deltaTime;
		saturation = Mathf.Clamp(saturation, 0f, 1f);
		if(Mathf.Approximately(saturation, 1f))
		{
			GrowFlower();
		}
	}

	void GrowFlower()
	{
		grown = true;
		seedSprite.DOFade(0f, 0.3f);
		mainFlowerTransform.localScale = Vector3.one * finalScale / 2f;
		mainFlowerTransform.DOScale(finalScale, 1f).SetEase(Ease.OutElastic);
	}

	public IEnumerator AbsorbByDroneTask()
	{
		headSprite.transform.DOShakePosition(0.5f, Vector3.one * 0.05f, 15, 5, false, false);
		yield return new WaitForSeconds(0.5f);
		yield return transform.DOMove(Drone.Instance.transform.position, 0.7f).SetEase(Ease.InQuad).WaitForCompletion();
		Destroy(gameObject);
	}

	public void CutOff()
	{
		stemSprite.transform.DOLocalMove(new Vector3(2f, 2f), 0.3f).SetRelative();
		stemSprite.transform.DOLocalRotate(Vector3.forward * -720f, 1f, RotateMode.FastBeyond360);
		stemSprite.DOFade(0f, 0.3f);
		headSprite.transform.DOLocalMoveY(0f, 0.5f);
		headSprite.transform.DOLocalMoveX(0f, 0.5f);
		oldCollider.enabled = false;
		newCollider.enabled = true;
		cutted = true;
	}

	public bool HasGrown()
	{
		return grown;
	}

	public bool HasPlaced()
	{
		return placed;
	}

	public bool HasCuttedOff()
	{
		return cutted;
	}

	public void Bounce(Ease ease, float duration)
	{
		transform.localScale = firstScale;
		transform.DOPunchScale(Vector3.one * 0.1f, duration, 6).SetEase(ease);
	}

	private void OnMouseDown()
	{
		if(holding || !cutted || !grown || placed)
		{
			return;
		}
		holding = true;
		firstPos = transform.position;
		firstScale = transform.localScale;
		transform.DOKill();
		transform.DOScale(firstScale * 2f, 0.2f);
		headSprite.sortingOrder = 600;
	}

	private void OnMouseUp()
	{
		if(!holding)
		{
			return;
		}
		holding = false;
		transform.DOKill(true);
		headSprite.sortingOrder = firstSortingOrder;
		if(Utility.getColliderAt<FlowerPot>(Utility.mouseToWorldPosition()) && !Drone.Instance.IsTravelling())
		{
			FlowerPot.Instance.PutFlower(this);
			currentDyeField.Dry();
			currentDyeField = null;
			placed = true;
		}
		else
		{
			transform.position = firstPos;
			transform.localScale = firstScale;
			transform.DOPunchScale(Vector3.one * 0.1f, 1f, 6).SetEase(Ease.OutElastic);
		}
		
	}
}
