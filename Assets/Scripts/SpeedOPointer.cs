﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SpeedOPointer : MonoBehaviour 
{
	bool increasing = false;
	Coroutine speedCo;

	public static SpeedOPointer Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.O))
		{
			IncreaseEffect();
		}
		else if(Input.GetKeyDown(KeyCode.L))
		{
			DecreaseEffect();
		}
	}

	public void IncreaseEffect()
	{
		if(increasing)
		{
			return;
		}
		increasing = true;
		if(speedCo != null)
		{
			StopCoroutine(speedCo);
		}
		transform.DOKill();
		speedCo = StartCoroutine(IncreaseEffectTask());
	}

	public void DecreaseEffect()
	{
		if(!increasing)
		{
			return;
		}
		increasing = false;
		if(speedCo != null)
		{
			StopCoroutine(speedCo);
		}
		transform.DOKill();
		speedCo = StartCoroutine(DecreaseEffectTask());
	}

	IEnumerator IncreaseEffectTask()
	{
		yield return transform.DORotate(Vector3.forward * 37f, 0.1f).WaitForCompletion();
		transform.DOPunchRotation(Vector3.forward * 5f, 0.3f, 10).SetLoops(-1, LoopType.Yoyo);
	}

	IEnumerator DecreaseEffectTask()
	{
		yield return transform.DORotate(Vector3.forward * 100f, 1f).WaitForCompletion();
		//transform.DOPunchRotation(Vector3.forward * 5f, 0.5f, 10).SetLoops(-1, LoopType.Yoyo);
	}
}
