﻿using System;
using System.Collections;
using System.Collections.Generic;
using PADA;
using UnityEngine;
using UnityEngine.Networking;

public class A1CWebRequester : MonoBehaviour
{
    public string accessToken = "";

    public static A1CWebRequester Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {
        StartCoroutine(PostRequestForLogin());
    }

    // Update is called once per frame
    void Update()
    {

    }

    // public void sendLoginRequest(string url, string json)
    // {
    //  StartCoroutine(PostRequestForLogin(url, json));
    // }

    public void sendRequest(string url, string json, int requestId)
    {
        StartCoroutine(PostRequest(url, json, requestId));
    }



    IEnumerator PostRequest(string url, string json, int requestId)
    {
    startRequest:
        var uwr = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Authorization", "Bearer " + accessToken);
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns

        UnityWebRequestAsyncOperation asyncOp = uwr.SendWebRequest();
        float sayac = 0f;
        bool interrupt = false;
        while (!asyncOp.isDone)
        {
            yield return new WaitForFixedUpdate();
            sayac += Time.deltaTime;
            if (sayac >= 15f)
            {
                interrupt = true;
                break;
            }
        }
        if (interrupt)
        {
            Debug.Log("Timeout");
            yield break;
        }

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {

            int responseCode = -999;
            if (requestId == 0)
            {
                responseCode = handleSessionResponse(JsonUtility.FromJson<SessionResponse>(uwr.downloadHandler.text));
            }
            else if (requestId == 1)
            {
                responseCode = handleFavResponse(JsonUtility.FromJson<FavResponse>(uwr.downloadHandler.text));
            }
            else if (requestId == 2)
            {
                responseCode = handleKnowledgeResponse(JsonUtility.FromJson<KnowResponse>(uwr.downloadHandler.text));
            }
            else if (requestId == 3)
            {
                responseCode = handleAbilityResponse(JsonUtility.FromJson<AbilityResponse>(uwr.downloadHandler.text));
            }


            if (responseCode == 401)
            {
                uwr.Dispose();
                yield return PostRequestForLogin();
                yield return new WaitForSeconds(1f);
                goto startRequest;
            }

        }
    }

    int handleSessionResponse(SessionResponse response)
    {
        if (response.success)
        {
            Debug.Log("session success");
            return -999;
        }
        else if (response.error != null)
        {
            Debug.Log("Session error : " + response.error.code);
            return response.error.code;
        }
        return 0;
    }

    int handleFavResponse(FavResponse response)
    {
        if (response.success)
        {
            return -999;
        }
        else if (response.error != null)
        {
            return response.error.code;
        }
        return 0;
    }

    int handleKnowledgeResponse(KnowResponse response)
    {
        if (response.success)
        {
            return -999;
        }
        else if (response.error != null)
        {
            return response.error.code;
        }
        return 0;
    }

    int handleAbilityResponse(AbilityResponse response)
    {
        if (response.success)
        {
            return -999;
        }
        else if (response.error != null)
        {
            return response.error.code;
        }
        return 0;
    }

    int handleLoginResponse(LoginResponse response)
    {
        if (response.success)
        {
            Debug.Log("Success login");
            accessToken = response.result.accessToken;
            A1CPadaManager.Instance.isEnabled = true;
            return -999;
        }
        else if (response.error != null)
        {
            A1CPadaManager.Instance.isEnabled = false;
            return response.error.code;
        }
        return 0;

    }

    IEnumerator PostRequestForLogin()
    {
        int tries = 0;
    startLogin:
        Debug.Log("Logging in");
        A1CPadaManager.Instance.isEnabled = false;
        string url = A1CPadaManager.Instance.hostName + A1CPadaManager.Instance.loginLocation;
        LoginRequest lr = new LoginRequest();
        lr.userNameOrEmailAddress = "7115ac67-4701-40ea-aa82-b1d58ac2";
        lr.password = "d49ba6d9-5198-4882-947b-9911bbd4";
        var uwr = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(JsonUtility.ToJson(lr));
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns
        UnityWebRequestAsyncOperation asyncOp = uwr.SendWebRequest();
        float sayac = 0f;
        bool interrupt = false;
        while (!asyncOp.isDone)
        {
            yield return new WaitForFixedUpdate();
            sayac += Time.deltaTime;
            if (sayac >= 15f)
            {
                interrupt = true;
                break;
            }
        }
        if (interrupt)
        {
            Debug.Log("Timeout");
            yield break;
        }

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {
            int resultCode = handleLoginResponse(JsonUtility.FromJson<LoginResponse>(uwr.downloadHandler.text));
            if (resultCode == 401)
            {
                uwr.Dispose();
                tries++;
                if (tries > 5)
                {
                    yield break;
                }
                yield return new WaitForSeconds(1f);
                goto startLogin;
            }
            else if (resultCode == -999)
            {
                yield return PadaOfflineManager.Instance.sendOfflineDatasToServerTask();
            }
        }
    }

    public IEnumerator PostRequestForOfflineData(string url, string json, int requestId, Action<bool> completedFunc)
    {
    startRequest:
        var uwr = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Authorization", "Bearer " + accessToken);
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns

        UnityWebRequestAsyncOperation asyncOp = uwr.SendWebRequest();
        float sayac = 0f;
        bool interrupt = false;
        while (!asyncOp.isDone)
        {
            yield return new WaitForFixedUpdate();
            sayac += Time.deltaTime;
            if (sayac >= 15f)
            {
                interrupt = true;
                break;
            }
        }
        if (interrupt)
        {
            Debug.Log("Timeout");
            completedFunc(false);
            yield break;
        }

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
            completedFunc(false);
        }
        else
        {

            int responseCode = -999;
            if (requestId == 0)
            {
                responseCode = handleSessionResponse(JsonUtility.FromJson<SessionResponse>(uwr.downloadHandler.text));
            }
            else if (requestId == 1)
            {
                responseCode = handleFavResponse(JsonUtility.FromJson<FavResponse>(uwr.downloadHandler.text));
            }
            else if (requestId == 2)
            {
                responseCode = handleKnowledgeResponse(JsonUtility.FromJson<KnowResponse>(uwr.downloadHandler.text));
            }
            else if (requestId == 3)
            {
                responseCode = handleAbilityResponse(JsonUtility.FromJson<AbilityResponse>(uwr.downloadHandler.text));
            }
            completedFunc(responseCode == -999);

        }
    }
}
