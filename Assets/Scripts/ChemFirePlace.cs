﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ChemFirePlace : MonoBehaviour 
{
	public SpriteRenderer fireSpriteRenderer;
	public Sprite[] fireSprites;
	public float interval = 0.05f;
	

	int frameIndex = 0;
	float timer = 0f;
	bool playing = false;

	public static ChemFirePlace Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(playing)
		{
			timer += Time.deltaTime;
			if(timer >= interval)
			{
				timer = 0f;
				frameIndex = (frameIndex + 1) % fireSprites.Length;
				fireSpriteRenderer.sprite = fireSprites[frameIndex];
			}
		}
	}

	public bool IsPlaying()
	{
		return playing;
	}

	public void Play()
	{
		if(playing)
		{
			return;
		}
		playing = true;
		frameIndex = 0;
		fireSpriteRenderer.sprite = fireSprites[0];
		fireSpriteRenderer.DOKill();
		fireSpriteRenderer.DOFade(1f, 0.2f);
	}

	public void Stop()
	{
		if(!playing)
		{
			return;
		}
		playing = false;
		fireSpriteRenderer.DOKill();
		fireSpriteRenderer.DOFade(0f, 0.2f);
	}

	private void OnMouseUpAsButton()
	{
		ChemBigTube.Instance.StartHeat();
	}
}
