﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Utility : MonoBehaviour
{

    public static float angleBetween(Vector3 p1, Vector3 p2)
    {
        return Mathf.Atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Mathf.PI;
    }

    public static float angleBetweenForForest(Vector3 p1, Vector3 p2)
    {
        //Debug.Log(p1 + " : " +  p2 + " : " + Mathf.Atan2(p2.z - p1.z, p2.x - p1.x) * 180 / Mathf.PI);

        return Mathf.Atan2(p2.z - p1.z, p2.x - p1.x) * 180 / Mathf.PI;
    }

    public static Vector3 mouseToWorldPosition()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        return mousePos;
    }
    public static void changeAlphaSprite(SpriteRenderer sr, float alpha = 0f)
    {
        Color c = sr.color;
        c.a = alpha;
        sr.color = c;
    }
    public static GameObject getObjectInPos(Vector3 pos, string tag)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero, 1f);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.tag == tag)
            {
                return hits[i].collider.gameObject;
            }
        }
        return null;
    }

    public static GameObject getColliderAt(Vector3 mousePos, string tag)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(mousePos, Vector2.zero, 1f);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.tag == tag)
            {
                return hits[i].collider.gameObject;
            }
        }
        return null;
    }

	public static T getColliderAt<T>(Vector3 mousePos) where T : Component
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(mousePos, Vector2.zero, 1f);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.GetComponent<T>())
            {
                return hits[i].collider.GetComponent<T>();
            }
        }
        return null;
    }

    public static GameObject getUIElementAt(GraphicRaycaster gr, Vector3 pos, string tag)
    {
        PointerEventData m_PointerEventData = new PointerEventData(null);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        gr.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            if (result.gameObject.tag == tag)
            {
                return result.gameObject;
            }
        }
        return null;
    }

    public static T getUIElementAt<T>(GraphicRaycaster gr, Vector3 pos) where T : Component
    {
        PointerEventData m_PointerEventData = new PointerEventData(null);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        gr.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            if (result.gameObject.GetComponent<T>())
            {
                return result.gameObject.GetComponent<T>();
            }
        }
        return null;
    }

    public static bool isThereUIElementAt(GraphicRaycaster gr, Vector3 pos)
    {
        PointerEventData m_PointerEventData = new PointerEventData(null);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        gr.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray

        return results.Count > 0;
    }

    public static Vector3 randomVectorAroundPoint(Transform spawnPoint, Transform owner, float range = 1f)
    {
        Vector2 randomVector = Random.insideUnitCircle * range;
        if(randomVector.x <= 0f)
        {
            randomVector.x = Mathf.Clamp(randomVector.x, -range / 2f, -range);
        }
        else
        {
            randomVector.x = Mathf.Clamp(randomVector.x, range / 2f, range);
        }

        if (randomVector.y <= 0f)
        {
            randomVector.y = Mathf.Clamp(randomVector.y, -range / 2f, -range);
        }
        else
        {
            randomVector.y = Mathf.Clamp(randomVector.y, range / 2f, range);
        }

        return new Vector3(spawnPoint.position.x + randomVector.x, owner.position.y, spawnPoint.position.z + randomVector.y);
    }

    public static void ShuffleList<T>(ref List<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }

    }



    

}
