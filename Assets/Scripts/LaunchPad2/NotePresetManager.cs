﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotePresetManager : MonoBehaviour 
{
	public NotePreset[] notePresets;
	public Text presetText;
	public int currentIndex = 3;

	public static NotePresetManager Instance;
	private void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		currentIndex = 12;
		presetText.text = notePresets[currentIndex].name;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public NotePreset GetNotesOfMusic()
	{
		return notePresets[currentIndex];
	}

	public void SetTextActive(bool value)
	{
		presetText.gameObject.SetActive(value);
	}

	public void GoToNextPreset()
	{
		if(MicrophoneManager.Instance.isMicButtonPressed())
        {
            return;
        }
		currentIndex = (currentIndex + 1) % notePresets.Length;
		presetText.text = notePresets[currentIndex].name;
	}
}

[Serializable]
public class NotePreset
{
	public float delayPerSpace = 0.5f;
	public string name;
	public string notes;
	public float delayMultiplierPiano = 1f;
	public float delayMultiplierViolin = 1f;
	public float delayMultiplierFlute = 1f;

	public float GetDelayForPlayerType(NotePlayerType playerType)
	{
		if(playerType == NotePlayerType.Piano)
		{
			return delayMultiplierPiano;
		}
		else if(playerType == NotePlayerType.Violin)
		{
			return delayMultiplierViolin;
		}
		else if(playerType == NotePlayerType.Flute)
		{
			return delayMultiplierFlute;
		}
		return 1f;
	}
}
