﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ViolinAudioPoolManager : MonoBehaviour 
{
	public AudioSource[] pool;
	public int poolCount = 10;

	public static ViolinAudioPoolManager Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		pool = new AudioSource[poolCount];
		for (int i = 0; i < poolCount; i++)
		{
			pool[i] = gameObject.AddComponent<AudioSource>();
			pool[i].playOnAwake = false;
			pool[i].volume = 0f;
			
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void PlayClip(AudioClip clip)
	{
		AudioSource audioSource = GetAudioSource();
		if(audioSource == null)
		{
			return;
		}
		audioSource.clip = clip;
		audioSource.DOKill();
		audioSource.DOFade(1f, 0.2f);
		audioSource.Play();
	}

	public void StopClip(AudioClip clip)
	{
		for (int i = 0; i < pool.Length; i++)
		{
			if(pool[i].clip == clip)
			{
				pool[i].DOKill();
				pool[i].DOFade(0f, 0.2f);
			}
		}
	}



	AudioSource GetAudioSource()
	{
		for (int i = 0; i < pool.Length; i++)
		{
			if(pool[i].volume < 0.05f)
			{
				return pool[i];
			}
		}
		return null;
	}
}
