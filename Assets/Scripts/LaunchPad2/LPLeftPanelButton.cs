﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LPLeftPanelButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	Button button;
	public Sprite normalSprite;
	public Sprite pressedSprite;
	public Color disabledColor;
	// Use this for initialization
	void Awake()
	{
		button = GetComponent<Button>();
	}

	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SwitchPress()
	{
		button.image.sprite = pressedSprite;
	}

	public void SwitchRelease()
	{
		button.image.sprite = normalSprite;
	}

	public void SetButtonActive(bool value)
	{
		button.interactable = value;
		if(value)
		{
			button.image.color = Color.white;
		}
		else
		{
			button.image.color = disabledColor;
		}
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        SwitchPress();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        SwitchRelease();
    }
}
