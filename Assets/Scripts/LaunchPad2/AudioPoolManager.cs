﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AudioPoolManager : MonoBehaviour 
{
	public LPAudioPlayer[] pool;
	public GameObject poolInstanceGO;
	public int poolCount = 20;

	public static AudioPoolManager Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		pool = new LPAudioPlayer[poolCount];
		for (int i = 0; i < poolCount; i++)
		{
			pool[i] = Instantiate(poolInstanceGO, transform).GetComponent<LPAudioPlayer>();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void PlayClip(AudioClip clip)
	{
		if(clip == null)
		{
			return;
		}
		StartCoroutine(PlayClipTask(clip));
	}

	IEnumerator PlayClipTask(AudioClip clip)
	{
		LPAudioPlayer audioPlayer = GetAudioPlayer();
		if(audioPlayer == null)
		{
			yield break;
		}
		audioPlayer.PlayClip(clip);
	}

	LPAudioPlayer GetAudioPlayer()
	{
		for (int i = 0; i < pool.Length; i++)
		{
			if(pool[i].isReady())
			{
				return pool[i];
			}
		}
		return null;
	}
}
