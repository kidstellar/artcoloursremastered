﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class NotePlayerTypeButton : MonoBehaviour 
{
	public float distanceY = -10f;

	RectTransform rectTransform;
	float firstY;
	// Use this for initialization
	void Start () 
	{
		rectTransform = GetComponent<RectTransform>();
		firstY = rectTransform.anchoredPosition.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SwitchPress()
	{
		rectTransform.DOKill();
		rectTransform.DOAnchorPosY(firstY + distanceY, 0.05f);
	}

	public void SwitchRelease()
	{
		rectTransform.DOKill();
		rectTransform.DOAnchorPosY(firstY, 0.05f);
	}
}
