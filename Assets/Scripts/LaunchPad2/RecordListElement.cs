﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordListElement : MonoBehaviour 
{
	public string filename;
	public Text songNameText;
	public int fileIndex = 0;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SetSongName(char index)
	{
		fileIndex = int.Parse(index.ToString());
		songNameText.text = "My Song " + index;
	}

	public void OnPlayClick()
	{
		IngameRecordManager.Instance.PlayRecord(fileIndex);
	}

	public void OnRemoveClick()
	{
		IngameRecordManager.Instance.RemoveRecord(fileIndex);
	}
}
