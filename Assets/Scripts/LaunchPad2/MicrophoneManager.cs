﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MicrophoneManager : MonoBehaviour
{
	public LPLeftPanelButton micButton;
	public LPLeftPanelButton removeButton;
	public NotePlayer customPlayer;
	public string deviceName = "Built-in Microphone";

    AudioClip myAudioClip;
	int currentIndex = -1;
	bool recording = false;
	bool micPressed = false;
	bool removePressed = false;
	float timer = 0f;


	public static MicrophoneManager Instance;
	void Awake()
	{
		Instance = this;
	}

    void Start()
    {

    }

    void Update()
    {
		if(recording)
		{
			timer += Time.deltaTime;
			if(timer >= 14.9f)
			{
				timer = 0f;
				OnMicButtonClick();
			}
		}
    }

	public void CheckForNotesGray(float duration)
	{
		for (int i = 0; i < LaunchPadManager.Instance.padButtons.Length; i++)
		{
			LaunchPadManager.Instance.padButtons[i].ActivateCustomGray(customPlayer.noteClips[i] == null && LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Custom, duration);
		}
	}

	public void SetCurrentIndex(int index)
	{
		currentIndex = index;
	}

	public void StartToRecordMic()
	{
		if(recording)
		{
			return;
		}
		recording = true;
		myAudioClip = Microphone.Start(Microphone.devices[0], false, 15, 44100);
	}

	public string StopRecordMic(bool completed)
	{
		if(!recording)
		{
			return null;
		}
		recording = false;
		var position = Microphone.GetPosition(Microphone.devices[0]);
		Microphone.End(Microphone.devices[0]);
		if(!completed)
		{
			return "";
		}
        var soundData = new float[myAudioClip.samples * myAudioClip.channels];
        myAudioClip.GetData(soundData, 0);

        //Create shortened array for the data that was used for recording
        var newData = new float[position * myAudioClip.channels];

        //Copy the used samples to a new array
        for (int i = 0; i < newData.Length; i++)
        {
            newData[i] = soundData[i];
        }
        //One does not simply shorten an AudioClip,
        //    so we make a new one with the appropriate length
        var newClip = AudioClip.Create(myAudioClip.name,position,myAudioClip.channels,myAudioClip.frequency,false,false);

        newClip.SetData(newData, 0);        //Give it the data from the old clip

        //Replace the old clip
        AudioClip.Destroy(myAudioClip);
        myAudioClip = newClip;
        SavWav.Save("Mic/CustomRec" + currentIndex, newClip);
		return Path.Combine(Application.persistentDataPath, "Mic/CustomRec" + currentIndex + ".wav");
	}

	public void OnCustomPadClick(int index)
	{
		currentIndex = index;
		StartToRecordMic();
	}

	public void OnRemoveButtonClick()
	{
		if(micPressed)
		{
			return;
		}

		if(!removePressed)
		{
			removePressed = true;
            currentIndex = -1;
            removeButton.SwitchPress();
            LPLeftPanelManager.Instance.SetInGameRecordButtonActive(false);
            LPLeftPanelManager.Instance.SetRemoveButtonActive(true);
            LPLeftPanelManager.Instance.SetPlayButtonActive(false);
			LPLeftPanelManager.Instance.SetMicRecordButtonActive(false);
		}
		else
		{
			removePressed = false;
			removeButton.SwitchRelease();
            LPLeftPanelManager.Instance.SetInGameRecordButtonActive(true);
            LPLeftPanelManager.Instance.SetRemoveButtonActive(true);
            LPLeftPanelManager.Instance.SetPlayButtonActive(true);
			LPLeftPanelManager.Instance.SetMicRecordButtonActive(true);
		}
		
	}

	public void RemoveCustomRecord(int index)
	{
		if(File.Exists(Path.Combine(Application.persistentDataPath, "Mic/CustomRec" + index + ".wav")))
		{
			File.Delete(Path.Combine(Application.persistentDataPath, "Mic/CustomRec" + index + ".wav"));
			customPlayer.RemoveNoteClip(index);
		}
		
		removeButton.SwitchRelease();
		removePressed = false;
		LPLeftPanelManager.Instance.SetInGameRecordButtonActive(true);
        LPLeftPanelManager.Instance.SetRemoveButtonActive(true);
        LPLeftPanelManager.Instance.SetPlayButtonActive(true);
		LPLeftPanelManager.Instance.SetMicRecordButtonActive(true);
		CheckForNotesGray(0.2f);
	}

	public void OnMicButtonClick()
	{
		if(!micPressed)
		{
			timer = 0f;
			micPressed = true;
			currentIndex = -1;
			micButton.SwitchPress();
			LPLeftPanelManager.Instance.SetInGameRecordButtonActive(false);
			LPLeftPanelManager.Instance.SetRemoveButtonActive(false);
			LPLeftPanelManager.Instance.SetPlayButtonActive(false);
		}
		else
		{
			string path = StopRecordMic(isCustomPadSelected());
			if(!isCustomPadSelected())
			{
				micButton.SwitchRelease();
				micPressed = false;
				LPLeftPanelManager.Instance.SetInGameRecordButtonActive(true);
				LPLeftPanelManager.Instance.SetRemoveButtonActive(true);
				LPLeftPanelManager.Instance.SetPlayButtonActive(true);
				currentIndex = -1;
				return;
			}
			if(string.IsNullOrEmpty(path))
			{
				return;
			}
			micPressed = false;
			
			LaunchPadManager.Instance.padButtons[currentIndex].OnPointerUp(null);
			LaunchPadManager.Instance.UnLitPadButton(currentIndex);
			
			customPlayer.SetNoteClip(currentIndex);
			currentIndex = -1;
			micButton.SwitchRelease();
			LPLeftPanelManager.Instance.SetInGameRecordButtonActive(true);
			LPLeftPanelManager.Instance.SetRemoveButtonActive(true);
			LPLeftPanelManager.Instance.SetPlayButtonActive(true);
		}
		
	}

	public bool isRecording()
	{
		return recording;
	}

	public bool isCustomPadSelected()
	{
		return currentIndex >= 0;
	}

	public bool isMicButtonPressed()
	{
		return micPressed;
	}

	public bool isRemoveButtonPressed()
	{
		return removePressed;
	}

}
