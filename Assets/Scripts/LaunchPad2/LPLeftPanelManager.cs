﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LPLeftPanelManager : MonoBehaviour 
{
	public LPLeftPanelButton ingameRecordButton;
	public LPLeftPanelButton playButton;
	public LPLeftPanelButton micRecordButton;
	public LPLeftPanelButton removeButton;

	public static LPLeftPanelManager Instance;
	void Awake()
	{
		Instance = this;
		
	}
	// Use this for initialization
	void Start () 
	{
		SetMicRecordButtonActive(false);
		SetRemoveButtonActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SetInGameRecordButtonActive(bool value)
	{
		ingameRecordButton.SetButtonActive(value);
	}
	public void SetPlayButtonActive(bool value)
	{
		playButton.SetButtonActive(value);
	}
	public void SetMicRecordButtonActive(bool value)
	{
		micRecordButton.SetButtonActive(value);
	}
	public void SetRemoveButtonActive(bool value)
	{
		removeButton.SetButtonActive(value);
	}
}
