﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ColorPresentManager : MonoBehaviour 
{
	public Image colorImage;
	public Color[] colors;

	int colorIndex = 0;
	List<int> colorIndexList;
	bool playing = false;
	Coroutine setColorCo;

	public static ColorPresentManager Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		colorIndexList = new List<int>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void StartPresent(string notePreset)
	{
		colorIndexList.Clear();
		string[] notePresetArray = notePreset.Split('|');
		for (int i = 0; i < notePresetArray.Length; i++)
		{
			if(notePresetArray[i].Length == 1)
			{
				colorIndexList.Add(int.Parse(notePresetArray[i]));
			}
		}
		NotePresetManager.Instance.SetTextActive(false);
		SetColorImageActive(true);
		playing = true;
		colorIndex = 0;
		SetColor(colors[colorIndexList[colorIndex]]);
	}

	public void SetColorImageActive(bool value)
	{
		colorImage.gameObject.SetActive(value);
	}

	public bool IsPlaying()
	{
		return playing;
	}

	public int GetCurrentColorIndex()
	{
		return colorIndexList[colorIndex];
	}

	public void PassNextColor()
	{
		if(IsPlaying())
		{
			colorIndex++;
			if(colorIndex < colorIndexList.Count)
			{
				SetColor(colors[colorIndexList[colorIndex]]);
			}
			else
			{
				StopPresent();
			}
		}
	}

	public void StopPresent()
	{
		if(IsPlaying())
		{
			playing = false;
			NotePresetManager.Instance.SetTextActive(true);
			SetColorImageActive(false);
			if(!IngameRecordManager.Instance.IsRecording())
			{
				LPLeftPanelManager.Instance.SetPlayButtonActive(true);
			}
		}
	}

	void SetColor(Color color)
	{
		if(IsPlaying())
		{
			if(setColorCo != null)
			{
				StopCoroutine(setColorCo);
			}
			colorImage.DOKill();
			setColorCo = StartCoroutine(SetColorTask(color));
		}
		
	}

	IEnumerator SetColorTask(Color color)
	{
		yield return colorImage.DOColor(color * 1.5f,0.1f).WaitForCompletion();
		yield return colorImage.DOColor(color,0.1f).WaitForCompletion();
		Color darker = new Color(color.r / 1.5f, color.g / 1.5f, color.b / 1.5f, color.a );
		colorImage.DOColor(darker, 0.5f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
	}
}
