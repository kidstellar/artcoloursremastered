﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;
using System.IO; // for FileStream
using System; // for BitConverter and Byte Type

public class IngameAudioRecorder : MonoBehaviour
{
	private int bufferSize;
	private int numBuffers;
	private int outputRate = 44100;
	private string fileName = "/LaunchPadIngameRecord";

	private int headerSize = 44; //default for uncompressed wav
	int lastRecordIndex = 0;

	private bool recOutput;

	private FileStream fileStream;

	void Awake()
	{
		AudioSettings.outputSampleRate = outputRate;
		lastRecordIndex = PlayerPrefs.GetInt("a1c_launchpad_lastrecordindex");
	}

	void Start()
	{
		AudioSettings.GetDSPBufferSize(out bufferSize, out numBuffers);
	}

	void Update()
	{
	
	}

	int GetFileIndex()
	{
		for (int i = 0; i < 10; i++)
		{
			if(File.Exists(Application.persistentDataPath + fileName + i + ".wav"))
			{
				continue;
			}
			return i;
		}
		return -1;
	}

	void ShiftFiles()
	{
		for (int i = 9; i >= 0; i--)
		{
			if(i == 9)
			{
				File.Delete(Application.persistentDataPath + fileName + i + ".wav");
			}
			else
			{
				File.Move(Application.persistentDataPath + fileName + i + ".wav",Application.persistentDataPath + fileName + (i + 1) + ".wav");
			}
		}
	}

	void StartWriting(string _name)
	{
		fileStream = new FileStream(_name, FileMode.Create);
		byte emptyByte = new byte();

		for (int i = 0; i < headerSize; i++) //preparing the header
		{
			fileStream.WriteByte(emptyByte);
		}
	}

	void OnAudioFilterRead(float[] data, int channels)
	{
		if (recOutput) 
		{
			ConvertAndWrite(data); //audio data is interlaced
		}
	}

	void ConvertAndWrite(float[] dataSource)
	{
		

		// ----------------------------------------------------
		short[] intData = new short[dataSource.Length];
		//converting in 2 steps : float[] to Int16[], //then Int16[] to Byte[]

		byte[] bytesData = new byte[dataSource.Length * 2];
		//bytesData array is twice the size of
		//dataSource array because a float converted in Int16 is 2 bytes.

		int rescaleFactor = 32767; //to convert float to Int16

		for (int i = 0; i < dataSource.Length; i++) {
			intData[i] = (short)(dataSource[i] * rescaleFactor);
			byte[] byteArr = new byte[2];
			byteArr = BitConverter.GetBytes(intData[i]);
			byteArr.CopyTo(bytesData, i * 2);
		}

		fileStream.Write(bytesData, 0, bytesData.Length);
	}

	void WriteHeader()
	{

		fileStream.Seek(0, SeekOrigin.Begin);

		Byte[] riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
		fileStream.Write(riff, 0, 4);

		Byte[] chunkSize = BitConverter.GetBytes(fileStream.Length - 8);
		fileStream.Write(chunkSize, 0, 4);

		Byte[] wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
		fileStream.Write(wave, 0, 4);

		Byte[] fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
		fileStream.Write(fmt, 0, 4);

		Byte[] subChunk1 = BitConverter.GetBytes(16);
		fileStream.Write(subChunk1, 0, 4);

		UInt16 two = 2;
		UInt16 one = 1;

		Byte[] audioFormat = BitConverter.GetBytes(one);
		fileStream.Write(audioFormat, 0, 2);

		Byte[] numChannels = BitConverter.GetBytes(two);
		fileStream.Write(numChannels, 0, 2);

		Byte[] sampleRate = BitConverter.GetBytes(outputRate);
		fileStream.Write(sampleRate, 0, 4);

		Byte[] byteRate = BitConverter.GetBytes(outputRate * 4);
		// sampleRate * bytesPerSample*number of channels, here 44100*2*2

		fileStream.Write(byteRate, 0, 4);

		UInt16 four = 4;
		Byte[] blockAlign = BitConverter.GetBytes(four);
		fileStream.Write(blockAlign, 0, 2);

		UInt16 sixteen = 16;
		Byte[] bitsPerSample = BitConverter.GetBytes(sixteen);
		fileStream.Write(bitsPerSample, 0, 2);

		Byte[] dataString = System.Text.Encoding.UTF8.GetBytes("data");
		fileStream.Write(dataString, 0, 4);

		Byte[] subChunk2 = BitConverter.GetBytes(fileStream.Length - headerSize);
		fileStream.Write(subChunk2, 0, 4);

		fileStream.Close();
	}

	public string GetFilePath(int index)
	{
		return Application.persistentDataPath + fileName + index + ".wav";
	}

	public int StartRecording()
	{
		if (!recOutput) 
		{
			int index = GetFileIndex();
			if(index == -1)
			{
				return -2;//For list full
			}
			lastRecordIndex = index;
			//StartWriting(Application.persistentDataPath + fileName + lastRecordIndex + ".wav");
			StartWriting(Application.persistentDataPath + fileName + "_temp.wav");
			recOutput = true;
			return lastRecordIndex;
		}
		return -1; 
	}

	public void StopRecording()
	{
		if(recOutput)
		{
			recOutput = false;
			WriteHeader();
			File.Move(Application.persistentDataPath + fileName + "_temp.wav", Application.persistentDataPath + fileName + lastRecordIndex + ".wav");
		}
	}

	public string GetLastFileURL()
	{
		for (int i = 0; i < 10; i++)
		{
			if(File.Exists(Application.persistentDataPath + fileName + lastRecordIndex + ".wav"))
			{
				return Application.persistentDataPath + fileName + lastRecordIndex + ".wav";
			}
		}
		return null;
	}

	public int GetLastRecordIndex()
	{
		if (File.Exists(GetFilePath(lastRecordIndex)))
        {
            return lastRecordIndex;
        }
        else
        {
            for (int i = lastRecordIndex + 1; i < 9 + lastRecordIndex; i++)
            {
				int index = i;
				if(index >= 10)
				{
					index -= 10;
				}
                if (File.Exists(GetFilePath(index)))
                {
					lastRecordIndex = index;
					PlayerPrefs.SetInt("a1c_launchpad_lastrecordindex", lastRecordIndex);
					return index;
                }
            }
        }
		lastRecordIndex = -1;
		PlayerPrefs.SetInt("a1c_launchpad_lastrecordindex", lastRecordIndex);
		return -1;
	}
}