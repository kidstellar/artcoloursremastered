﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LaunchPadManager : MonoBehaviour 
{
	public NotePlayerType currentPlayerType;
	public PadButton[] padButtons;
	public NotePlayer[] notePlayers;
	public NotePlayerTypeButton[] playerTypeButtons;
	public RectTransform recordListButton;

	public Button playPresetButton, stopPresetButton;
	
	bool presetPlaying = false;

	public static LaunchPadManager Instance;
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start () 
	{
		Application.targetFrameRate = 60;
		playerTypeButtons[0].SwitchPress();
	}
	
	// Update is called once per frame
	void Update () 
	{
		// if(Input.GetMouseButtonUp(0) && !ArePlayersPlayingPreset() && !MicrophoneManager.Instance.isMicButtonPressed())
		// {
		// 	fixViolinContinueProblem();
		// }
	}

	public void fixViolinContinueProblem()
	{
		for (int i = 0; i < padButtons.Length; i++)
		{
			padButtons[i].OnRelease();
		}
	}


	public bool ArePlayersPlayingPreset()
	{
		for (int i = 0; i < notePlayers.Length; i++)
		{
			if(notePlayers[i].isPresetPlaying())
			{
				return true;
			}
		}
		return false;
	}

	public void BounceRecordListButton()
	{
		recordListButton.DOKill(true);
		recordListButton.DOPunchScale(Vector3.one * 0.15f, 0.3f, 6, 1f);
	}

	public void ChangePlayerType(int playerType)
	{
		if(MicrophoneManager.Instance.isMicButtonPressed() || MicrophoneManager.Instance.isRemoveButtonPressed())
        {
            return;
        }
		else if(currentPlayerType == NotePlayerType.Violin || currentPlayerType == NotePlayerType.Flute)
		{
			fixViolinContinueProblem();
		}
		currentPlayerType = (NotePlayerType)playerType;
		if(currentPlayerType == NotePlayerType.Custom)
		{
			LPLeftPanelManager.Instance.SetMicRecordButtonActive(true);
			LPLeftPanelManager.Instance.SetRemoveButtonActive(true);
		}
		else
		{
			LPLeftPanelManager.Instance.SetMicRecordButtonActive(false);
			LPLeftPanelManager.Instance.SetRemoveButtonActive(false);
		}
		for (int i = 0; i < padButtons.Length; i++)
		{
			padButtons[i].SetInstrumentIcon(currentPlayerType);
		}
		MicrophoneManager.Instance.CheckForNotesGray(0.08f);

		for (int i = 0; i < playerTypeButtons.Length; i++)
		{
			playerTypeButtons[i].SwitchRelease();
		}
		playerTypeButtons[playerType - 1].SwitchPress();
	}

	public void LitPadButton(int index)
	{
		padButtons[index].Lit();
	}

	public void UnLitPadButton(int index)
	{
		padButtons[index].Unlit();
		padButtons[index].micImage.DOKill();
		padButtons[index].micImage.DOFade(0f, 0.1f);
	}

	public void pressDownPadButton(int index)
	{
		padButtons[index].OnPress();
	}

	public void pressUpPadButton(int index)
	{
		padButtons[index].OnRelease();
	}

	public void SimulatePadButtonClick(int index)
	{
		padButtons[index].SimulateClick();
	}

	public void StopAllPlayers()
	{
		for (int i = 0; i < notePlayers.Length; i++)
		{
			for (int j = 0; j < notePlayers[i].noteClips.Length; j++)
			{
				notePlayers[i].StopNotePad(j);
			}
		}
	}

	public void PlayPreset()
	{
		if(MicrophoneManager.Instance.isMicButtonPressed() || MicrophoneManager.Instance.isRemoveButtonPressed())
        {
            return;
        }
		if(presetPlaying)
		{
			presetPlaying = false;
			StopPreset();
			return;
		}
		presetPlaying = true;
		LPLeftPanelManager.Instance.SetPlayButtonActive(false);
		if(currentPlayerType == NotePlayerType.Piano)
		{
			notePlayers[0].PlayPreset(NotePresetManager.Instance.GetNotesOfMusic());
		}
		else if(currentPlayerType == NotePlayerType.Violin)
		{
			notePlayers[1].PlayPreset(NotePresetManager.Instance.GetNotesOfMusic());
		}
		else if(currentPlayerType == NotePlayerType.Flute)
		{
			notePlayers[2].PlayPreset(NotePresetManager.Instance.GetNotesOfMusic());
		}
		else if(currentPlayerType == NotePlayerType.Drums)
		{
			notePlayers[3].PlayPreset(NotePresetManager.Instance.GetNotesOfMusic());
		}
		else if(currentPlayerType == NotePlayerType.Custom)
		{
			notePlayers[4].PlayPreset(NotePresetManager.Instance.GetNotesOfMusic());
		}
		DisablePlayPresetButton();
	}

	public void StopPreset()
	{
		if(MicrophoneManager.Instance.isMicButtonPressed() || MicrophoneManager.Instance.isRemoveButtonPressed())
        {
            return;
        }

		for (int i = 0; i < notePlayers.Length; i++)
		{
			notePlayers[i].StopPreset();
		}
		// if(currentPlayerType == NotePlayerType.Piano)
		// {
		// 	notePlayers[0].StopPreset();
		// }
		// else if(currentPlayerType == NotePlayerType.Violin)
		// {
		// 	notePlayers[1].StopPreset();
		// }
		// else if(currentPlayerType == NotePlayerType.Flute)
		// {
		// 	notePlayers[2].StopPreset();
		// }
		// else if(currentPlayerType == NotePlayerType.Drums)
		// {
		// 	notePlayers[3].StopPreset();
		// }
		// else if(currentPlayerType == NotePlayerType.Custom)
		// {
		// 	notePlayers[4].StopPreset();
		// }
		presetPlaying = false;
		EnablePlayPresetButton();
	}

	public void EnablePlayPresetButton()
	{
		playPresetButton.gameObject.SetActive(true);
		stopPresetButton.gameObject.SetActive(false);
	}

	public void DisablePlayPresetButton()
	{
		playPresetButton.gameObject.SetActive(false);
		stopPresetButton.gameObject.SetActive(true);
	}
}
