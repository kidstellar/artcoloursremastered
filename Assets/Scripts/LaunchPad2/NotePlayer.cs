﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class NotePlayer : MonoBehaviour 
{
	public NotePlayerType playerType;
	public AudioClip[] noteClips;

	bool presetPlaying = false;
	Coroutine presetCo;

	// Use this for initialization
	void Start () 
	{
		if(playerType == NotePlayerType.Custom)
		{
			StartCoroutine(LoadCustomNotesTask());
		}
	}

	// Update is called once per frame
	void Update () 
	{
		// if(Input.GetKeyDown(KeyCode.Space) && playerType == NotePlayerType.Violin) 
		// {
		// 	PlayPreset();
		// }
	}

	void PlayPianoNote(int noteIndex)
	{
		AudioPoolManager.Instance.PlayClip(noteClips[noteIndex]);
	}

	void PlayViolinNote(int noteIndex)
	{
		ViolinAudioPoolManager.Instance.PlayClip(noteClips[noteIndex]);
	}

	void StopViolinNote(int noteIndex)
	{
		ViolinAudioPoolManager.Instance.StopClip(noteClips[noteIndex]);
	}

	public void SetNoteClip(int index)
	{
		if(playerType == NotePlayerType.Custom)
		{
			StartCoroutine(LoadClipIntoNote(index));
		}
	}

	public void RemoveNoteClip(int index)
	{
		if(playerType == NotePlayerType.Custom)
		{
			noteClips[index] = null;
		}
	}

	public void PlayNotePad(int noteIndex)
	{
		if(playerType == NotePlayerType.Piano || playerType == NotePlayerType.Drums || playerType == NotePlayerType.Custom)
		{
			PlayPianoNote(noteIndex);
		}
		else if(playerType == NotePlayerType.Violin || playerType == NotePlayerType.Flute)
		{
			PlayViolinNote(noteIndex);
		}
	}

	public void StopNotePad(int noteIndex)
	{
		if(playerType == NotePlayerType.Violin || playerType == NotePlayerType.Flute)
		{
			StopViolinNote(noteIndex);
		}
	}

	public void StopPreset()
	{
		if(presetCo != null) 
		{
			for (int i = 0; i < 8; i++)
			{
				LaunchPadManager.Instance.padButtons[i].StopSimulateClick();
			}
			StopCoroutine(presetCo);
		}
		if(playerType == NotePlayerType.Violin || playerType == NotePlayerType.Flute || playerType == NotePlayerType.Custom)
		{
			for (int i = 0; i < 8; i++)
			{
				StopViolinNote(i);
			}
			
		}

		for (int i = 0; i < 8; i++)
		{
			LaunchPadManager.Instance.pressUpPadButton(i);
		}
		presetPlaying = false;
		if(!IngameRecordManager.Instance.IsRecording())
		{
			LPLeftPanelManager.Instance.SetPlayButtonActive(true);
		}
		
	}

	public void PlayPreset(NotePreset notePreset)
	{
		if(presetCo != null) 
		{
			StopCoroutine(presetCo);
		}
		
		if(playerType == NotePlayerType.Piano || playerType == NotePlayerType.Drums || playerType == NotePlayerType.Custom)
		{
			presetCo = StartCoroutine(PlayPianoPresetTask(notePreset));
		}
		else if(playerType == NotePlayerType.Violin || playerType == NotePlayerType.Flute)
		{
			presetCo = StartCoroutine(PlayViolinPresetTask(notePreset));
		}
		
	}

	public bool isPresetPlaying()
	{
		return presetPlaying;
	}

	IEnumerator LoadClipIntoNote(int index)
	{
		string filepath = Path.Combine(Application.persistentDataPath, "Mic/CustomRec" + index + ".wav");
        using (var www = new WWW("file://" + filepath))
        {
            yield return www;
            if (string.IsNullOrEmpty(www.error))
            {
                noteClips[index] = www.GetAudioClip();
            }
            else
            {
                noteClips[index] = null;
            }
        }
		MicrophoneManager.Instance.CheckForNotesGray(0.2f);
	}

	IEnumerator PlayPianoPresetTask(NotePreset notePreset)
	{
		presetPlaying = true;
		yield return new WaitForSeconds(0.3f);
		string[] notes = notePreset.notes.Split('|');
		for (int i = 0; i < notes.Length; i++) 
		{
			
			if(notes[i].Length > 2) 
			{
				yield return new WaitForSeconds(float.Parse(notes[i]) * notePreset.GetDelayForPlayerType(playerType));
				
			} 
			else 
		  	{
				if(i > 0 && notes[i-1].Length == 1)
				{
					yield return new WaitForSeconds(notePreset.delayPerSpace * notePreset.GetDelayForPlayerType(playerType));
				}
				LaunchPadManager.Instance.SimulatePadButtonClick(int.Parse(notes[i]));
				//PlayPianoNote();
			}
		}
		presetPlaying = false;
		yield return new WaitForSeconds(0.5f);
		LaunchPadManager.Instance.StopPreset();
		ColorPresentManager.Instance.StartPresent(notePreset.notes);
	}

	IEnumerator LoadCustomNotesTask()
	{
		noteClips = new AudioClip[9];
        for (int i = 0; i < noteClips.Length; i++)
        {
			string filepath = Path.Combine(Application.persistentDataPath, "Mic/CustomRec" + i + ".wav");
            using (var www = new WWW("file://" + filepath))
            {
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    noteClips[i] = www.GetAudioClip();
                }
                else
                {
					noteClips[i] = null;
                }
            }
            
        }
	}

	IEnumerator PlayViolinPresetTask(NotePreset notePreset)
	{
		yield return new WaitForSeconds(0.3f);
		string[] notes = notePreset.notes.Split('|');
		for (int i = 0; i < notes.Length; i++) 
		{
			if(notes[i].Length > 2) 
			{
				float delay = float.Parse(notes[i]) * notePreset.GetDelayForPlayerType(playerType);
				yield return new WaitForSeconds(delay / 2f);
				LaunchPadManager.Instance.pressUpPadButton(int.Parse(notes[i-1]));
				yield return new WaitForSeconds(delay / 2f);
			} 
			else 
		  	{
				if(i > 0 && notes[i-1].Length == 1)
				{
					float delay = notePreset.delayPerSpace * notePreset.GetDelayForPlayerType(playerType);
					yield return new WaitForSeconds(delay / 2f);
					LaunchPadManager.Instance.pressUpPadButton(int.Parse(notes[i-1]));
					yield return new WaitForSeconds(delay / 2f);
				}
				LaunchPadManager.Instance.pressDownPadButton(int.Parse(notes[i]));
				
				
			}
		}
		yield return new WaitForSeconds(0.5f);
		LaunchPadManager.Instance.pressUpPadButton(int.Parse(notes[notes.Length - 1]));
		presetPlaying = false;
		LaunchPadManager.Instance.StopPreset();
		ColorPresentManager.Instance.StartPresent(notePreset.notes);
	}
}

public enum NotePlayerType
{
	None, Piano, Violin, Drums, Flute, Custom
}