﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PadButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	public NotePlayer pianoPlayer;
	public NotePlayer violinPlayer;
	public NotePlayer flutePlayer;
	public NotePlayer drumsPlayer;
	public NotePlayer customPlayer;
	public Image lightImage;
	public Image grayImage;
	public Image micImage;
	public Image[] instrumentIconImages;
	public float pressDistanceX = 0f;
	public float pressDistanceY = -10f;
	[Space]
	public int noteIndex;
	public Color defaultColor = Color.white;
	public Color litColor;
	public Image buttonImage;

	Coroutine simulateClickCo;
	RectTransform rectTransform;
	float firstX;
	float firstY;
	bool holdingWithPointerEnter = false;
	bool holding = false;
	
	// Use this for initialization
	void Start () 
	{
		rectTransform = GetComponent<RectTransform>();
		firstX = rectTransform.anchoredPosition.x;
		firstY = rectTransform.anchoredPosition.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButtonUp(0) && holdingWithPointerEnter)
		{
			OnPointerUp(null);
		}
	}

	public void OnClick()
	{
		
	}

	public void Lit()
	{
		lightImage.DOKill();
		lightImage.DOFade(1f, 0.1f);
		rectTransform.DOKill();
		rectTransform.DOAnchorPos(new Vector2(firstX + pressDistanceX, firstY + pressDistanceY), 0.1f);
		//buttonImage.color = litColor;
	}

	public void Unlit()
	{
		lightImage.DOKill();
		lightImage.DOFade(0f, 0.1f);
		rectTransform.DOKill();
		rectTransform.DOAnchorPos(new Vector2(firstX, firstY), 0.1f);
		//buttonImage.color = defaultColor;
	}

	public void ActivateCustomGray(bool value, float duration)
	{
		if(value)
		{
			grayImage.DOKill();
			grayImage.DOFade(1f, duration);
		}
		else
		{
			grayImage.DOKill();
			grayImage.DOFade(0f, duration);
		}
	}


    public void OnPointerUp(PointerEventData eventData)
    {
		if(!holding)
		{
			return;
		}
		if(MicrophoneManager.Instance.isMicButtonPressed())
		{
			return;
		}
		else if(MicrophoneManager.Instance.isRemoveButtonPressed())
		{
			Unlit();
			holding = false;
			MicrophoneManager.Instance.RemoveCustomRecord(noteIndex);
			return;
		}
		else
		{
			OnRelease();
			IngameRecordManager.Instance.AddUnlitToArray(noteIndex);
		}
		holdingWithPointerEnter = false;
		holding = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
		if(holding)
		{
			return;
		}
		holding = true;
		if(MicrophoneManager.Instance.isMicButtonPressed())
		{
			if(MicrophoneManager.Instance.isCustomPadSelected())
			{
				holding = false;
				return;
			}
			MicrophoneManager.Instance.OnCustomPadClick(noteIndex);
			Lit();
			micImage.DOKill();
			micImage.DOFade(1f, 0.4f).SetLoops(-1, LoopType.Yoyo);
		}
		else if(MicrophoneManager.Instance.isRemoveButtonPressed())
		{
			Lit();
			return;
		}
		else
		{
			if(ColorPresentManager.Instance.IsPlaying() && noteIndex == ColorPresentManager.Instance.GetCurrentColorIndex())
			{
				ColorPresentManager.Instance.PassNextColor();
			}
			OnPress();
			IngameRecordManager.Instance.AddLitToArray(noteIndex);
		}
    }

	public void OnPointerEnter(PointerEventData eventData)
    {
        if(Input.GetMouseButton(0) && !holding)
		{
			holdingWithPointerEnter = true;
			OnPointerDown(eventData);
		}
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(Input.GetMouseButton(0) && holding)
		{
			OnPointerUp(eventData);
		}
    }

	public void SetInstrumentIcon(NotePlayerType playerType)
	{
		// for (int i = 0; i < instrumentIconImages.Length; i++)
		// {
		// 	instrumentIconImages[i].gameObject.SetActive(false);
		// }
		// if(playerType == NotePlayerType.Piano)
		// {
		// 	instrumentIconImages[0].gameObject.SetActive(true);
		// }
		// else if(playerType == NotePlayerType.Violin)
		// {
		// 	instrumentIconImages[1].gameObject.SetActive(true);
		// }
		// else if(playerType == NotePlayerType.Flute)
		// {
		// 	instrumentIconImages[2].gameObject.SetActive(true);
		// }
		// else if(playerType == NotePlayerType.Drums)
		// {
		// 	instrumentIconImages[3].gameObject.SetActive(true);
		// }
		// else if(playerType == NotePlayerType.Custom)
		// {
		// 	instrumentIconImages[4].gameObject.SetActive(true);
		// }
	}
	
	public void OnRelease()
	{
		Unlit();
		if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Violin)
		{
			violinPlayer.StopNotePad(noteIndex);
		}
		else if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Flute)
		{
			flutePlayer.StopNotePad(noteIndex);
		}
		else if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Custom)
		{
			customPlayer.StopNotePad(noteIndex);
		}
	}

	public void OnPress()
	{
		Lit();
		if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Piano)
		{
			pianoPlayer.PlayNotePad(noteIndex);
		}
		else if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Violin)
		{
			violinPlayer.PlayNotePad(noteIndex);
		}
		else if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Flute)
		{
			flutePlayer.PlayNotePad(noteIndex);
		}
		else if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Drums)
		{
			drumsPlayer.PlayNotePad(noteIndex);
		}
		else if(LaunchPadManager.Instance.currentPlayerType == NotePlayerType.Custom)
		{
			customPlayer.PlayNotePad(noteIndex);
		}
		
	}

    

    public void OnPointerClick(PointerEventData eventData)
    {
        
    }

	public void SimulateClick()
	{
		if(simulateClickCo != null)
		{
			StopCoroutine(simulateClickCo);
		}
		simulateClickCo = StartCoroutine(SimulateClickTask());
	}

	public void StopSimulateClick()
	{
		if(simulateClickCo != null)
		{
			StopCoroutine(simulateClickCo);
		}
	}

	IEnumerator SimulateClickTask()
	{
		OnPress();
		yield return new WaitForSeconds(0.15f);
		OnRelease();
	}
}
