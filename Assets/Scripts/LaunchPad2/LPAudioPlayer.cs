﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LPAudioPlayer : MonoBehaviour 
{
	bool ready = true;
	bool playing = false;
	AudioSource audioSource;

	// Use this for initialization
	void Start () 
	{
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public bool isReady()
	{
		return ready;
	}

	public void StopClip(AudioClip clip)
	{
		if(playing && audioSource.clip == clip)
		{
			StopAllCoroutines();
			audioSource.DOFade(0, 0.2f).OnComplete(() => 
			{
				audioSource.Stop();
				ready = true;
				playing = false;
			});
		}
	}

	public void PlayClip(AudioClip clip)
	{
		ready = false;
		playing = true;
		StartCoroutine(PlayClipTask(clip));
	}

	IEnumerator PlayClipTask(AudioClip clip)
	{
		audioSource.volume = 0.5f;
		audioSource.PlayOneShot(clip);
		yield return new WaitForSeconds(clip.length);
		audioSource.DOFade(0f, 0.2f);
		playing = false;
		yield return new WaitForSeconds(0.3f);
		audioSource.clip = null;
		ready = true;
	}


}
