﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using PADA;
using System.Net;
using System.IO;

public class A1CPadaManager : MonoBehaviour
{
    public string hostName = "http://89.252.185.164:5001";
    public string hostLocation = "/api/child/";
    public string loginLocation = "/api/TokenAuth/Authenticate/";

    public string knowledgePrefix = "know";
    public string abilityPrefix = "ability";
    public string favPrefix = "fav";
    public string sessionPrefix = "session";


    public int childId = 0;
    public int gameId = 23;
    public int childGameId = 0;
    public bool isEnabled = false;

    private int bookStartIndex = 320;
    private int colorKnowId = 102;

    private bool gamePaused = false;
    private List<RequestPack> sessionRequestList;
    private List<RequestPack> favRequestList;
    private List<RequestPack> knowRequestList;
    private List<RequestPack> abilityRequestList;

    private Dictionary<string, int> sceneNameDict = new Dictionary<string, int>
    {
        {"PlayScreen", 513},
        {"MainLevelScreen", 512},
        {"DyeMakerExterior", 514},
        {"PlushToyMaker", 502},
        {"GameCentreInterior", 515},
        {"ArtMusicInterior", 516},
        {"FlowerGarden", 505},
        {"DyeLab", 506},
        {"ExtractionMachine", 517},
        {"ChemistrySet", 518},
        {"ColorDropper", 519},
        {"ShotPut", 507},
        {"DontTouchTheRope", 508},
        {"RingInARingRing", 509},
        {"Drawing Book", 510},
        {"LaunchPad", 511}

    };

    private Dictionary<string, int> colorNameDict = new Dictionary<string, int>
    {
        {"Red", 6},
        {"Yellow", 5},
        {"Blue", 4},
        {"Pink", 7},
        {"Green", 8},
        {"Orange", 9},
        {"Purple", 10},
        {"Brown", 11},
        {"Grey", 12},
        {"Black", 13},
        {"Dark Red", 116},
        {"Light Yellow", 117},
        {"Olive Green", 119},
        {"Light Blue", 120},
        {"Navy Blue", 121},
        {"Silver", 123},
        {"Beige", 125},
        {"Dark Yellow", 126},
        {"Light Green", 127},
        {"White", 115},
        {"Turquoise", 124},
    };

    private Dictionary<string, int> characterNameDict = new Dictionary<string, int>
    {
        {"Pasha", 14},
        {"Pika", 15},
        {"Rose", 16},
        {"BigMax", 17}

    };

    private Dictionary<string, int> animalNameDict = new Dictionary<string, int>
    {
        {"Woodpecker", 65},
        {"Beaver", 66},
        {"Mole", 67},
        {"HoneyBee", 68},
        {"Frog", 69},
        {"Bear", 70},
        {"Squirrel", 71},
        {"Owl", 72},
        {"Firefly", 73},
        {"Skunk", 74},
        {"Rabbit", 75},
        {"Racoon", 76},
        {"Hedgehog", 77},
        {"Deer", 78},
        {"Turtle", 79}

    };

    private string currentScene = "";
    private string startDate = "";
    private string endDate = "";

    DateTime startDateTime;
    DateTime endDateTime;

    public static A1CPadaManager Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            sessionRequestList = new List<RequestPack>();
            favRequestList = new List<RequestPack>();
            knowRequestList = new List<RequestPack>();
            abilityRequestList = new List<RequestPack>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }

    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public int getAnimalIndexFromName(string _name)
    {
        int sayac = 0;
        foreach (KeyValuePair<string, int> kvp in animalNameDict)
        {
            if (kvp.Key == _name)
            {
                return sayac;
            }
            sayac++;
        }
        return -1;

    }

    public DateTime getCurrentDate()
    {
        return DateTime.Now;
    }

    string getCurrentDateString()
    {
        DateTime currentDate = DateTime.Now;
        string result = "";
        result += currentDate.Year.ToString("00") + "-";
        result += currentDate.Month.ToString("00") + "-";
        result += currentDate.Day.ToString("00") + "T";
        result += currentDate.TimeOfDay.Hours.ToString("00") + ":";
        result += currentDate.TimeOfDay.Minutes.ToString("00") + ":";
        result += currentDate.TimeOfDay.Seconds.ToString("00");
        return result;
    }

    string getDateString(DateTime date)
    {
        DateTime currentDate = date;
        string result = "";
        result += currentDate.Year.ToString("00") + "-";
        result += currentDate.Month.ToString("00") + "-";
        result += currentDate.Day.ToString("00") + "T";
        result += currentDate.TimeOfDay.Hours.ToString("00") + ":";
        result += currentDate.TimeOfDay.Minutes.ToString("00") + ":";
        result += currentDate.TimeOfDay.Seconds.ToString("00");
        return result;
    }

    string getANewGuid()
    {
        return System.Guid.NewGuid().ToString();
    }

    public int calculatePercentage(string sceneName)
    {
        if (sceneName == "Woodpecker")
        {
            int level = PlayerPrefs.GetInt("a1f_woodpecker_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Beaver")
        {
            int level = PlayerPrefs.GetInt("a1f_beaver_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Mole")
        {
            int level = PlayerPrefs.GetInt("a1f_mole_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "HoneyBee")
        {
            int level = PlayerPrefs.GetInt("a1f_honeybee_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Frog")
        {
            int level = PlayerPrefs.GetInt("a1f_frog_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Bear")
        {
            int level = PlayerPrefs.GetInt("a1f_bear_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Squirrel")
        {
            int level = PlayerPrefs.GetInt("a1f_squirrel_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Owl")
        {
            int level = PlayerPrefs.GetInt("a1f_owl_completed");
            return Mathf.Min(level * 100, 100);
        }
        else if (sceneName == "Firefly")
        {
            int level = PlayerPrefs.GetInt("a1f_firefly_completed");
            return Mathf.Min(level * 100, 100);
        }
        return 0;
    }

    public void setCurrentScene(string sceneName)
    {
        if (currentScene.Equals(sceneName))
        {
            return;
        }
        if (currentScene != "")
        {
            endDate = getCurrentDateString();
            endDateTime = DateTime.Now;
            Debug.Log("Sending " + currentScene);
            //A1FMiniPadaManager.Instance.recordPlayTimes(currentScene, endDateTime.Subtract(startDateTime));
            int percent = calculatePercentage(currentScene);
            addSession(percent);
        }
        startDate = getCurrentDateString();
        startDateTime = DateTime.Now;
        currentScene = sceneName;
        gameId = sceneNameDict[currentScene];

    }

    public void addBookSession(DateTime startDate, DateTime endDate, int gameId)
    {
        if (gameId < 0)
        {
            return;
        }
        //A1FMiniPadaManager.Instance.recordPlayTimes("Book", endDate.Subtract(startDate));
        addCustomSession(startDate, endDate, bookStartIndex + gameId);
    }

    public void addCustomSession(DateTime startDate, DateTime endDate, int gameId)
    {
        try
        {
            SessionRequest sq = new SessionRequest();
            sq.childGameId = childGameId;
            sq.gameId = gameId;
            sq.childId = childId;
            sq.percent = 0;
            sq.start = getDateString(startDate);
            sq.end = getDateString(endDate);
            //sq.sound = !MuteManager.Instance.isSFXMuted;
            //sq.music = !MuteManager.Instance.isMusicMuted;
            sq.sound = true;
            sq.music = true;
            string jsonString = JsonUtility.ToJson(sq);
            sessionRequestList.Add(new RequestPack(0, jsonString));
            //BLWebRequester.Instance.sendRequest(hostName + hostLocation + sessionPrefix, jsonString, 0);
        }
        catch (Exception ex)
        {
            //Crashlytics.RecordCustomException("PADA Session", "When adding session", ex.StackTrace);
        }
    }



    public void addSession(int percent)
    {
        try
        {
            addAchiever();
            SessionRequest sq = new SessionRequest();
            sq.childGameId = childGameId;
            sq.gameId = gameId;
            sq.childId = childId;
            sq.percent = percent;
            sq.start = startDate;
            sq.end = endDate;
            //sq.sound = !MuteManager.Instance.isSFXMuted;
            //sq.music = !MuteManager.Instance.isMusicMuted;
            sq.sound = true;
            sq.music = true;
            string jsonString = JsonUtility.ToJson(sq);
            sessionRequestList.Add(new RequestPack(0, jsonString));
            //BLWebRequester.Instance.sendRequest(hostName + hostLocation + sessionPrefix, jsonString, 0);
        }
        catch (Exception ex)
        {
            //Crashlytics.RecordCustomException("PADA Session", "When adding session", ex.StackTrace);
        }

    }

    public void addFav(int favTypeId, int favItemId)
    {
        try
        {
            FavRequest req = new FavRequest();
            req.childGameId = childGameId;
            req.gameId = gameId;
            req.childId = childId;
            req.favItemId = favItemId;
            req.favTypeId = favTypeId;
            req.date = getCurrentDateString();
            string jsonString = JsonUtility.ToJson(req);
            favRequestList.Add(new RequestPack(1, jsonString));
            //BLWebRequester.Instance.sendRequest(hostName + hostLocation + favPrefix, jsonString, 1);
        }
        catch (Exception ex)
        {
            //Crashlytics.RecordCustomException("PADA Favourite", "When adding favourites", ex.StackTrace);
        }

    }

    public void addRecogAnimals(int animalIndex)
    {
        addKnow(1, 20, 52 + animalIndex);
    }

    public void addRecogAnimalFeatures(int animalIndex)
    {
        addKnow(1, 21, 52 + animalIndex);
    }

    public void addRecogAnimalEatHabits(int animalIndex)
    {
        addKnow(1, 23, 52 + animalIndex);
    }

    public void addRecogAnimalNocturnal(int animalIndex)
    {
        addKnow(1, 24, 52 + animalIndex);
    }

    public void addKnow(int status, int catId, int itemId)
    {
        try
        {
            KnowRequest req = new KnowRequest();
            req.childGameId = childGameId;
            req.gameId = gameId;
            req.childId = childId;
            req.date = getCurrentDateString();
            req.knowStatus = status;
            req.knowCategoryId = catId;
            req.knowItemId = itemId;
            string jsonString = JsonUtility.ToJson(req);
            Debug.Log(jsonString);
            knowRequestList.Add(new RequestPack(0, jsonString));
            //BLWebRequester.Instance.sendRequest(hostName + hostLocation + sessionPrefix, jsonString, 0);
        }
        catch (Exception ex)
        {
            //Crashlytics.RecordCustomException("PADA Session", "When adding session", ex.StackTrace);
        }
    }

    public void addAbility(int status, int itemId)
    {
        try
        {
            AbilityRequest req = new AbilityRequest();
            req.childGameId = childGameId;
            req.gameId = gameId;
            req.childId = childId;
            req.date = getCurrentDateString();
            req.abilityStatus = status;
            req.abilityItemId = itemId;
            string jsonString = JsonUtility.ToJson(req);
            Debug.Log(jsonString);
            abilityRequestList.Add(new RequestPack(0, jsonString));
            //BLWebRequester.Instance.sendRequest(hostName + hostLocation + sessionPrefix, jsonString, 0);
        }
        catch (Exception ex)
        {
            //Crashlytics.RecordCustomException("PADA Session", "When adding session", ex.StackTrace);
        }
    }

    public void addMapDevelopmentSkills()
    {
        addAbility(1, 50);
        addAbility(1, 54);
        addAbility(1, 27);
    }

    public void addWoodpeckerDevelopmentSkills()
    {
        addAbility(1, 20);
        addAbility(1, 13);
        addAbility(1, 31);
        addAbility(1, 18);
        addAbility(1, 29);
        addAbility(1, 21);
        addAbility(1, 19);
    }

    public void addMoleDevelopmentSkills()
    {
        addAbility(1, 14);
        addAbility(1, 44);
        addAbility(1, 19);
        addAbility(1, 31);
    }

    public void addHoneyBeeDevelopmentSkills()
    {
        addAbility(1, 30);
        addAbility(1, 13);
        //Counting
        addAbility(1, 18);
    }

    public void addSquirrelDevelopmentSkills()
    {
        addAbility(1, 25);
        addAbility(1, 44);
        addAbility(1, 27);
        addAbility(1, 19);
        addAbility(1, 31);
    }

    public void addBearDevelopmentSkills()
    {
        addAbility(1, 18);
        addAbility(1, 31);
        addAbility(1, 25);
        addAbility(1, 19);
    }

    public void addFrogDevelopmentSkills()
    {
        addAbility(1, 25);
        addAbility(1, 19);
        addAbility(1, 44);
    }

    public void addBeaverDevelopmentSkills()
    {
        addAbility(1, 42);
        addAbility(1, 44);
        addAbility(1, 43);
        addAbility(1, 28);
    }

    public void addOwlDevelopmentSkills()
    {
        addAbility(1, 27);
        addAbility(1, 25);
        addAbility(1, 19);
    }

    public void addFireflyDevelopmentSkills()
    {
        addAbility(1, 50);
    }

    public void addCurious()
    {
        addAbility(1, 40);
    }

    public void addAchiever()
    {
        //{ "Woodpecker", 44},
        //{ "HoneyBee", 45},
        //{ "Firefly", 46},
        //{ "Squirrel", 47},
        //{ "Mole", 48},
        //{ "Owl", 49},
        //{ "Bear", 50},
        //{ "Beaver", 51},
        //{ "Frog", 80}
        if (PlayerPrefs.GetInt("a1f_ability_achiever_unlocked") == 0)
        {
            int total = 0;
            string[] scenes = { "Woodpecker", "HoneyBee", "Firefly", "Squirrel", "Mole", "Owl", "Bear", "Beaver", "Frog" };
            for (int i = 0; i < scenes.Length; i++)
            {
                total += calculatePercentage(scenes[i]);
            }
            if (total / scenes.Length == 100)
            {
                addAbility(1, 4);
                PlayerPrefs.SetInt("a1f_ability_achiever_unlocked", 1);
            }
        }
    }

    public void addColor(string colorName)
    {
        addFav(4, colorNameDict[colorName]);
    }

    public void addCharacter(string characterName)
    {
        addFav(7, characterNameDict[characterName]);
    }

    public void addNumber(int number)
    {
        addFav(8, 18 + number);
    }

    public void addLetter(int letterId)
    {
        addFav(9, 29 + letterId);
    }

    public void addSport(int sportId)
    {
        addFav(10, sportId);
    }

    public void addAnimal(int animalId)
    {
        Debug.Log("Animal : " + animalId);
        addFav(12, animalId);
    }

    public void addColorRecognition(int colorId, int status)
    {
        addKnow(status, 38, colorKnowId + colorId);
    }

    //public void addColorRecognition(int colorId, int status)
    //{
    //    addKnow(status, 38, colorKnowId + colorId);
    //}

    public void sendAllRequests()
    {
        sendAllSessionRequests();
        sendAllFavRequests();
        sendAllKnowRequests();
        sendAllAbilityRequests();
    }

    void sendAllSessionRequests()
    {
        if (sessionRequestList.Count == 0)
        {
            return;
        }
        if (!isEnabled)
        {
            PadaOfflineManager.Instance.appendSessions(sessionRequestList);
            sessionRequestList.Clear();
            return;
        }
        string reqString = "[";
        for (int i = 0; i < sessionRequestList.Count; i++)
        {
            Debug.Log("Session ::: " + sessionRequestList[i].jsonString);
            reqString += sessionRequestList[i].jsonString;
            if (i < sessionRequestList.Count - 1)
            {
                reqString += ",";
            }
        }
        reqString += "]";

        A1CWebRequester.Instance.sendRequest(hostName + hostLocation + sessionPrefix, reqString, 0);
        sessionRequestList.Clear();
    }

    void sendAllFavRequests()
    {
        if (favRequestList.Count == 0)
        {
            return;
        }
        if (!isEnabled)
        {
            PadaOfflineManager.Instance.appendFavs(favRequestList);
            favRequestList.Clear();
            return;
        }
        string reqString = "[";
        for (int i = 0; i < favRequestList.Count; i++)
        {
            Debug.Log("Fav ::: " + favRequestList[i].jsonString);
            reqString += favRequestList[i].jsonString;
            if (i < favRequestList.Count - 1)
            {
                reqString += ",";
            }
        }
        reqString += "]";
        A1CWebRequester.Instance.sendRequest(hostName + hostLocation + favPrefix, reqString, 1);
        favRequestList.Clear();
    }

    void sendAllKnowRequests()
    {
        if (knowRequestList.Count == 0)
        {
            return;
        }
        if (!isEnabled)
        {
            PadaOfflineManager.Instance.appendKnowledges(knowRequestList);
            knowRequestList.Clear();
            return;
        }
        string reqString = "[";
        for (int i = 0; i < knowRequestList.Count; i++)
        {
            Debug.Log("Know ::: " + knowRequestList[i].jsonString);
            reqString += knowRequestList[i].jsonString;
            if (i < knowRequestList.Count - 1)
            {
                reqString += ",";
            }
        }
        reqString += "]";
        A1CWebRequester.Instance.sendRequest(hostName + hostLocation + knowledgePrefix, reqString, 2);
        knowRequestList.Clear();
    }

    void sendAllAbilityRequests()
    {
        if (abilityRequestList.Count == 0)
        {
            return;
        }
        if (!isEnabled)
        {
            PadaOfflineManager.Instance.appendAbilities(abilityRequestList);
            abilityRequestList.Clear();
            return;
        }
        string reqString = "[";
        for (int i = 0; i < abilityRequestList.Count; i++)
        {
            Debug.Log("Ability ::: " + abilityRequestList[i].jsonString);
            reqString += abilityRequestList[i].jsonString;
            if (i < abilityRequestList.Count - 1)
            {
                reqString += ",";
            }
        }
        reqString += "]";
        A1CWebRequester.Instance.sendRequest(hostName + hostLocation + abilityPrefix, reqString, 3);
        abilityRequestList.Clear();
    }
    // void OnApplicationFocus(bool hasFocus)
    // {
    //     Debug.Log("Focus : " + hasFocus);
    // }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            gamePaused = true;
            endDate = getCurrentDateString();
            int percent = calculatePercentage(currentScene);
            addSession(percent);
            sendAllRequests();
        }
        else
        {
            startDate = getCurrentDateString();
        }
    }






}


// public class WebRequestJob : ThreadedJob
// {
//     public string url;
//  public string data;
//  public string result;

//     protected override void ThreadFunction()
//     {
//         // Do your threaded task. DON'T use the Unity API here
//         WebRequest request = WebRequest.Create(url);
//      request.ContentType = "application/json";
//      request.Method = "POST";
//      using (var streamWriter = new StreamWriter(request.GetRequestStream()))
//      {
//             streamWriter.Write(data);
//             streamWriter.Flush();
//             streamWriter.Close();
//         }
//         var httpResponse = (HttpWebResponse)request.GetResponse();
//         using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
//         {
//             var result = streamReader.ReadToEnd();
//          this.result = result;
//          Debug.Log(result);
//         }
//     }
//     protected override void OnFinished()
//     {
//         // This is executed by the Unity main thread when the job is finished



//     }
// }

namespace PADA
{
    [Serializable]
    public class RequestPack
    {
        public int type;
        public string jsonString;

        public RequestPack(int type, string jsonString)
        {
            this.type = type;
            this.jsonString = jsonString;
        }
    }

    [Serializable]
    public class RequestBase
    {
        public int childId;
        public int gameId;
        public int childGameId;
    }

    [Serializable]
    public class SessionRequest : RequestBase
    {
        public string start;
        public string end;
        public int percent;
        public bool sound;
        public bool music;
    }

    [Serializable]
    public class SessionResult
    {
        public int id;
        public string start;
        public string end;
        public int percent;
        public int childId;
        public int gameId;
        public int childGameId;
    }

    [Serializable]
    public class SessionResponse
    {
        public SessionResult result;
        public object targetUrl;
        public bool success;
        public ResponseError error;
        public bool unAuthorizedRequest;
        public bool __abp;
    }

    [Serializable]
    public class FavRequest : RequestBase
    {
        public int favTypeId;
        public int favItemId;
        public string date;
    }

    [Serializable]
    public class FavResult
    {
        public int id;
        public int favTypeId;
        public int favItemId;
        public int childId;
        public int gameId;
        public int childGameId;
        public string date;
    }

    [Serializable]
    public class FavResponse
    {
        public FavResult result;
        public object targetUrl;
        public bool success;
        public ResponseError error;
        public bool unAuthorizedRequest;
        public bool __abp;
    }

    [Serializable]
    public class AbilityRequest : RequestBase
    {
        public int abilityStatus;
        public int abilityItemId;
        public string date;
    }

    [Serializable]
    public class AbilityResult
    {
        public int id;
        public int abilityStatus;
        public int abilityItemId;
        public int childId;
        public int gameId;
        public int childGameId;
        public string date;
    }

    [Serializable]
    public class AbilityResponse
    {
        public AbilityResult result;
        public object targetUrl;
        public bool success;
        public ResponseError error;
        public bool unAuthorizedRequest;
        public bool __abp;
    }

    [Serializable]
    public class KnowRequest : RequestBase
    {
        public int knowStatus;
        public int knowCategoryId;
        public int knowItemId;
        public string date;
    }

    [Serializable]
    public class KnowResult
    {
        public int id;
        public int knowStatus;
        public int knowCategoryId;
        public int knowItemId;
        public int childId;
        public int gameId;
        public int childGameId;
        public string date;
    }

    [Serializable]
    public class KnowResponse
    {
        public KnowResult result;
        public object targetUrl;
        public bool success;
        public ResponseError error;
        public bool unAuthorizedRequest;
        public bool __abp;
    }

    [Serializable]
    public class LoginRequest
    {
        public string userNameOrEmailAddress;
        public string password;
    }

    [Serializable]
    public class LoginResult
    {
        public string accessToken;
        public string encryptedAccessToken;
        public int expireInSeconds;
        public int userId;
    }

    [Serializable]
    public class LoginResponse
    {
        public LoginResult result;
        public object targetUrl;
        public bool success;
        public ResponseError error;
        public bool unAuthorizedRequest;
        public bool __abp;
    }

    [Serializable]
    public class ResponseError
    {
        public int code;
        public string message;
        public object details;
        public object validationErrors;
    }
}
