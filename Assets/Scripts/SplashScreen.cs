﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SplashScreen : MonoBehaviour
{
    public VideoPlayer _videoPlayer;
    public string nextSceneName = "PlayScreen";

    // Use this for initialization
    void Start()
    {
        Application.targetFrameRate = 60;
        StartCoroutine(splashScreenTask());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator splashScreenTask()
    {
       yield return FaderController.Instance.unfadeScreen(0.5f);
        //_videoPlayer.Stop();
        //_videoPlayer.Play();
        //Debug.Log("Prepared : " + _videoPlayer.isPrepared);
        //Debug.Log("Playing : " + _videoPlayer.isPlaying);
        yield return new WaitForSeconds(4f);
        yield return FaderController.Instance.fadeScreen(0.5f);
        SceneManager.LoadScene(nextSceneName);
    }
}
