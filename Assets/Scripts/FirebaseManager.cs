﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;
using Firebase;
using System;
using System.Threading.Tasks;

public class FirebaseManager : MonoBehaviour
{
    public bool isActive = false;
    public bool isInitialized = false;
    public bool isRCDeveloperMode = false;

    private string lastSceneName = "";

    public static FirebaseManager Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }

    }
    // Use this for initialization
    void Start()
    {
        setupFirebase();

    }

    void setupFirebase()
    {
        try
        {
            Debug.Log("-----------------------Starting Firebase-----------------------");
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    isInitialized = true;
                    isActive = true;
                    //FirebaseAnalytics.SetCurrentScreen("SplashScreen", "SplashScreen");
                    //FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAppOpen);
                    Debug.Log("-----------------------Initialized-----------------------");
                    //setupRemoteConfig();
                }
                else
                {
                    Debug.Log("-----------------------Hata-----------------------");
                    UnityEngine.Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    isActive = false;
                    isInitialized = false;
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }
        catch (Exception ex)
        {
            isActive = false;
            Debug.Log("Error " + ex.Message);
        }
    }

    

    bool getStoredValue(string ppString, bool defaultValue)
    {
        return defaultValue;
        //if (!PlayerPrefs.HasKey(ppString))
        //{
        //    PlayerPrefs.SetInt(ppString, defaultValue ? 1 : 0);
        //    return defaultValue;
        //}
        //return PlayerPrefs.GetInt(ppString) == 1;
    }


    public void changeScreen(string sceneName, string className)
    {
        if (!isActive || !isInitialized)
        {
            Debug.Log("Returneeeed");
            return;
        }
        FirebaseAnalytics.SetCurrentScreen(sceneName, className);
        lastSceneName = sceneName;
    }

    public void sendColor(string colorName, string prefix)
    {
        if (!isActive || !isInitialized)
        {
            return;
        }
        FirebaseAnalytics.LogEvent("bm_" + prefix + "_color", colorName, 1);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnApplicationFocus()
    {
        if (!isActive || !isInitialized)
        {
            return;
        }

    }

    void OnApplicationPause()
    {
        if (!isActive || !isInitialized)
        {
            return;
        }
        //FirebaseAnalytics.LogEvent("event", "app_close", DateTime.UtcNow.Ticks);
    }


}
