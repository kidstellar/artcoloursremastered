﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpinObject : MonoBehaviour
{

	public float glPlFltDeltaLimit;
	public float glPlFltDeltaReduce;
	public int glPlIntLapsBeforeStopping;
	public bool glPlBoolCanRotate;
	public AudioClip glPlSpinSound;
	private float glPrFltDeltaRotation;
	private float glPrFltPreviousRotation;
	private float glPrFltCurrentRotation;
	public int glPrIntCurrentLaps;
	private float glPrFloatRotation;
	private float glPrFltQuarterRotation;
	private bool boolCountRotations;
	public bool rot = false;

	void Start()
	{
		glPrIntCurrentLaps = glPlIntLapsBeforeStopping;
		glPrFloatRotation = 0f;
		//glPlBoolCanRotate = true;
		boolCountRotations = true;

	}

	// Update is called once per frame
	void Update()
	{

			RotateThis();
			CountRotations();


		if (rot) 
		{
			glPrFltCurrentRotation = angleBetweenPoints(transform.position,
			    Camera.main.ScreenToWorldPoint(Input.mousePosition));
			glPrFltDeltaRotation = Mathf.DeltaAngle(glPrFltCurrentRotation, glPrFltPreviousRotation);
			if (Mathf.Abs(glPrFltDeltaRotation) > glPlFltDeltaLimit) 
			{
				glPrFltDeltaRotation = glPlFltDeltaLimit * Mathf.Sign(glPrFltDeltaRotation);
			}

			glPrFltPreviousRotation = glPrFltCurrentRotation;
			transform.Rotate(Vector3.back * Time.deltaTime, glPrFltDeltaRotation);
		}
	}

	public void SetRotateActive(bool value)
	{
		glPlBoolCanRotate = value;
	}

	public void DeactivateAnim()
	{
		if(!glPlBoolCanRotate)
		{
			return;
		}
		SetRotateActive(false);
		transform.DOKill();
		transform.DORotate(Vector3.forward * 0f, 0.4f);
	}

	public void ActivateAnim()
	{
		if(glPlBoolCanRotate)
		{
			return;
		}
		SetRotateActive(true);
	}

	private void CountRotations()
	{
		if (boolCountRotations) 
		{

			if (Mathf.Sign(glPrFltDeltaRotation).Equals(1)) 
			{
				glPrFloatRotation += glPrFltDeltaRotation;
			}
			else if (Mathf.Sign(glPrFltDeltaRotation).Equals(-1)) 
		  	{
				glPrFloatRotation += glPrFltDeltaRotation;
			}
			if (glPrFloatRotation >= 360 || glPrFloatRotation < -360f && rot) 
			{
				glPrFloatRotation -= glPrFloatRotation;
				glPrIntCurrentLaps += 1;
				ExtractionMachine.Instance.FillAllTubes();


			}
		}
	}

	private void RotateThis()
	{
		if (Input.GetMouseButtonDown(0) && glPlBoolCanRotate) {

			if (this.gameObject == Utility.getObjectInPos(Camera.main.ScreenToWorldPoint(Input.mousePosition), "bigValve")) {
				rot = true;
				// Get initial rotation of this game object
				glPrFltDeltaRotation = 0f;
				glPrFltPreviousRotation = angleBetweenPoints(transform.position,
				    Camera.main.ScreenToWorldPoint(Input.mousePosition));
			}
		} else if (Input.GetMouseButtonUp(0)) {
			rot = false;
		}

	}

	private float angleBetweenPoints(Vector2 v2Position1, Vector2 v2Position2)
	{
		Vector2 v2FromLine = v2Position2 - v2Position1;
		Vector2 v2ToLine = new Vector2(1, 0);

		float fltAngle = Vector2.Angle(v2FromLine, v2ToLine);

		// If rotation is more than 180
		Vector3 v3Cross = Vector3.Cross(v2FromLine, v2ToLine);
		if (v3Cross.z > 0) {
			fltAngle = 360f - fltAngle;
		}

		return fltAngle;
	}

	private IEnumerator EnableSpinForever(int intWaitSeconds)
	{
		yield return new WaitForSeconds(intWaitSeconds);
		glPlBoolCanRotate = true;
		boolCountRotations = false;
	}

	private void PlaySound()
	{
		glPrFltQuarterRotation += Mathf.Abs(glPrFltDeltaRotation);

		if (glPrFltQuarterRotation >= 90) {
			glPrFltQuarterRotation -= 90;
			//SoundManager.instance.RandomizeSfxShot(glPlSpinSound);
		}
	}
}
