﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PingPongMove : MonoBehaviour 
{
	public float distanceX;
	public float duration;
    float scaleX;
	// Use this for initialization
	void Start () 
    {
        scaleX = transform.localScale.x;
        StartCoroutine(animTask());
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    IEnumerator animTask()
    {
        while(true)
        {
            flip();
            yield return transform.DOMoveX(distanceX, duration).WaitForCompletion();
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            flip();
            yield return transform.DOMoveX(-distanceX, duration).WaitForCompletion();
            yield return new WaitForSeconds(Random.Range(1f, 3f));

        }
    }
}
