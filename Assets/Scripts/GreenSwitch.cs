﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GreenSwitch : MonoBehaviour 
{
	bool switching = false;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void OnMouseUpAsButton()
	{
		if(switching)
		{
			return;
		}
		StartCoroutine(SwitchAnimTask());
	}

	IEnumerator SwitchAnimTask()
	{
		switching = true;
		transform.DOLocalRotate(Vector3.forward * -60f, 0.5f).SetEase(Ease.OutSine);
		yield return transform.DOLocalMoveY(-1.11f, 0.5f).SetEase(Ease.OutSine).WaitForCompletion();
		transform.DOLocalRotate(Vector3.forward * 0f, 0.5f).SetEase(Ease.InSine);
		yield return transform.DOLocalMoveY(0.72f, 0.5f).SetEase(Ease.InSine).WaitForCompletion();
		switching = false;
	}
}
