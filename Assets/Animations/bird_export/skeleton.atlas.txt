
skeleton.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
Lower_beak
  rotate: false
  xy: 557, 1002
  size: 99, 46
  orig: 99, 46
  offset: 0, 0
  index: -1
Wing_right
  rotate: false
  xy: 557, 1050
  size: 264, 680
  orig: 264, 680
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2, 367
  size: 591, 414
  orig: 591, 414
  offset: 0, 0
  index: -1
eye1
  rotate: false
  xy: 351, 299
  size: 57, 66
  orig: 57, 66
  offset: 0, 0
  index: -1
eye2
  rotate: false
  xy: 823, 1470
  size: 57, 46
  orig: 57, 46
  offset: 0, 0
  index: -1
eye3
  rotate: false
  xy: 1007, 1707
  size: 61, 23
  orig: 61, 23
  offset: 0, 0
  index: -1
leg_left
  rotate: false
  xy: 823, 1518
  size: 182, 212
  orig: 182, 212
  offset: 0, 0
  index: -1
leg_right
  rotate: false
  xy: 2, 2
  size: 182, 212
  orig: 182, 212
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 2, 216
  size: 347, 149
  orig: 347, 149
  offset: 0, 0
  index: -1
upper_beak
  rotate: false
  xy: 418, 914
  size: 111, 44
  orig: 111, 44
  offset: 0, 0
  index: -1
wing_left
  rotate: false
  xy: 2, 960
  size: 553, 770
  orig: 553, 770
  offset: 0, 0
  index: -1
