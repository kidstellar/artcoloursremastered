
skeleton.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
beld_buckle
  rotate: false
  xy: 1331, 323
  size: 175, 175
  orig: 175, 175
  offset: 0, 0
  index: -1
belt
  rotate: false
  xy: 2518, 2771
  size: 495, 131
  orig: 495, 131
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 1933, 2797
  size: 583, 813
  orig: 583, 813
  offset: 0, 0
  index: -1
eyes1
  rotate: false
  xy: 2, 308
  size: 923, 230
  orig: 923, 230
  offset: 0, 0
  index: -1
eyes1 confused
  rotate: false
  xy: 2518, 3381
  size: 924, 229
  orig: 924, 229
  offset: 0, 0
  index: -1
eyes1 curious
  rotate: true
  xy: 1530, 1723
  size: 924, 244
  orig: 924, 244
  offset: 0, 0
  index: -1
eyes1 scared_sad
  rotate: false
  xy: 2518, 3179
  size: 925, 200
  orig: 925, 200
  offset: 0, 0
  index: -1
eyes2
  rotate: false
  xy: 2518, 3016
  size: 964, 161
  orig: 964, 161
  offset: 0, 0
  index: -1
eyes3
  rotate: true
  xy: 1776, 1746
  size: 901, 53
  orig: 901, 53
  offset: 0, 0
  index: -1
glasses
  rotate: false
  xy: 2, 540
  size: 1142, 466
  orig: 1142, 466
  offset: 0, 0
  index: -1
hair_back
  rotate: true
  xy: 1933, 3612
  size: 452, 1536
  orig: 452, 1536
  offset: 0, 0
  index: -1
hair_head
  rotate: true
  xy: 1146, 500
  size: 506, 430
  orig: 506, 430
  offset: 0, 0
  index: -1
hair_head2
  rotate: false
  xy: 1831, 1980
  size: 542, 224
  orig: 542, 224
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 1008
  size: 1526, 1639
  orig: 1526, 1639
  offset: 0, 0
  index: -1
hip
  rotate: false
  xy: 1831, 2206
  size: 491, 330
  orig: 491, 330
  offset: 0, 0
  index: -1
iris
  rotate: false
  xy: 2518, 2904
  size: 681, 110
  orig: 681, 110
  offset: 0, 0
  index: -1
l_arm
  rotate: true
  xy: 2504, 2609
  size: 160, 402
  orig: 160, 402
  offset: 0, 0
  index: -1
l_arm2
  rotate: true
  xy: 1831, 1808
  size: 170, 526
  orig: 170, 526
  offset: 0, 0
  index: -1
l_foot
  rotate: true
  xy: 2375, 1961
  size: 264, 222
  orig: 264, 222
  offset: 0, 0
  index: -1
l_leg
  rotate: false
  xy: 1530, 1036
  size: 230, 685
  orig: 230, 685
  offset: 0, 0
  index: -1
mouth_curious2
  rotate: false
  xy: 2359, 1806
  size: 176, 153
  orig: 176, 153
  offset: 0, 0
  index: -1
mouth_curious3
  rotate: false
  xy: 3015, 2749
  size: 168, 153
  orig: 168, 153
  offset: 0, 0
  index: -1
mouth_idle
  rotate: false
  xy: 3445, 3236
  size: 273, 152
  orig: 273, 152
  offset: 0, 0
  index: -1
mouth_sad2
  rotate: false
  xy: 2599, 2050
  size: 235, 164
  orig: 235, 164
  offset: 0, 0
  index: -1
mouth_sad3
  rotate: true
  xy: 1122, 321
  size: 177, 207
  orig: 177, 207
  offset: 0, 0
  index: -1
mouth_scared2
  rotate: false
  xy: 927, 330
  size: 193, 208
  orig: 193, 208
  offset: 0, 0
  index: -1
mouth_scared3
  rotate: true
  xy: 689, 83
  size: 223, 242
  orig: 223, 242
  offset: 0, 0
  index: -1
mouth_smiling1
  rotate: false
  xy: 1112, 146
  size: 171, 173
  orig: 171, 173
  offset: 0, 0
  index: -1
mouth_smiling2
  rotate: true
  xy: 933, 106
  size: 222, 177
  orig: 222, 177
  offset: 0, 0
  index: -1
mouth_suprised2
  rotate: true
  xy: 2620, 2216
  size: 167, 249
  orig: 167, 249
  offset: 0, 0
  index: -1
mouth_suprised3
  rotate: false
  xy: 3985, 3814
  size: 79, 250
  orig: 79, 250
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 2324, 2227
  size: 294, 309
  orig: 294, 309
  offset: 0, 0
  index: -1
nose
  rotate: true
  xy: 3848, 3385
  size: 165, 124
  orig: 165, 124
  offset: 0, 0
  index: -1
pickaxe
  rotate: false
  xy: 3471, 3552
  size: 512, 512
  orig: 512, 512
  offset: 0, 0
  index: -1
r_arm
  rotate: true
  xy: 3444, 3390
  size: 160, 402
  orig: 160, 402
  offset: 0, 0
  index: -1
r_arm2
  rotate: false
  xy: 1578, 508
  size: 170, 526
  orig: 170, 526
  offset: 0, 0
  index: -1
r_foot
  rotate: false
  xy: 2620, 2385
  size: 264, 222
  orig: 264, 222
  offset: 0, 0
  index: -1
r_leg
  rotate: true
  xy: 2, 76
  size: 230, 685
  orig: 230, 685
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 2, 2649
  size: 1929, 1415
  orig: 1929, 1415
  offset: 0, 0
  index: -1
toka
  rotate: false
  xy: 1933, 2538
  size: 569, 257
  orig: 569, 257
  offset: 0, 0
  index: -1
