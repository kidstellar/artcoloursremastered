
skeleton.png
size: 8192,8192
format: RGBA8888
filter: Linear,Linear
repeat: none
apron
  rotate: false
  xy: 1399, 5337
  size: 1532, 776
  orig: 1532, 776
  offset: 0, 0
  index: -1
apron_pocket
  rotate: false
  xy: 5499, 6815
  size: 972, 564
  orig: 972, 564
  offset: 0, 0
  index: -1
bahce_makasi
  rotate: false
  xy: 1399, 6115
  size: 2048, 2048
  orig: 2048, 2048
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 5206
  size: 1395, 2957
  orig: 1395, 2957
  offset: 0, 0
  index: -1
eye confused
  rotate: false
  xy: 6981, 7899
  size: 798, 264
  orig: 798, 264
  offset: 0, 0
  index: -1
eye sad
  rotate: true
  xy: 2, 1510
  size: 798, 264
  orig: 798, 264
  offset: 0, 0
  index: -1
eye scared
  rotate: false
  xy: 5499, 6550
  size: 798, 263
  orig: 798, 263
  offset: 0, 0
  index: -1
eye1
  rotate: true
  xy: 916, 4406
  size: 798, 265
  orig: 798, 265
  offset: 0, 0
  index: -1
eye1 curious
  rotate: true
  xy: 565, 2975
  size: 798, 264
  orig: 798, 264
  offset: 0, 0
  index: -1
eye2
  rotate: false
  xy: 6981, 7664
  size: 798, 233
  orig: 798, 233
  offset: 0, 0
  index: -1
eye3
  rotate: false
  xy: 1399, 5267
  size: 830, 68
  orig: 830, 68
  offset: 0, 0
  index: -1
eye_happy
  rotate: false
  xy: 4398, 5849
  size: 798, 264
  orig: 798, 264
  offset: 0, 0
  index: -1
iris
  rotate: false
  xy: 2933, 5355
  size: 676, 195
  orig: 676, 195
  offset: 0, 0
  index: -1
l_arm
  rotate: false
  xy: 2, 2310
  size: 561, 1463
  orig: 561, 1463
  offset: 0, 0
  index: -1
l_foot
  rotate: true
  xy: 7781, 7609
  size: 554, 384
  orig: 554, 384
  offset: 0, 0
  index: -1
logo
  rotate: true
  xy: 4398, 5381
  size: 466, 493
  orig: 466, 493
  offset: 0, 0
  index: -1
mouth_confused
  rotate: false
  xy: 6013, 6052
  size: 257, 247
  orig: 257, 247
  offset: 0, 0
  index: -1
mouth_confused 2
  rotate: true
  xy: 2, 1229
  size: 279, 247
  orig: 279, 247
  offset: 0, 0
  index: -1
mouth_idle
  rotate: false
  xy: 565, 2452
  size: 222, 247
  orig: 222, 247
  offset: 0, 0
  index: -1
mouth_sad2
  rotate: false
  xy: 6013, 6301
  size: 274, 247
  orig: 274, 247
  offset: 0, 0
  index: -1
mouth_sad3
  rotate: false
  xy: 6981, 7415
  size: 274, 247
  orig: 274, 247
  offset: 0, 0
  index: -1
mouth_scared2
  rotate: false
  xy: 7257, 7415
  size: 254, 247
  orig: 254, 247
  offset: 0, 0
  index: -1
mouth_scared3
  rotate: false
  xy: 5198, 5865
  size: 263, 248
  orig: 263, 248
  offset: 0, 0
  index: -1
mouth_smile2
  rotate: true
  xy: 916, 3823
  size: 261, 247
  orig: 261, 247
  offset: 0, 0
  index: -1
mouth_smile3
  rotate: true
  xy: 565, 2701
  size: 272, 247
  orig: 272, 247
  offset: 0, 0
  index: -1
mouth_suprise2
  rotate: true
  xy: 268, 1515
  size: 237, 247
  orig: 237, 247
  offset: 0, 0
  index: -1
mouth_suprise3
  rotate: true
  xy: 2, 969
  size: 258, 247
  orig: 258, 247
  offset: 0, 0
  index: -1
notebook
  rotate: true
  xy: 4893, 5443
  size: 404, 331
  orig: 404, 331
  offset: 0, 0
  index: -1
orak
  rotate: false
  xy: 3449, 6115
  size: 2048, 2048
  orig: 2048, 2048
  offset: 0, 0
  index: -1
pants
  rotate: false
  xy: 5499, 7381
  size: 1480, 782
  orig: 1480, 782
  offset: 0, 0
  index: -1
pen
  rotate: false
  xy: 1183, 5077
  size: 129, 127
  orig: 129, 127
  offset: 0, 0
  index: -1
pen_part
  rotate: false
  xy: 6299, 6711
  size: 100, 102
  orig: 100, 102
  offset: 0, 0
  index: -1
pickaxe
  rotate: false
  xy: 6473, 6867
  size: 512, 512
  orig: 512, 512
  offset: 0, 0
  index: -1
pipet
  rotate: false
  xy: 916, 4086
  size: 260, 318
  orig: 260, 318
  offset: 0, 0
  index: -1
r_arm
  rotate: true
  xy: 2933, 5552
  size: 561, 1463
  orig: 561, 1463
  offset: 0, 0
  index: -1
r_foot
  rotate: true
  xy: 268, 1754
  size: 554, 384
  orig: 554, 384
  offset: 0, 0
  index: -1
thirt
  rotate: true
  xy: 2, 3775
  size: 1429, 912
  orig: 1429, 912
  offset: 0, 0
  index: -1
tirmik
  rotate: false
  xy: 5499, 6036
  size: 512, 512
  orig: 512, 512
  offset: 0, 0
  index: -1
