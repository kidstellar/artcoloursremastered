��                       �  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float _Distortion;
    float _Wind;
    float _Wind2;
    float _Wind3;
    float _Wind4;
    float _Alpha;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_MainTex [[ sampler (0) ]],
    sampler sampler_LightBuffer [[ sampler (1) ]],
    texture2d<float, access::sample > _MainTex [[ texture(0) ]] ,
    texture2d<float, access::sample > _LightBuffer [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    float4 u_xlat1;
    float4 u_xlat2;
    float u_xlat3;
    bool u_xlatb4;
    float3 u_xlat5;
    bool u_xlatb5;
    bool2 u_xlatb9;
    float u_xlat12;
    bool u_xlatb12;
    u_xlat0.x = input.TEXCOORD0.y / FGlobals._Distortion;
    u_xlat0.x = fma(u_xlat0.x, FGlobals._Wind, input.TEXCOORD0.x);
    u_xlatb4 = abs(u_xlat0.x)>=-abs(u_xlat0.x);
    u_xlat0.x = fract(abs(u_xlat0.x));
    u_xlat0.x = (u_xlatb4) ? u_xlat0.x : (-u_xlat0.x);
    u_xlat0.y = input.TEXCOORD0.y;
    u_xlat0 = _MainTex.sample(sampler_MainTex, u_xlat0.xy);
    u_xlat0 = fma(u_xlat0, input.TEXCOORD2, float4(-0.194999993, -0.194999993, -0.194999993, -0.00999999978));
    u_xlat1 = input.TEXCOORD0.xyxy + float4(0.200000003, 0.00999999978, 0.400000006, 0.0199999996);
    u_xlat5.xz = u_xlat1.yw / float2(FGlobals._Distortion);
    u_xlat1.xy = fma(u_xlat5.xz, float2(FGlobals._Wind2, FGlobals._Wind3), u_xlat1.xz);
    u_xlatb9.xy = (abs(u_xlat1.xy)>=-abs(u_xlat1.xy));
    u_xlat1.xy = fract(abs(u_xlat1.xy));
    {
        float4 hlslcc_movcTemp = u_xlat1;
        u_xlat1.x = (u_xlatb9.x) ? hlslcc_movcTemp.x : hlslcc_movcTemp.x;
        u_xlat1.y = (u_xlatb9.y) ? hlslcc_movcTemp.y : hlslcc_movcTemp.y;
    }
    u_xlat1.zw = input.TEXCOORD0.yy + float2(0.00999999978, 0.0199999996);
    u_xlat2 = _MainTex.sample(sampler_MainTex, u_xlat1.xz);
    u_xlat1 = _MainTex.sample(sampler_MainTex, u_xlat1.yw);
    u_xlat1 = fma(u_xlat1, input.TEXCOORD2, float4(-0.0649999976, -0.0649999976, -0.0649999976, -0.00999999978));
    u_xlat2 = fma(u_xlat2, input.TEXCOORD2, float4(-0.129999995, -0.129999995, -0.129999995, -0.00999999978));
    u_xlat3 = (-u_xlat2.w) + 1.0;
    u_xlat2 = u_xlat2.wwww * u_xlat2;
    u_xlat0 = fma(u_xlat0, float4(u_xlat3), u_xlat2);
    u_xlat2.x = (-u_xlat1.w) + 1.0;
    u_xlat1 = u_xlat1.wwww * u_xlat1;
    u_xlat0 = fma(u_xlat0, u_xlat2.xxxx, u_xlat1);
    u_xlat1.xyw = input.TEXCOORD0.xyy + float3(0.600000024, 0.0299999993, 0.0299999993);
    u_xlat5.x = u_xlat1.y / FGlobals._Distortion;
    u_xlat1.x = fma(u_xlat5.x, FGlobals._Wind4, u_xlat1.x);
    u_xlatb5 = abs(u_xlat1.x)>=-abs(u_xlat1.x);
    u_xlat1.x = fract(abs(u_xlat1.x));
    u_xlat1.z = (u_xlatb5) ? u_xlat1.x : (-u_xlat1.x);
    u_xlat1 = _MainTex.sample(sampler_MainTex, u_xlat1.zw);
    u_xlat2 = fma(u_xlat1, input.TEXCOORD2, float4(0.0, 0.0, 0.0, -0.00999999978));
    u_xlat1.x = fma(u_xlat1.w, input.TEXCOORD2.w, -0.00999999978);
    u_xlat2 = u_xlat1.xxxx * u_xlat2;
    u_xlat1.x = (-u_xlat1.x) + 1.0;
    u_xlat0 = fma(u_xlat0, u_xlat1.xxxx, u_xlat2);
    u_xlat12 = u_xlat0.w + (-FGlobals._Alpha);
    u_xlat0.xyz = float3(u_xlat12) * u_xlat0.xyz;
    u_xlat1.x = u_xlat12 + -0.0500000007;
    output.SV_Target0.w = u_xlat12;
    u_xlatb12 = u_xlat1.x<0.0;
    if((int(u_xlatb12) * int(0xffffffffu))!=0){discard_fragment();}
    u_xlat1.xy = input.TEXCOORD3.xy / input.TEXCOORD3.ww;
    u_xlat1.xyz = _LightBuffer.sample(sampler_LightBuffer, u_xlat1.xy).xyz;
    u_xlat1.xyz = log2(u_xlat1.xyz);
    u_xlat1.xyz = (-u_xlat1.xyz) + input.TEXCOORD5.xyz;
    output.SV_Target0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    return output;
}
                                 FGlobals         _Distortion                          _Wind                           _Wind2                          _Wind3                          _Wind4                          _Alpha                                 _MainTex                  _LightBuffer                FGlobals           