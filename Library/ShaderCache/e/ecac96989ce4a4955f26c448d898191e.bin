��                         POINT      SHADOWS_CUBEY  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _Time;
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    float4 _LightPositionRange;
    float4 _LightProjectionParams;
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 _LightColor0;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
    float _Distortion;
    float _Hole;
    float _Speed;
    float _Alpha;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (0) ]],
    sampler sampler_LightTexture0 [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    texture2d<half, access::sample > _MainTex [[ texture(0) ]] ,
    texture2d<float, access::sample > _LightTexture0 [[ texture(1) ]] ,
    depthcube<float, access::sample > _ShadowMapTexture [[ texture(2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float3 u_xlat1;
    half4 u_xlat16_1;
    float3 u_xlat2;
    float3 u_xlat3;
    half3 u_xlat16_4;
    float3 u_xlat5;
    bool u_xlatb5;
    half u_xlat16_9;
    float2 u_xlat10;
    float u_xlat15;
    float u_xlat16;
    half u_xlat16_19;
    u_xlat0.x = sin(FGlobals._Distortion);
    u_xlat5.x = FGlobals._Time.x * FGlobals._Speed;
    u_xlat5.x = u_xlat5.x * 5.0;
    u_xlat1.x = sin(u_xlat5.x);
    u_xlat2.x = cos(u_xlat5.x);
    u_xlat3.z = u_xlat1.x;
    u_xlat5.xy = input.TEXCOORD0.xy + float2(-0.5, -0.5);
    u_xlat5.xy = u_xlat5.xy * float2(1.24600005, 1.24600005);
    u_xlat3.y = u_xlat2.x;
    u_xlat3.x = (-u_xlat1.x);
    u_xlat1.y = dot(u_xlat5.xy, u_xlat3.xy);
    u_xlat1.x = dot(u_xlat5.xy, u_xlat3.yz);
    u_xlat5.x = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat5.x = sqrt(u_xlat5.x);
    u_xlat10.x = (-u_xlat5.x) + 0.5;
    u_xlatb5 = u_xlat5.x<0.5;
    u_xlat10.x = u_xlat10.x + u_xlat10.x;
    u_xlat10.x = u_xlat10.x * u_xlat10.x;
    u_xlat0.x = u_xlat0.x * u_xlat10.x;
    u_xlat0.x = u_xlat0.x * 16.0;
    u_xlat2.x = cos(u_xlat0.x);
    u_xlat0.x = sin(u_xlat0.x);
    u_xlat3.x = (-u_xlat0.x);
    u_xlat3.y = u_xlat2.x;
    u_xlat3.z = u_xlat0.x;
    u_xlat2.y = dot(u_xlat1.yx, u_xlat3.yz);
    u_xlat2.x = dot(u_xlat1.yx, u_xlat3.xy);
    u_xlat0.xy = (bool(u_xlatb5)) ? u_xlat2.xy : u_xlat1.xy;
    u_xlat10.xy = u_xlat1.xy + float2(0.5, 0.5);
    u_xlat10.xy = (-u_xlat10.xy) + float2(0.5, 0.5);
    u_xlat10.x = dot(u_xlat10.xy, u_xlat10.xy);
    u_xlat10.x = sqrt(u_xlat10.x);
    u_xlat0.xy = u_xlat0.xy + float2(0.5, 0.5);
    u_xlat16_1 = _MainTex.sample(sampler_MainTex, u_xlat0.xy);
    u_xlat0.x = (-FGlobals._Alpha) + 1.0;
    u_xlat0.x = u_xlat0.x * float(u_xlat16_1.w);
    u_xlat5.x = u_xlat10.x + -0.25;
    u_xlat5.y = u_xlat10.x + (-FGlobals._Hole);
    u_xlat5.xy = u_xlat5.xy * float2(4.0, 6.66666651);
    u_xlat5.xy = clamp(u_xlat5.xy, 0.0f, 1.0f);
    u_xlat15 = fma(u_xlat5.x, -2.0, 3.0);
    u_xlat5.x = u_xlat5.x * u_xlat5.x;
    u_xlat5.x = fma((-u_xlat15), u_xlat5.x, 1.0);
    u_xlat15 = fma(u_xlat5.y, -2.0, 3.0);
    u_xlat10.x = u_xlat5.y * u_xlat5.y;
    u_xlat10.x = fma((-u_xlat15), u_xlat10.x, 1.0);
    u_xlat10.x = (-u_xlat10.x) + 1.0;
    u_xlat5.x = u_xlat10.x * u_xlat5.x;
    u_xlat1.xyz = u_xlat10.xxx * float3(u_xlat16_1.xyz);
    u_xlat16_4.x = half(fma(u_xlat0.x, u_xlat5.x, -0.0500000007));
    u_xlat0.x = u_xlat5.x * u_xlat0.x;
    u_xlatb5 = u_xlat16_4.x<half(0.0);
    if((int(u_xlatb5) * int(0xffffffffu))!=0){discard_fragment();}
    u_xlat5.xyz = u_xlat0.xxx * u_xlat1.xyz;
    output.SV_Target0.w = half(u_xlat0.x);
    u_xlat1.xyz = input.TEXCOORD2.xyz + (-FGlobals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat1.xyz = (-input.TEXCOORD2.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat2.x = FGlobals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat2.y = FGlobals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat2.z = FGlobals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat0.x = u_xlat0.x + (-u_xlat1.x);
    u_xlat0.x = fma(FGlobals.unity_ShadowFadeCenterAndType.w, u_xlat0.x, u_xlat1.x);
    u_xlat0.x = fma(u_xlat0.x, float(FGlobals._LightShadowData.z), float(FGlobals._LightShadowData.w));
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat1.xyz = input.TEXCOORD2.xyz + (-FGlobals._LightPositionRange.xyz);
    u_xlat16 = max(abs(u_xlat1.y), abs(u_xlat1.x));
    u_xlat16 = max(abs(u_xlat1.z), u_xlat16);
    u_xlat16 = u_xlat16 + (-FGlobals._LightProjectionParams.z);
    u_xlat16 = max(u_xlat16, 9.99999975e-06);
    u_xlat16 = u_xlat16 * FGlobals._LightProjectionParams.w;
    u_xlat16 = FGlobals._LightProjectionParams.y / u_xlat16;
    u_xlat16 = u_xlat16 + (-FGlobals._LightProjectionParams.x);
    u_xlat16 = (-u_xlat16) + 1.0;
    u_xlat16_1.x = _ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xyz, saturate(u_xlat16), level(0.0));
    u_xlat16_4.x = (-FGlobals._LightShadowData.x) + half(1.0);
    u_xlat16_4.x = fma(u_xlat16_1.x, u_xlat16_4.x, FGlobals._LightShadowData.x);
    u_xlat16_9 = (-u_xlat16_4.x) + half(1.0);
    u_xlat16_4.x = half(fma(u_xlat0.x, float(u_xlat16_9), float(u_xlat16_4.x)));
    u_xlat1.xyz = input.TEXCOORD2.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, input.TEXCOORD2.xxx, u_xlat1.xyz);
    u_xlat1.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, input.TEXCOORD2.zzz, u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz + FGlobals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat0.x = _LightTexture0.sample(sampler_LightTexture0, u_xlat0.xx).x;
    u_xlat0.x = float(u_xlat16_4.x) * u_xlat0.x;
    u_xlat16_4.xyz = half3(u_xlat0.xxx * float3(FGlobals._LightColor0.xyz));
    u_xlat16_4.xyz = half3(u_xlat5.xyz * float3(u_xlat16_4.xyz));
    u_xlat0.xyz = (-input.TEXCOORD2.xyz) + FGlobals._WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat0.xyz = float3(u_xlat15) * u_xlat0.xyz;
    u_xlat16_19 = dot(input.TEXCOORD1.xyz, u_xlat0.xyz);
    u_xlat16_19 = max(u_xlat16_19, half(0.0));
    output.SV_Target0.xyz = half3(u_xlat16_19) * u_xlat16_4.xyz;
    return output;
}
                                 FGlobals        _Time                            _WorldSpaceCameraPos                        _WorldSpaceLightPos0                         _LightPositionRange                   0      _LightProjectionParams                    @      _LightShadowData                 P      unity_ShadowFadeCenterAndType                     `      _LightColor0                 �      _Distortion                         _Hole                          _Speed                         _Alpha                         unity_MatrixV                    p      unity_WorldToLight                   �             _MainTex                 _LightTexture0                  _ShadowMapTexture                    FGlobals           