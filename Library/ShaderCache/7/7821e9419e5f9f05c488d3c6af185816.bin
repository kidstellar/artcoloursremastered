��                         POINT      SHADOWS_CUBE   SHADOWS_SOFT!  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _Time;
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    float4 _LightPositionRange;
    float4 _LightProjectionParams;
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 _LightColor0;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
    float _Alpha;
    float _Value1;
    float _Value2;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (0) ]],
    sampler sampler_LightTexture0 [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    sampler sampler_MainTex2 [[ sampler (3) ]],
    texture2d<half, access::sample > _MainTex2 [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    texture2d<float, access::sample > _LightTexture0 [[ texture(2) ]] ,
    depthcube<float, access::sample > _ShadowMapTexture [[ texture(3) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float3 u_xlat1;
    half3 u_xlat16_1;
    float4 u_xlat2;
    half4 u_xlat16_2;
    float4 u_xlat3;
    bool2 u_xlatb3;
    float4 u_xlat4;
    bool2 u_xlatb4;
    half3 u_xlat16_5;
    float3 u_xlat6;
    half u_xlat16_12;
    float2 u_xlat15;
    bool u_xlatb15;
    float2 u_xlat17;
    float u_xlat21;
    bool u_xlatb21;
    float u_xlat22;
    float u_xlat23;
    bool u_xlatb23;
    half u_xlat16_26;
    u_xlat0.xyz = (-input.TEXCOORD2.xyz) + FGlobals._WorldSpaceLightPos0.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = rsqrt(u_xlat21);
    u_xlat0.xyz = float3(u_xlat21) * u_xlat0.xyz;
    u_xlat1.xy = input.TEXCOORD0.xy * float2(0.125, 0.125);
    u_xlat21 = FGlobals._Time.x * FGlobals._Value1;
    u_xlatb15 = u_xlat21>=(-u_xlat21);
    u_xlat22 = fract(abs(u_xlat21));
    u_xlat15.x = (u_xlatb15) ? u_xlat22 : (-u_xlat22);
    u_xlat15.x = u_xlat15.x * 8.0;
    u_xlat15.x = floor(u_xlat15.x);
    u_xlat2.x = fma(u_xlat15.x, 0.125, u_xlat1.x);
    u_xlat21 = u_xlat21 * 0.125;
    u_xlatb15 = u_xlat21>=(-u_xlat21);
    u_xlat21 = fract(abs(u_xlat21));
    u_xlat21 = (u_xlatb15) ? u_xlat21 : (-u_xlat21);
    u_xlat21 = u_xlat21 * 8.0;
    u_xlat21 = floor(u_xlat21);
    u_xlat21 = fma((-u_xlat21), 0.125, u_xlat1.y);
    u_xlat2.y = u_xlat21 + 1.0;
    u_xlat16_2.xyz = _MainTex2.sample(sampler_MainTex2, u_xlat2.xy).xyz;
    u_xlat15.xy = FGlobals._Time.xx + float2(0.200000003, 0.800000012);
    u_xlat15.xy = u_xlat15.xy * float2(FGlobals._Value1);
    u_xlatb3.xy = (u_xlat15.xy>=(-u_xlat15.xy));
    u_xlat17.xy = fract(abs(u_xlat15.xy));
    u_xlat3.x = (u_xlatb3.x) ? u_xlat17.x : (-u_xlat17.x);
    u_xlat3.y = (u_xlatb3.y) ? u_xlat17.y : (-u_xlat17.y);
    u_xlat3.xy = u_xlat3.xy * float2(8.0, 8.0);
    u_xlat3.xy = floor(u_xlat3.xy);
    u_xlat3.xy = fma(u_xlat3.xy, float2(0.125, 0.125), u_xlat1.xx);
    u_xlat1.xz = u_xlat15.xy * float2(0.125, 0.125);
    u_xlatb4.xy = (u_xlat1.xz>=(-u_xlat1.xz));
    u_xlat1.xz = fract(abs(u_xlat1.xz));
    {
        float3 hlslcc_movcTemp = u_xlat1;
        u_xlat1.x = (u_xlatb4.x) ? hlslcc_movcTemp.x : hlslcc_movcTemp.x;
        u_xlat1.z = (u_xlatb4.y) ? hlslcc_movcTemp.z : hlslcc_movcTemp.z;
    }
    u_xlat1.xz = u_xlat1.xz * float2(8.0, 8.0);
    u_xlat1.xz = floor(u_xlat1.xz);
    u_xlat1.xy = fma((-u_xlat1.xz), float2(0.125, 0.125), u_xlat1.yy);
    u_xlat3.zw = u_xlat1.xy + float2(1.0, 1.0);
    u_xlat16_1.xyz = _MainTex2.sample(sampler_MainTex2, u_xlat3.xz).xyz;
    u_xlat16_1.xyz = u_xlat16_1.xyz + u_xlat16_2.xyz;
    u_xlat16_2.xyz = _MainTex2.sample(sampler_MainTex2, u_xlat3.yw).xyz;
    u_xlat16_1.xyz = u_xlat16_1.xyz + u_xlat16_2.xyz;
    u_xlat16_2 = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy);
    u_xlat2 = float4(u_xlat16_2) * input.TEXCOORD3;
    u_xlat1.xyz = fma(float3(u_xlat16_1.xyz), float3(FGlobals._Value2), u_xlat2.xyz);
    u_xlat21 = (-FGlobals._Alpha) + 1.0;
    u_xlat22 = u_xlat21 * u_xlat2.w;
    u_xlat1.xyz = float3(u_xlat22) * u_xlat1.xyz;
    u_xlat16_5.x = half(fma(u_xlat2.w, u_xlat21, -0.0500000007));
    u_xlatb21 = u_xlat16_5.x<half(0.0);
    if((int(u_xlatb21) * int(0xffffffffu))!=0){discard_fragment();}
    u_xlat2.xyz = input.TEXCOORD2.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, input.TEXCOORD2.xxx, u_xlat2.xyz);
    u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, input.TEXCOORD2.zzz, u_xlat2.xyz);
    u_xlat2.xyz = u_xlat2.xyz + FGlobals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat3.xyz = (-input.TEXCOORD2.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat4.x = FGlobals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat4.y = FGlobals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat4.z = FGlobals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat21 = dot(u_xlat3.xyz, u_xlat4.xyz);
    u_xlat3.xyz = input.TEXCOORD2.xyz + (-FGlobals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat23 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat23 = sqrt(u_xlat23);
    u_xlat23 = (-u_xlat21) + u_xlat23;
    u_xlat21 = fma(FGlobals.unity_ShadowFadeCenterAndType.w, u_xlat23, u_xlat21);
    u_xlat21 = fma(u_xlat21, float(FGlobals._LightShadowData.z), float(FGlobals._LightShadowData.w));
    u_xlat21 = clamp(u_xlat21, 0.0f, 1.0f);
    u_xlatb23 = u_xlat21<0.99000001;
    if(u_xlatb23){
        u_xlat3.xyz = input.TEXCOORD2.xyz + (-FGlobals._LightPositionRange.xyz);
        u_xlat23 = max(abs(u_xlat3.y), abs(u_xlat3.x));
        u_xlat23 = max(abs(u_xlat3.z), u_xlat23);
        u_xlat23 = u_xlat23 + (-FGlobals._LightProjectionParams.z);
        u_xlat23 = max(u_xlat23, 9.99999975e-06);
        u_xlat23 = u_xlat23 * FGlobals._LightProjectionParams.w;
        u_xlat23 = FGlobals._LightProjectionParams.y / u_xlat23;
        u_xlat23 = u_xlat23 + (-FGlobals._LightProjectionParams.x);
        u_xlat23 = (-u_xlat23) + 1.0;
        u_xlat4.xyz = u_xlat3.xyz + float3(0.0078125, 0.0078125, 0.0078125);
        u_xlat4.x = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat4.xyz, saturate(u_xlat23), level(0.0)));
        u_xlat6.xyz = u_xlat3.xyz + float3(-0.0078125, -0.0078125, 0.0078125);
        u_xlat4.y = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat6.xyz, saturate(u_xlat23), level(0.0)));
        u_xlat6.xyz = u_xlat3.xyz + float3(-0.0078125, 0.0078125, -0.0078125);
        u_xlat4.z = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat6.xyz, saturate(u_xlat23), level(0.0)));
        u_xlat3.xyz = u_xlat3.xyz + float3(0.0078125, -0.0078125, -0.0078125);
        u_xlat4.w = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat3.xyz, saturate(u_xlat23), level(0.0)));
        u_xlat23 = dot(u_xlat4, float4(0.25, 0.25, 0.25, 0.25));
        u_xlat16_5.x = (-FGlobals._LightShadowData.x) + half(1.0);
        u_xlat16_5.x = half(fma(u_xlat23, float(u_xlat16_5.x), float(FGlobals._LightShadowData.x)));
    } else {
        u_xlat16_5.x = half(1.0);
    }
    u_xlat16_12 = (-u_xlat16_5.x) + half(1.0);
    u_xlat16_5.x = half(fma(u_xlat21, float(u_xlat16_12), float(u_xlat16_5.x)));
    u_xlat21 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat21 = _LightTexture0.sample(sampler_LightTexture0, float2(u_xlat21)).x;
    u_xlat21 = float(u_xlat16_5.x) * u_xlat21;
    u_xlat16_5.xyz = half3(float3(u_xlat21) * float3(FGlobals._LightColor0.xyz));
    u_xlat16_26 = dot(input.TEXCOORD1.xyz, u_xlat0.xyz);
    u_xlat16_26 = max(u_xlat16_26, half(0.0));
    u_xlat16_5.xyz = half3(u_xlat1.xyz * float3(u_xlat16_5.xyz));
    output.SV_Target0.xyz = half3(u_xlat16_26) * u_xlat16_5.xyz;
    output.SV_Target0.w = half(u_xlat22);
    return output;
}
                                FGlobals        _Time                            _WorldSpaceCameraPos                        _WorldSpaceLightPos0                         _LightPositionRange                   0      _LightProjectionParams                    @      _LightShadowData                 P      unity_ShadowFadeCenterAndType                     `      _LightColor0                 �      _Alpha                          _Value1                        _Value2                        unity_MatrixV                    p      unity_WorldToLight                   �          	   _MainTex2                    _MainTex                _LightTexture0                  _ShadowMapTexture                    FGlobals           