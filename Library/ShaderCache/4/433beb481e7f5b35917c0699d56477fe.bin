��                         POINT      SHADOWS_CUBEC  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    float4 _LightPositionRange;
    float4 _LightProjectionParams;
    half4 _LightShadowData;
    float4 unity_ShadowFadeCenterAndType;
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    half4 _LightColor0;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
    float _Distortion;
    float _Alpha;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (0) ]],
    sampler sampler_LightTexture0 [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    texture2d<half, access::sample > _MainTex [[ texture(0) ]] ,
    texture2d<float, access::sample > _LightTexture0 [[ texture(1) ]] ,
    depthcube<float, access::sample > _ShadowMapTexture [[ texture(2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half4 u_xlat16_0;
    half3 u_xlat16_1;
    float4 u_xlat2;
    half u_xlat16_2;
    bool u_xlatb2;
    float3 u_xlat3;
    half u_xlat16_5;
    float2 u_xlat6;
    float u_xlat10;
    float u_xlat12;
    half u_xlat16_13;
    float u_xlat14;
    u_xlat16_0 = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy);
    u_xlat12 = fma(float(u_xlat16_0.w), input.TEXCOORD3.w, (-FGlobals._Alpha));
    u_xlat16_1.x = half(u_xlat12 + -0.0500000007);
    u_xlatb2 = u_xlat16_1.x<half(0.0);
    if((int(u_xlatb2) * int(0xffffffffu))!=0){discard_fragment();}
    u_xlat2.xyz = float3(u_xlat16_0.xyz) * input.TEXCOORD3.xyz;
    u_xlat2.x = dot(u_xlat2.xyz, float3(0.212599993, 0.715200007, 0.0722000003));
    u_xlat6.x = fma((-u_xlat2.x), 0.115896732, 1.0);
    u_xlat10 = u_xlat2.x * u_xlat2.x;
    u_xlat6.x = fma(u_xlat10, 2.58329701, u_xlat6.x);
    u_xlat3.xyz = fma(u_xlat2.xxx, float3(0.616473019, 3.36968088, 0.169122502), float3(0.860117733, 1.0, 0.317398727));
    u_xlat2.x = u_xlat2.x * FGlobals._Distortion;
    u_xlat2.xw = u_xlat2.xx * float2(2.4000001, 1.5999999);
    u_xlat2.xw = u_xlat2.xw * u_xlat2.xw;
    u_xlat3.xyz = fma(float3(u_xlat10), float3(2.05825949, 11.3303223, 0.672770679), u_xlat3.xyz);
    u_xlat6.x = u_xlat3.z / u_xlat6.x;
    u_xlat10 = u_xlat3.x / u_xlat3.y;
    u_xlat3.x = u_xlat10 + u_xlat10;
    u_xlat6.y = u_xlat10 * 3.0;
    u_xlat3.x = fma((-u_xlat6.x), 8.0, u_xlat3.x);
    u_xlat6.x = u_xlat6.x + u_xlat6.x;
    u_xlat3.x = u_xlat3.x + 4.0;
    u_xlat2.yz = u_xlat6.xy / u_xlat3.xx;
    u_xlat3.x = (-u_xlat2.z) + 1.0;
    u_xlat3.x = (-u_xlat2.y) + u_xlat3.x;
    u_xlat2.y = float(1.0) / u_xlat2.y;
    u_xlat3.x = u_xlat3.x * u_xlat2.y;
    u_xlat2.xyw = u_xlat2.xzw * u_xlat2.xyw;
    u_xlat2.x = u_xlat2.x * u_xlat2.y;
    u_xlat3.x = u_xlat2.w * u_xlat3.x;
    u_xlat2.xy = u_xlat2.xw / float2(FGlobals._Distortion);
    u_xlat2.z = u_xlat3.x * FGlobals._Distortion;
    u_xlat0.xyz = fma(float3(u_xlat16_0.xyz), input.TEXCOORD3.xyz, u_xlat2.xyz);
    u_xlat0.xyz = float3(u_xlat12) * u_xlat0.xyz;
    output.SV_Target0.w = half(u_xlat12);
    u_xlat2.xyz = input.TEXCOORD2.xyz + (-FGlobals.unity_ShadowFadeCenterAndType.xyz);
    u_xlat12 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat12 = sqrt(u_xlat12);
    u_xlat2.xyz = (-input.TEXCOORD2.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat3.x = FGlobals.hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat3.y = FGlobals.hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat3.z = FGlobals.hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat3.xyz);
    u_xlat12 = u_xlat12 + (-u_xlat2.x);
    u_xlat12 = fma(FGlobals.unity_ShadowFadeCenterAndType.w, u_xlat12, u_xlat2.x);
    u_xlat12 = fma(u_xlat12, float(FGlobals._LightShadowData.z), float(FGlobals._LightShadowData.w));
    u_xlat12 = clamp(u_xlat12, 0.0f, 1.0f);
    u_xlat2.xyz = input.TEXCOORD2.xyz + (-FGlobals._LightPositionRange.xyz);
    u_xlat14 = max(abs(u_xlat2.y), abs(u_xlat2.x));
    u_xlat14 = max(abs(u_xlat2.z), u_xlat14);
    u_xlat14 = u_xlat14 + (-FGlobals._LightProjectionParams.z);
    u_xlat14 = max(u_xlat14, 9.99999975e-06);
    u_xlat14 = u_xlat14 * FGlobals._LightProjectionParams.w;
    u_xlat14 = FGlobals._LightProjectionParams.y / u_xlat14;
    u_xlat14 = u_xlat14 + (-FGlobals._LightProjectionParams.x);
    u_xlat14 = (-u_xlat14) + 1.0;
    u_xlat16_2 = _ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xyz, saturate(u_xlat14), level(0.0));
    u_xlat16_1.x = (-FGlobals._LightShadowData.x) + half(1.0);
    u_xlat16_1.x = fma(u_xlat16_2, u_xlat16_1.x, FGlobals._LightShadowData.x);
    u_xlat16_5 = (-u_xlat16_1.x) + half(1.0);
    u_xlat16_1.x = half(fma(u_xlat12, float(u_xlat16_5), float(u_xlat16_1.x)));
    u_xlat2.xyz = input.TEXCOORD2.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, input.TEXCOORD2.xxx, u_xlat2.xyz);
    u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, input.TEXCOORD2.zzz, u_xlat2.xyz);
    u_xlat2.xyz = u_xlat2.xyz + FGlobals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat12 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat12 = _LightTexture0.sample(sampler_LightTexture0, float2(u_xlat12)).x;
    u_xlat12 = float(u_xlat16_1.x) * u_xlat12;
    u_xlat16_1.xyz = half3(float3(u_xlat12) * float3(FGlobals._LightColor0.xyz));
    u_xlat16_1.xyz = half3(u_xlat0.xyz * float3(u_xlat16_1.xyz));
    u_xlat0.xyz = (-input.TEXCOORD2.xyz) + FGlobals._WorldSpaceLightPos0.xyz;
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = rsqrt(u_xlat12);
    u_xlat0.xyz = float3(u_xlat12) * u_xlat0.xyz;
    u_xlat16_13 = dot(input.TEXCOORD1.xyz, u_xlat0.xyz);
    u_xlat16_13 = max(u_xlat16_13, half(0.0));
    output.SV_Target0.xyz = half3(u_xlat16_13) * u_xlat16_1.xyz;
    return output;
}
                               FGlobals�         _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        _LightPositionRange                          _LightProjectionParams                    0      _LightShadowData                 @      unity_ShadowFadeCenterAndType                     P      _LightColor0                 �      _Distortion                   �      _Alpha                    �      unity_MatrixV                    `      unity_WorldToLight                   �             _MainTex                 _LightTexture0                  _ShadowMapTexture                    FGlobals           